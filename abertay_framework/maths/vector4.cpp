#include <maths/vector4.h>
#include <maths/matrix44.h>

namespace abfw
{

const Vector4 Vector4::Transform(const class Matrix44& _mat) const
{
	Vector4 result;

	result.x = x*_mat.m[0][0]+y*_mat.m[1][0]+z*_mat.m[2][0]+w*_mat.m[3][0];
	result.y = x*_mat.m[0][1]+y*_mat.m[1][1]+z*_mat.m[2][1]+w*_mat.m[3][1];
	result.z = x*_mat.m[0][2]+y*_mat.m[1][2]+z*_mat.m[2][2]+w*_mat.m[3][2];
	result.w = x*_mat.m[0][3]+y*_mat.m[1][3]+z*_mat.m[2][3]+w*_mat.m[3][3];

	return result;
}

}