#include "sprite_app.h"
#include <system/platform.h>
#include <graphics/sprite_renderer.h>
#include <graphics/texture.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>
#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/font.h>
#include <input/touch_input_manager.h>
#include <maths/vector3.h>
#include <input/sony_controller_input_manager.h>
#include <maths/math_utils.h>
#include <system/debug_log.h>

SpriteApp::SpriteApp(abfw::Platform& platform) :
	Application(platform),
	sprite_renderer_(NULL),
	font_(NULL)
{
}

void SpriteApp::Init()
{
	sprite_renderer_ = platform_.CreateSpriteRenderer();

	InitFont();

	sprite_.set_height(32.0f);
	sprite_.set_width(32.0f);
	sprite_.set_position(abfw::Vector3(platform_.width()*0.5f, platform_.height()*0.5f, 0.0f));

	sprite2_.set_height(64.0f);
	sprite2_.set_width(128.0f);
	sprite2_.set_position(abfw::Vector3(platform_.width()*0.5f, 64.0f, 0.0f));


	abfw::Colour red(1.0f, 0.0f, 0.0f);
	sprite2_.set_colour(red.GetABGR());

}

void SpriteApp::CleanUp()
{
	CleanUpFont();
	delete sprite_renderer_;
	sprite_renderer_ = NULL;
}

bool SpriteApp::Update(float frame_time)
{
	fps_ = 1.0f / frame_time;

	abfw::Vector3 position = sprite_.position();
	sprite_.set_position(abfw::Vector3(60 * frame_time + position.x, position.y, position.z));

	// spin sprite 2
	float rotation = sprite2_.rotation();
	rotation = std::fmodf(rotation+abfw::DegToRad(90.0f)*frame_time, FRAMEWORK_PI*2.0f);
	sprite2_.set_rotation(rotation);

	return true;
}

void SpriteApp::Render()
{
	sprite_renderer_->Begin();

	sprite_renderer_->DrawSprite(sprite_);
	sprite_renderer_->DrawSprite(sprite2_);

	DrawHUD();
	sprite_renderer_->End();
}
void SpriteApp::InitFont()
{
	font_ = new abfw::Font();
	font_->Load("comic_sans", platform_);
}

void SpriteApp::CleanUpFont()
{
	delete font_;
	font_ = NULL;
}

void SpriteApp::DrawHUD()
{
	if(font_)
	{
		font_->RenderText(sprite_renderer_, abfw::Vector3(850.0f, 510.0f, -0.9f), 1.0f, 0xffffffff, abfw::TJ_LEFT, "FPS: %.1f", fps_);
	}
}
