#ifndef _sprite_app_H
#define _sprite_app_H

#include <system/application.h>
#include <graphics/sprite.h>
#include <maths/vector2.h>
#include <maths/vector3.h>

#include <input/sony_controller_input_manager.h>

// FRAMEWORK FORWARD DECLARATIONS
namespace abfw
{
	class Platform;
	class SpriteRenderer;
	class Font;
}

class SpriteApp : public abfw::Application
{
public:
	SpriteApp(abfw::Platform& platform);
	void Init();
	void CleanUp();
	bool Update(float frame_time);
	void Render();
private:
	void InitFont();
	void CleanUpFont();
	void DrawHUD();

	abfw::SpriteRenderer* sprite_renderer_;
	abfw::Font* font_;

	abfw::Sprite sprite_;
	abfw::Sprite sprite2_;

	float fps_;
};

#endif // _sprite_app_H