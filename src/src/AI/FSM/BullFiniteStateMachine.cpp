#include "BullFiniteStateMachine.h"
#include "IdleState.h"
#include "ChargeState.h"
#include "StunnedState.h"
#include "..\..\BL\objects\Enemy.h"
#include "..\..\BL\EventHandler.h"
#include "../../Audio/AudioManager.h"

BullFiniteStateMachine::BullFiniteStateMachine(Enemy* e){
	State<Enemy>* idleState	= new IdleState;
	State<Enemy>* chargeState	= new ChargeState;
	StunnedState* stunnedState = new StunnedState(5);

	idleState->setTransitionFunction([e,this]{
			//capture player position
			abfw::Vector3 playerPosition = e->level->getGameObjectWithName("player")->getPosition();
			if (playerPosition.x <= e->position.x + e->fieldOfView.x && playerPosition.x >= e->position.x - e->fieldOfView.x &&
				playerPosition.y <= e->position.y + e->fieldOfView.y && playerPosition.y >= e->position.y - e->fieldOfView.y){
				//this->changeState(e,this->states["charge"]);
					if (e->getPosition().x - playerPosition.x > 0){
						e->facing = -1;
					}else{
						e->facing = 1;
					}
				EventHandler::getInstance()->playAnimation(e->name,"spotPlayer_" + e->getStringFacing(),false);
				this->currentState->exit(e);
				this->currentState = 0;
				this->inTransition = true;
			}
		});
	
	chargeState->setTransitionFunction([e,this]{
		//if hit a block, you're stunned
		if (e->hasHitABlock()){
			AudioManager::getInstance()->play("break",false);
			this->changeState(e,this->states["stunned"]);
			//EventHandler::getInstance()->playAnimation(e->name,"idle_" + e->getStringFacing(),true);
		}
	});

	stunnedState->setTransitionFunction([stunnedState,e,this]{
		if (stunnedState->getTimeCounter() <= 0){
			this->changeState(e,this->states["idle"]);
		}
	});

	states.insert(std::pair<std::string,State<Enemy>*>("idle",idleState));
	states.insert(std::pair<std::string,State<Enemy>*>("charge",chargeState));
	states.insert(std::pair<std::string,State<Enemy>*>("stunned",stunnedState));

	animationEvents = new BullAnimationEvents(e->name);
	changeState(e,idleState);

}

BullFiniteStateMachine::~BullFiniteStateMachine(){
	for (std::map<std::string,State<Enemy>*>::iterator it = states.begin(); it != states.end(); it++){
		delete (*it).second;
	}
	delete animationEvents;
}

////////////////
void BullAnimationEvents::animationFinished(std::string animName, Enemy* e){
	if (animName == "spotPlayer"){
		e->getStateMachine()->changeState(e,"charge");
	}
}

BullAnimationEvents::BullAnimationEvents(std::string owner){
	EventHandler::getInstance()->addListener(owner,this);
}