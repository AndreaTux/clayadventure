#pragma once
#include "FiniteStateMachine.h"
#include "..\..\BL\EnemyAnimationEvents.h"
#include <map>

class BullFiniteStateMachine : public FiniteStateMachine<Enemy>{
public:
	BullFiniteStateMachine(Enemy*);
	~BullFiniteStateMachine();

private:
	EnemyAnimationEvents* animationEvents;
};

class BullAnimationEvents : public EnemyAnimationEvents{
public:
	BullAnimationEvents(std::string);
	void animationFinished(std::string, Enemy*);
};