#include "ChargeState.h"
#include "..\..\BL\objects\Enemy.h"
#include "..\..\Audio\AudioManager.h"

void ChargeState::enter(Enemy* e){
	originalAcceleration = e->getAcceleration();
	originalMaxSpeed	 = e->getMaxSpeed();
	AudioManager::getInstance()->play("paper_walking",true);
	AudioManager::getInstance()->play("paper_loop",true);

}

void ChargeState::exit (Enemy* e){
	e->setAcceleration(originalAcceleration);
	e->setMaxSpeed(originalMaxSpeed);
	if (AudioManager::getInstance()->isPlaying("paper_walking")){
		AudioManager::getInstance()->stop("paper_walking");
		AudioManager::getInstance()->stop("paper_loop");

	}
}

void ChargeState::execute(Enemy* e,float dTime){
	if (e->targetPoint == 0){
		e->setAcceleration(abfw::Vector3(5,0,0));
		e->setMaxSpeed(abfw::Vector3(280,0,0));
		GameObject* player = e->level->getGameObjectWithName("player");
		e->direction.x = 1;
		if(e->position.x - player->getPosition().x >= 0){
			e->direction.x = -1;
		}
		e->targetPoint = new abfw::Vector3(player->getPosition().x,player->getPosition().y,0);
	}
}