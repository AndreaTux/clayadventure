#pragma once
#include "State.h"
#include <maths\vector3.h>

class ChargeState : public State<Enemy>{
private:
	abfw::Vector3 originalAcceleration;
	abfw::Vector3 originalMaxSpeed;
public:
	virtual void enter(Enemy*);

	virtual void exit (Enemy*);

	virtual void execute(Enemy*,float dTime);
};