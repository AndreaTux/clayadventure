#pragma once
#include "State.h"
#include <string>
#include <map>

template <class T>
class FiniteStateMachine{

protected:
	State<T>* currentState;
	std::map<std::string,State<T>*> states;
	//needed to see if we are waiting for some event
	bool inTransition;

public:
	FiniteStateMachine(State<T>* initState);
	FiniteStateMachine();
	void changeState(T* type,State<T>*);
	void changeState(T* type, std::string stateName);
	virtual void update (T*,float dTime);
};

template <class T>
FiniteStateMachine<T>::FiniteStateMachine()
	:currentState(0),inTransition(false){}

template <class T>
FiniteStateMachine<T>::FiniteStateMachine(State<T>* init)
	:currentState(init),inTransition(false){}

template <class T>
void FiniteStateMachine<T>::changeState(T* enemy, State<T>* nextState){
	if (currentState != 0){
		currentState->exit(enemy);
		currentState = 0;
	}
	if (nextState != 0){
		inTransition = false;
		currentState = nextState;
		currentState->enter(enemy);
	}
}

template <class T>
void FiniteStateMachine<T>::changeState(T* e, std::string s){
	if (states.find(s) != states.end()){
		changeState(e,states[s]);
	}
}

template <class T>
void FiniteStateMachine<T>::update(T* e, float dTime){
	if (currentState != 0){
		if (!inTransition && currentState->transitionFunctionExists()){
			currentState->checkTransitionFunction();
		}
		if (!inTransition){
			currentState->execute(e,dTime);
		}
	}
}