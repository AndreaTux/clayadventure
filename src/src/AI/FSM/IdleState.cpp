#include "IdleState.h"
#include "..\..\BL\objects\Enemy.h"
#include "..\..\util\RandomGenerator.h"
#include "..\..\BL\EventHandler.h"

IdleState::IdleState()
	:timeToThink(2),timeCounter(0) {}

IdleState::IdleState(std::function<void()> func)
	:State(func),timeToThink(2),timeCounter(0){}

void IdleState::enter(Enemy* enemy){
	enemy->deleteTargetPoint();
}

void IdleState::exit (Enemy* enemy){
	enemy->deleteTargetPoint();
}

void IdleState::execute(Enemy* enemy,float dTime){
	timeCounter -= dTime;
	if (timeCounter <= 0){
		//if it doesn't have a target point
		if (enemy->targetPoint == 0){
			//choose randomly if move or not
			if (RandomGenerator::getRandomInteger(2) == 0){
				//decide the amount of movement
				int fow = enemy->fieldOfWalk.x;
				if (fow > 0){
					int movementX = RandomGenerator::getRandomInteger(fow);
					//decide a direction
					//int direction = RandomGenerator::getRandomInteger(2);
					int direction = 0;
					if (direction == 1){
						//turn around
						enemy->setDirection(abfw::Vector3(-1,0,0));
					}else{
						enemy->setDirection(abfw::Vector3(1,0,0));
					}
					enemy->targetPoint = new abfw::Vector3(enemy->position.x + (enemy->direction.x * movementX),enemy->position.y,enemy->position.z);
				}
			}
		}else{
			//else, if we already have a target point
			//monster directed right
			if (enemy->direction.x > 0 && enemy->position.x >= enemy->targetPoint->x ||
				//monster directed left
				enemy->direction.x < 0 && enemy->position.x <= enemy->targetPoint->x){

				enemy->deleteTargetPoint();
				enemy->direction = abfw::Vector3(0,0,0);
				enemy->currentSpeed = abfw::Vector3(0,0,0);
				timeCounter = timeToThink + 10;
				EventHandler::getInstance()->playAnimation(enemy->name,"idle_" + enemy->getStringFacing(),true);
			}

			if(enemy->hitABlock){
				enemy->hitABlock = false;
				enemy->deleteTargetPoint();
				enemy->direction = abfw::Vector3(0,0,0);
				enemy->currentSpeed = abfw::Vector3(0,0,0);
				EventHandler::getInstance()->playAnimation(enemy->name,"idle_" + enemy->getStringFacing(),true);

			}
		}
	}
}