#pragma once
#include "State.h"

class IdleState : public State<Enemy>{
private:
	float timeToThink;
	float timeCounter;
public:
	IdleState(std::function<void()>);

	IdleState();

	virtual void enter(Enemy*);

	virtual void exit (Enemy*);

	virtual void execute(Enemy*,float dTime);
};