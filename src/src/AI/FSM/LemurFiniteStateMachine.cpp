#include "LemurFiniteStateMachine.h"
#include "ShootState.h"
#include "IdleState.h"
#include "..\..\BL\objects\Enemy.h"
#include "..\..\BL\EventHandler.h"

LemurFiniteStateMachine::LemurFiniteStateMachine(Enemy* e){
	State<Enemy>* idleState	 = new IdleState;
	State<Enemy>* shootingState = new ShootState;

	idleState->setTransitionFunction([e,this]{
		//capture player position
			abfw::Vector3 playerPosition = e->level->getGameObjectWithName("player")->getPosition();
			if (playerPosition.x <= e->position.x + e->fieldOfView.x && playerPosition.x >= e->position.x - e->fieldOfView.x){
				EventHandler::getInstance()->playAnimation(e->name,"spotPlayer_" + e->getStringFacing(),false);
				this->currentState->exit(e);
				this->currentState = 0;
				this->inTransition = true;
				//this->changeState(e,this->states["shooting"]);
			}
		});

	shootingState->setTransitionFunction([e,this]{
		abfw::Vector3 playerPosition = e->level->getGameObjectWithName("player")->getPosition();
		if (playerPosition.x > e->position.x + e->fieldOfView.x || playerPosition.x < e->position.x - e->fieldOfView.x){
				this->changeState(e,this->states["idle"]);
		}
	});

	states.insert(std::pair<std::string,State<Enemy>*>("shooting",shootingState));
	states.insert(std::pair<std::string,State<Enemy>*>("idle", idleState));

	changeState(e,idleState);
	animationEvents = new LemurAnimationEvents(e->name);
}

LemurFiniteStateMachine::~LemurFiniteStateMachine(){
	for (std::map<std::string,State<Enemy>*>::iterator it = states.begin(); it != states.end(); it++){
		delete (*it).second;
	}
	delete animationEvents;
}


LemurAnimationEvents::LemurAnimationEvents(std::string owner){
	EventHandler::getInstance()->addListener(owner,this);
}

void LemurAnimationEvents::animationFinished(std::string animName, Enemy* e){
	if (animName == "spotPlayer"){
		e->getStateMachine()->changeState(e,"shooting");
	}
	
}
