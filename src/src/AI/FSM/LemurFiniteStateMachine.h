#pragma once
#include "FiniteStateMachine.h"
#include "..\..\BL\EnemyAnimationEvents.h"

class LemurFiniteStateMachine : public FiniteStateMachine<Enemy>{
public:
	LemurFiniteStateMachine(Enemy*);
	~LemurFiniteStateMachine();

private:
	EnemyAnimationEvents* animationEvents;
};

class LemurAnimationEvents : public EnemyAnimationEvents{
public:
	LemurAnimationEvents(std::string);
	void animationFinished(std::string, Enemy*);
};