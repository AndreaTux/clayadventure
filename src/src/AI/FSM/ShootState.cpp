#include "ShootState.h"
#include "..\..\BL\objects\ParabolicBullet.h"
#include "..\..\BL\EventHandler.h"
#include "..\..\BL\objects\Enemy.h"

void ShootState::enter(Enemy*){
	timeCounter = 4.f;
}

void ShootState::exit (Enemy*){}

void ShootState::execute(Enemy* e,float dTime){
	timeCounter -= dTime;
	if (timeCounter <= 0){
		GameObject* player = e->level->getGameObjectWithName("player");
		abfw::Vector3 target = player->getPosition();
		//player left side
		if (player->getPosition().x <= e->getPosition().x)
			e->setFacing(-1);
		else
			e->setFacing(1);
		EventHandler::getInstance()->playAnimation(e->getName(),"attack_" + e->getStringFacing(),false);
		ParabolicBullet* bullet = new ParabolicBullet(target,abfw::Vector3(e->getPosition().x + 60, e->getPosition().y,0),abfw::Vector3(e->facing,0,0),2,e->level);
		EventHandler::getInstance()->addObjectToGame(bullet,"projectile.png");
		timeCounter = 4.f;
	}

}