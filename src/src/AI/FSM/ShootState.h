#pragma once
#include "State.h"

class ShootState : public State<Enemy>{
private:
	float timeCounter;
public:
	void enter(Enemy*);

	void exit (Enemy*);

	void execute(Enemy*,float dTime);
};