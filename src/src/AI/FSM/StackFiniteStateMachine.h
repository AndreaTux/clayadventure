#pragma once
#include <list>
#include "State.h"
#include <map>

template <class T>
class StackFiniteStateMachine{
protected:
	std::map <std::string, State<T>*> states;
	std::list<State<T>*> stateStack;
	State<T>* previousState;

public:
	StackFiniteStateMachine();
	//destructor missing
	StackFiniteStateMachine(State<T>* initState);

	void update (T* type, float dTime);
	State<T>* popState ();
	void pushState(State<T>* state);
	void addState(State<T>* state);
	State<T>* frontState();

	State<T>* getStateFromString(std::string);

};

template <class T>
StackFiniteStateMachine<T>::StackFiniteStateMachine()
	:previousState(0){}

template <class T>
StackFiniteStateMachine<T>::StackFiniteStateMachine(State<T>* init)
	:previousState(0){
	pushState(init);
}

template <class T>
void StackFiniteStateMachine<T>::addState(State<T>* s){
	states[s->getName()] = s;
}

template <class T>
State<T>* StackFiniteStateMachine<T>::popState(){
	return stateStack.pop_front();
}

template <class T>
void StackFiniteStateMachine<T>::pushState(State<T>* state){
	stateStack.push_front(state);
}

template<class T>
State<T>* StackFiniteStateMachine<T>::frontState(){
	return stateStack.front();
}

template<class T>
State<T>* StackFiniteStateMachine<T>::getStateFromString(std::string name){
	return states[name];
}
