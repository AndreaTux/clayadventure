#pragma once
#include <functional>
class Enemy;

template <class T>
class State{
protected:
	std::function<void()> transitionFunction;
	std::string name;
public:
	State();

	State(std::function<void()> transitionFunction);

	State(std::string name);

	virtual void enter(T*) = 0;

	virtual void exit (T*) = 0;

	virtual void execute(T*,float dTime) = 0;

	std::string getName();

	void checkTransitionFunction();

	void setTransitionFunction(std::function<void()>);

	bool transitionFunctionExists();
};

template <class T>
State<T>::State()
	:transitionFunction(0){}

template <class T>
State<T>::State(std::string n)
	:name(n),transitionFunction(0){}

template <class T>
State<T>::State(std::function<void()> fun)
	:transitionFunction(fun){
}
template <class T>
void State<T>::setTransitionFunction(std::function<void()> function){
	transitionFunction = function;
}
template <class T>
void State<T>::checkTransitionFunction(){
	transitionFunction();
}
template <class T>
bool State<T>::transitionFunctionExists(){
	return transitionFunction != 0;
}

template <class T>
std::string State<T>::getName(){
	return name;
}