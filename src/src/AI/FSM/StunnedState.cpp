#include "StunnedState.h"
#include "IdleState.h"
#include "..\..\BL\objects\Enemy.h"
#include "..\..\BL\EventHandler.h"

StunnedState::StunnedState(float t)
	:timeOfStunning(t),timeCounter(t){
}

void StunnedState::enter(Enemy* obj){
	EventHandler::getInstance()->playAnimation(obj->getName(),"stunned_" + obj->getStringFacing(),true);
}

void StunnedState::exit (Enemy*){
	timeCounter = timeOfStunning;
}

void StunnedState::execute(Enemy* e,float dTime){
	timeCounter -= dTime;
}

float StunnedState::getTimeCounter(){
	return timeCounter;
}