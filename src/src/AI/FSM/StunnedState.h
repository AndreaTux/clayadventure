#pragma once
#include "State.h"

class StunnedState :public State<Enemy>{
private:
	float timeOfStunning;
	float timeCounter;
public:
	StunnedState(float timeOfStunning = 2);

	virtual void enter(Enemy*);

	virtual void exit (Enemy*);

	virtual void execute(Enemy*,float dTime);

	float getTimeCounter();

};