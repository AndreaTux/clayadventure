#include "AudioManager.h"

AudioManager* AudioManager::instance = 0;

AudioManager* AudioManager::getInstance(){
	if (instance == 0){
		instance = new AudioManager;
	}
	return instance;
}

void AudioManager::shutdown(){
	system->release();
	/*if (instance != 0){
		delete instance;
	}*/

	
}

AudioManager::AudioManager(){
	//Init the system and check for errors
	if(System_Create(&system) != FMOD_OK){
		// Report Error
	}
	int driverCount = 0;
    system->getNumDrivers(&driverCount);
 
    if (driverCount == 0)
    {
       // Report Error
       return;
    }
 
    // Initialize with 36 Channels
    system->init(36, FMOD_INIT_NORMAL, 0);
}



void AudioManager::init(){
	// for each sound
	Sound* sound;
	Sound* sound2;
	Sound* sound3;
	Sound* sound4;
	Sound* sound5;
	Sound* sound6;
	Sound* sound7;
	Sound* sound8;
	Sound* sound9;
	Sound* sound10;
	Sound* sound11;
	Sound* sound12;
	Sound* sound13;
	Sound* sound14;
	Sound* sound15;
	Sound* sound16;
	Sound* sound17;



	system->createSound("cave.mp3",FMOD_HARDWARE,0,&sound);
	system->createSound("shoot.mp3",FMOD_HARDWARE,0,&sound2);
	system->createSound("ball_jump.mp3",FMOD_HARDWARE,0,&sound3);
	system->createSound("screen_selection.mp3",FMOD_HARDWARE,0,&sound4);
	system->createSound("ball_roll.mp3",FMOD_HARDWARE,0,&sound5);
	system->createSound("ClayPickup.mp3",FMOD_HARDWARE,0,&sound6);
	system->createSound("hit.mp3",FMOD_HARDWARE,0,&sound7);
	system->createSound("ClayFootstepSlow1.mp3",FMOD_HARDWARE,0,&sound8);
	system->createSound("ClayFootstepSlow2.mp3",FMOD_HARDWARE,0,&sound9);
	system->createSound("Crash.mp3",FMOD_HARDWARE,0,&sound10);
	system->createSound("PaperShuffle.mp3",FMOD_HARDWARE,0,&sound11);
	system->createSound("BallLand2.mp3",FMOD_HARDWARE,0,&sound12);
	system->createSound("PaperMoveLoop.mp3",FMOD_HARDWARE,0,&sound13);
	system->createSound("ClaySquish.mp3",FMOD_HARDWARE,0,&sound14);
	system->createSound("PlantHit.mp3",FMOD_HARDWARE,0,&sound15);
	system->createSound("Paper_valley.mp3",FMOD_HARDWARE,0,&sound16);
	system->createSound("trololo.mp3",FMOD_HARDWARE,0,&sound17);





	


	sounds.insert(std::pair<std::string,Sound*>("cave",sound));
	sounds.insert(std::pair<std::string,Sound*>("shoot",sound2));
	sounds.insert(std::pair<std::string,Sound*>("collect_clay",sound6));
	sounds.insert(std::pair<std::string,Sound*>("ball_jump",sound3));
	sounds.insert(std::pair<std::string,Sound*>("hit",sound7));
	sounds.insert(std::pair<std::string,Sound*>("ball_moving",sound5));
	sounds.insert(std::pair<std::string,Sound*>("screen_selection",sound4));
	sounds.insert(std::pair<std::string,Sound*>("footstep_ranged",sound8));
	sounds.insert(std::pair<std::string,Sound*>("footstep_melee",sound9));
	sounds.insert(std::pair<std::string,Sound*>("break",sound10));
	sounds.insert(std::pair<std::string,Sound*>("paper_walking",sound11));
	sounds.insert(std::pair<std::string,Sound*>("player_landing",sound12));
	sounds.insert(std::pair<std::string,Sound*>("paper_loop",sound13));
	sounds.insert(std::pair<std::string,Sound*>("ball_squish",sound14));
	sounds.insert(std::pair<std::string,Sound*>("plant_hit",sound15));
	sounds.insert(std::pair<std::string,Sound*>("screen_theme",sound16));
	sounds.insert(std::pair<std::string,Sound*>("trololo",sound17));


		

}

void AudioManager::play(std::string name,bool loop){
	if(loop){
		sounds[name]->setMode(FMOD_LOOP_NORMAL);
		sounds[name]->setLoopCount(-1);
	}
	Channel* channel;
	system->playSound(FMOD_CHANNEL_FREE,sounds[name], false, &channel);
	channels[name] = channel;
	
}

void AudioManager::stop(std::string name){
	if (channels.find(name) != channels.end()){
		channels[name]->stop();
	}
}

Sound* AudioManager::getSound(std::string name){
	return sounds[name];
}

bool AudioManager::isPlaying(std::string name){
	bool result;
	channels[name]->isPlaying(&result);
	return result;
}

void AudioManager::objectMoved(std::string name){
	std::string soundName = "";

	if (name == "ball"){
		soundName = "ball_squish";
	}
	if (name == "melee"){ 
		soundName = "footstep_melee";
	}
	if( name == "ranged"){
		soundName = "footstep_ranged";
	}
	if (name == "bird"){
		soundName = "footstep_melee";
	}

	if (!isPlaying(soundName) && soundName != ""){
		play(soundName,true);
		if (name == "ranged"){
			channels["footstep_ranged"]->setFrequency(60000);
		}
	}
}

void AudioManager::objectStopped(std::string name){
	std::string soundName = "";

	if (name == "ball"){
		soundName = "ball_squish";
	}
	if (name == "melee"){
		soundName = "footstep_melee";
	}
	if (name == "ranged"){
		soundName = "footstep_ranged";
	}

	if (soundName != ""){
		channels[soundName]->stop();
	}
}

void AudioManager::update(float dTime){
	if (!fadeOutChannels.empty()){
		//improve me 
		for (std::vector<Channel*>::iterator it = fadeOutChannels.begin(); it != fadeOutChannels.end(); ){
			float currentvolume;
			(*it)->getVolume(&currentvolume);			
			float currentChange = dTime * 1;
			(*it)->setVolume(currentvolume - currentChange);
			if (currentvolume <= 0){
				(*it)->stop();
				it = fadeOutChannels.erase(it);
			}else{
				it++;
			}
		}
	}
	// shold set a max volume
	if (!fadeInChannels.empty()){
		for (std::vector<Channel*>::iterator it = fadeInChannels.begin(); it != fadeInChannels.end(); ){
			float currentvolume;
			float currentChange = dTime * 1;
			(*it)->getVolume(&currentvolume);
			(*it)->setVolume(currentvolume + currentChange);
			if (currentvolume >= 1){
				it = fadeInChannels.erase(it);
			}else{
				it++;
			}
		}
		
	}
	

	system->update();

}

void AudioManager::fadeOut(std::string channelName){
	//TODO: check for its existance!
	fadeOutChannels.push_back(channels[channelName]);
}

void AudioManager::fadeIn(std::string channelName){
	fadeInChannels.push_back(channels[channelName]);
}

void AudioManager::setVolume(std::string soundName, float value){
	if (channels.find(soundName) != channels.end()){
		channels[soundName]->setVolume(value);
	}
}
