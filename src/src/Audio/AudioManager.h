#pragma once
#include <conio.h>
#include <Fmod\fmod.hpp>
#include <Fmod\fmod_errors.h>
#include <string>
#include <map>
#include <vector>

using namespace FMOD;

class AudioManager{
private:
	System* system;
	std::map<std::string,Sound*> sounds;
	std::map<std::string,Channel*> channels;
	std::map<std::string, int> playingSounds;
	std::vector<Channel*> fadeOutChannels;
	std::vector<Channel*> fadeInChannels;

	static AudioManager* instance;

	AudioManager(); 

public:
	static AudioManager* getInstance();

	void shutdown();

	void update(float dTime);
	void play(std::string name,bool loop);
	void stop(std::string name);
	Sound* getSound(std::string);
	void objectMoved(std::string name);
	void objectStopped(std::string name);
	bool isPlaying(std::string);
	void setVolume(std::string, float value);
	void fadeOut (std::string channelname);
	void fadeIn (std::string channelname);
	void init();
};