#pragma once
#include <input/sony_controller_input_manager.h>
#include <Windows.h>
#include "platform/windows/KeyboardCodes.h"
#include <string>
class Controls{
public:

	//should be constant...
	static int K_LEFT;
	static int K_RIGHT;
	static int JUMP;
	static int TRANSFORMATION_1;
	static int TRANSFORMATION_2;
	static int TRANSFORMATION_3;
	static int TRANSFORMATION_4;
	static int ACTION;
	static int SELECT;
	static int TOGGLE_DEBUGMODE;
	static int CHEAT;


	//TODO these are hardcoded; should pick them from a config file

static void initControls(std::string controllerType){
	if (controllerType == "ps4"){
		K_LEFT				= ABFW_SONY_CTRL_LEFT;
		K_RIGHT				=ABFW_SONY_CTRL_RIGHT;
		JUMP				= ABFW_SONY_CTRL_L1;
		TRANSFORMATION_1	= ABFW_SONY_CTRL_SQUARE;
		TRANSFORMATION_2	= ABFW_SONY_CTRL_TRIANGLE;
		TRANSFORMATION_3	= ABFW_SONY_CTRL_CIRCLE;
		TRANSFORMATION_4	= ABFW_SONY_CTRL_CROSS;
		ACTION				= ABFW_SONY_CTRL_R1;
		TOGGLE_DEBUGMODE	= ABFW_SONY_CTRL_L3;
		SELECT				= ABFW_SONY_CTRL_TOUCH_PAD;
		CHEAT				= ABFW_SONY_CTRL_R3;
	}else{
	//fallback into keyboard
		K_LEFT				= VK_A;
		K_RIGHT				= VK_D;
		JUMP				= VK_SPACE;
		TRANSFORMATION_1	= VK_LEFT;
		TRANSFORMATION_2	= VK_UP;
		TRANSFORMATION_3	= VK_RIGHT;
		TRANSFORMATION_4	= VK_DOWN;
		ACTION				= VK_E;
		TOGGLE_DEBUGMODE	= VK_Q;
		SELECT				= VK_RETURN;
		CHEAT				= VK_Q;
	}
}
};