#pragma once
#include<string>

class Enemy;
class EnemyAnimationEvents{
public:
	virtual void animationFinished(std::string animationName,Enemy*) = 0;

};