#include "EventHandler.h"
#include "objects\Bullet.h"
#include "..\BL\objects\Enemy.h"
#include "..\sprite_app.h"

EventHandler* EventHandler::instance = 0;
GUIEventHandler* EventHandler::guiEventHandler = 0;

EventHandler* EventHandler::getInstance(){
	if (instance == 0){
		instance = new EventHandler;
	}
	return instance;
}

EventHandler::EventHandler(){
}
	
void EventHandler::init(Player* p, Level* l,AudioManager* audioManager,InputManager* inputManager,SpriteApp* a){
	guiEventHandler = GUIEventHandler::getInstance();
	player = p;
	level = l;
	audio = audioManager;
	input = inputManager;
	app = a;
}

void EventHandler::cleanUp(){
	//delete instance;
}

void EventHandler::objectMoved(std::string id, abfw::Vector3 delta, bool byUser){
	
	guiEventHandler->objectMoved(id,delta,byUser);
	if (id == "player"){
		id = player->getCurrentTransformation()->getName();
	}
	//play the sound if the player has moved but is not falling or jumping
	if (delta.x != 0 && !player->isFalling() && !player->isJumping()){
		if (id != "melee" && id != "bird" && byUser)
			audio->objectMoved(id);
	}
	// if the player fell, stop the walking sound
	if (player->isFalling() && id != "melee" && id != "bird" ){
		audio->objectStopped(id);
	}
	 
}

void EventHandler::writeOnScreen(int x, int y, std::string text){
	guiEventHandler->writeOnScreen(x,y,text);
}

void EventHandler::playerChangeForm(std::string oldFormname,std::string formName){
	guiEventHandler->playerChangedForm(oldFormname,formName);
	audio->objectStopped(oldFormname);
}

void EventHandler::playerShoots(){
	player->shoot();
	audio->play("shoot",false);
	
}

void EventHandler::addObjectToGame(GameObject* obj,std::string texturePath){
	level->addGameObject(obj);
	guiEventHandler->addBulletToScene(obj->getName(),texturePath,obj->getPosition());
}

void EventHandler::removeObjectFromGame(std::string name){
	level->removeGameObject(name);
	guiEventHandler->removeSceneObject(name);
}

void EventHandler::addExternalForceToPlayer(abfw::Vector3 force, abfw::Vector3 intensity){
	player->addExternalForce(force,intensity);
}

void EventHandler::collectClay(){
	player->gainPoints(50);
	audio->play("collect_clay",false);
	if (player->getSize() < player->getMaxSize()){
		float newSize = player->grow();
		guiEventHandler->scaleObj("player",newSize);
	}
}

void EventHandler::blockPlayerForCollision(const CollisionBox* collider){
	player->block(collider);
}

void EventHandler::objectStopped(std::string name){
	guiEventHandler->objectStopped(name);
	if (name == "player"){
		name = player->getCurrentTransformation()->getName();
	}
	audio->objectStopped(name);
}

void EventHandler::playerGetsHit(){
	//check for size
	if (!player->isInvulnerable()){
		if (player->getSize() > 1){
			guiEventHandler->scaleObj("player",player->shrink());
			GUIEventHandler::getInstance()->objectHit("player");

		}else{
			this->removeObjectFromGame("player");
			audio->objectStopped(player->getCurrentTransformation()->getName());
			app->displayDeathScreen();
		}
	}
}

void EventHandler::playerSmashTriggered(){
	//player->smash();
	guiEventHandler->playerAction(player->getFacing());
}

void EventHandler::playerSmash(){
	player->smash();
}

void EventHandler::putPlayerToCheckpoint(){
	player->goToCheckPoint();
	guiEventHandler->objectRespawn("player");
	audio->objectStopped(player->getCurrentTransformation()->getName());
}

void EventHandler::setPlayerCheckpoint(abfw::Vector3 point){
	player->setCheckpoint(point);
}

void EventHandler::attachPlayerAsChild(GameObject* father){
	if (father->getPosition().y >= player->getPosition().y){
		father->attachChild(player);
		player->addFather(father);
	}
}

void EventHandler::learnTransformation(Transformation* transformation){
	transformation->getCollisionBox()->scale(player->getSize());
	player->addTransformation(transformation);
	input->addTransformationMapping(transformation->getName());
	guiEventHandler->powerupCollected(transformation->getName());
	audio->play("collect_clay",false);
}

void EventHandler::beginGame(){
	audio->play("screen_selection",false);
	audio->fadeOut("screen_theme");
}

void EventHandler::enemyKilled(std::string enemyRace){
	//this is ugly. What can be done instead, is to load
	//per-race sounds and play a generic "dead" sound
	if (enemyRace == "rose" || enemyRace == "thorn"){
		audio->play("plant_hit",false);
	}else{
		audio->play("hit",false);
	}
}
/*
void EventHandler::playerJumped(){
	player->intentionToJump();
	guiEventHandler->jumpRequest();
}*/

void EventHandler::playerJumps(){
	//player->jump();
	audio->play("ball_jump",false);
	audio->objectStopped(player->getCurrentTransformation()->getName());

	guiEventHandler->jumpRequest();
}

void EventHandler::blockForCollision(std::string who, const CollisionBox* collider){
	dynamic_cast<AbstractCharacter*>(level->getGameObjectWithName(who))->block(collider);
}

void EventHandler::setDebugMode(bool debug){
	guiEventHandler->setDebugMode(debug);
}

void EventHandler::playerAction(){
	guiEventHandler->playerAction(player->getFacing());
}

void EventHandler::objectLanded(std::string name, int side){
	std::string sideStr = "left";
	if (side == 1){
		sideStr = "right";
	}
	guiEventHandler->objectLanded(name,sideStr);
	if (name == "player"){
 		AudioManager::getInstance()->play("player_landing",false);
	}
}

void EventHandler::playAnimation(std::string who, std::string animation, bool loop){
	guiEventHandler->playAnimation(who,animation,loop);
}

void EventHandler::addListener(std::string id,EnemyAnimationEvents* listener){
	enemyListeners[id] = listener;
}

void EventHandler::animationFinished(std::string who, std::string animation){
	for(std::map<std::string,EnemyAnimationEvents*>::iterator it = enemyListeners.begin(); it != enemyListeners.end(); it++){
		if (it->first == who){
			Enemy* enemy = (Enemy*) (level->getGameObjectWithName(who));
			if (enemy != 0){
				//get rid of the side
				std::string animationName = animation.substr(0,animation.find("_"));
				it->second->animationFinished(animationName,enemy);
			}
		}
	}
}

void EventHandler::objectFalling(std::string name){
	GUIEventHandler::getInstance()->objectFalling(name);
}

void EventHandler::objectRemoved(std::string who){
	level->removeGameObject(who);
}

void EventHandler::levelEnded(){
	this->removeObjectFromGame("player");
	audio->objectStopped(player->getCurrentTransformation()->getName());
	app->displayEndLevelScreen();
}

void EventHandler::gameFinished(){
	app->setGameFinished();
}

void EventHandler::invulerabilityEnded(){
	guiEventHandler->invulnerabilityEnded();
}

void EventHandler::playerAttackStarted(){
	player->setAttacking(true);
}

void EventHandler::playerAttackEnded(){
	player->setAttacking(false);
}

void EventHandler::firstTimeTransformation(std::string name){
	guiEventHandler->firstTimeTransformation(name);
}

