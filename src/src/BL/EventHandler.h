#pragma once
#include "..\GUI\GUIEventHandler.h"
#include "Level.h"
#include "objects\player\player.h"
#include "..\Audio\AudioManager.h"
#include "InputManager.h"
#include <vector>

class EnemyAnimationEvents;
class SpriteApp;

class EventHandler{
private:
	static EventHandler* instance;
	static GUIEventHandler* guiEventHandler;
	std::map <std::string,EnemyAnimationEvents*> enemyListeners;
	Player* player;
	Level* level;
	AudioManager* audio;
	InputManager* input;
	SpriteApp* app;
	
	EventHandler();

public:
	static EventHandler* getInstance();
	
	void init(Player*,Level* l,AudioManager* a,InputManager* i, SpriteApp* app);
	void cleanUp();

	void writeOnScreen(int x, int y, std::string text);
	void playerChangeForm (std::string oldFormName, std::string formName);
	void objectMoved(std::string,abfw::Vector3 delta,bool byUser = true);

	void playerShoots();
	void playerSmashTriggered();
	void playerSmash();
	void addObjectToGame(GameObject*,std::string texturePath);
	//From BL
	void removeObjectFromGame(std::string);
	//From GUI
	void objectRemoved (std::string);

	void addExternalForceToPlayer(abfw::Vector3,abfw::Vector3);

	void collectClay();
	void playerGetsHit();
	void blockPlayerForCollision(const CollisionBox*);

	void objectStopped(std::string name);
	void putPlayerToCheckpoint();
	void setPlayerCheckpoint(abfw::Vector3);

	void attachPlayerAsChild (GameObject* father);
	void learnTransformation (Transformation*);

	void beginGame();
	void enemyKilled(std::string enemyRace);
	void playerJumped();
	void playerJumps();

	void blockForCollision(std::string who, const CollisionBox* collider);
	void playerAction();

	void setDebugMode(bool);
	void objectLanded(std::string name,int side);
	void playAnimation(std::string name, std::string animationName, bool loop);

	void animationFinished(std::string who, std::string animation);

	void addListener(std::string,EnemyAnimationEvents*);

	void objectFalling(std::string name);

	void gameFinished();
	void levelEnded();

	void invulerabilityEnded();

	void playerAttackStarted();
	void playerAttackEnded();
	void firstTimeTransformation(std::string transformatioName);

};