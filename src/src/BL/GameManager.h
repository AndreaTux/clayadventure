#pragma once
#include "Level.h"
#include "..\BL\objects\player\player.h"

class GameManager{
private:
	static GameManager* instance;
	Level* currentLevel;
	Player* player;

	GameManager();
	void shutdown();
public:
	static GameManager* getInstance();
	void loadResources();
	void instanceLevel(Level*);
	void initPlayer(Player*);
	
};