#include "InputManager.h"
#include <iostream>
#include <maths/vector3.h>
#include "Controls.h"
#include <Windows.h>
#include "EventHandler.h"
#include "..\sprite_app.h"

InputManager::InputManager(Player* p,SpriteApp* a)
:player(p),app(a),debugMode(false){
	for (int i = 0; i < 256; i++){
		buttons[i] = false;
	}
	titleScreen = true;
	deathScreen = levelFinishedScreen = instructionScreen = false;

	transformationMapping.insert(std::pair<int,const std::string>(Controls::TRANSFORMATION_2,"ball"));
	transformationUse["ball"] = true;
	transformationUse["ball"] = true;

}

InputManager::~InputManager(){}

void InputManager::update(float dTime){
	abfw::Vector3 finalDirection(0,0,0);
	
	if(!isScreenDisplayed()){
		//check the directional buttons only if the player is not attacking or it is attacking but
		//it is on air (jumping or falling)
		if (!player->isAttacking() || (player->isAttacking() && (player->isFalling() || player->isJumping()))){
			finalDirection += checkDirectionalButton(Controls::K_LEFT,abfw::Vector3(-1,0,0));
			finalDirection += checkDirectionalButton(Controls::K_RIGHT,abfw::Vector3(1,0,0));
		}else{
			//reset the buttons states
			buttons[Controls::K_LEFT] = buttons[Controls::K_RIGHT] = false;
			//reset the x direction
			player->addDirection(abfw::Vector3(player->getDirection().x * -1, 0, 0));
		}
		finalDirection += checkJump();
		checkTransformation(Controls::TRANSFORMATION_1);
		checkTransformation(Controls::TRANSFORMATION_2);
		checkTransformation(Controls::TRANSFORMATION_3);
		checkTransformation(Controls::TRANSFORMATION_4);
		//actions
		if(checkButtonPressed(Controls::ACTION) && !buttons[Controls::ACTION]){
			buttons[Controls::ACTION] = true;
			player->action();
		}else 
			if(checkButtonReleased(Controls::ACTION) && buttons[Controls::ACTION]){
				buttons[Controls::ACTION] = false;
		}
		//debug mode
			if(checkButtonPressed(Controls::TOGGLE_DEBUGMODE) && !buttons[Controls::TOGGLE_DEBUGMODE]){
				debugMode = !debugMode;
				EventHandler::getInstance()->setDebugMode(debugMode);
				buttons[Controls::TOGGLE_DEBUGMODE] = true;
			}else
			if(checkButtonReleased(Controls::TOGGLE_DEBUGMODE) && buttons[Controls::TOGGLE_DEBUGMODE]){
				buttons[Controls::TOGGLE_DEBUGMODE] = false;
				
			}
	}
	//instruction screen
	if (instructionScreen && !buttons[Controls::SELECT] && checkButtonPressed(Controls::SELECT)){
		EventHandler::getInstance()->beginGame();
		app->beginGame();		
		instructionScreen = false;
	}else{
		if (instructionScreen && buttons[Controls::SELECT] && !checkButtonPressed(Controls::SELECT)){
			buttons[Controls::SELECT] = false;
		}
	}
	//title screen
	if (titleScreen && checkButtonPressed(Controls::SELECT)){
		AudioManager::getInstance()->play("screen_selection",false);		
		titleScreen = false;
		app->displayInstructionScreen();
		instructionScreen = true;
		buttons[Controls::SELECT] = true;
	}
	
	//death screen
	if ( (deathScreen || levelFinishedScreen) && checkButtonPressed(Controls::SELECT)){
		EventHandler::getInstance()->gameFinished();
	}

	//if there are changes in direction
	if (!(finalDirection.x == 0 && finalDirection.y == 0)){
		//update the player
		player->addDirection(finalDirection);
	}
	
}

abfw::Vector3 InputManager::checkDirectionalButton(int buttonCode, abfw::Vector3 dir){
	//if the button is down 
	if (checkButtonPressed(buttonCode) && !buttons[buttonCode]){
		//store that the user is pushing it
		buttons[buttonCode] = true;
		return dir;
	}else
		//if the button has been released
		if (checkButtonReleased(buttonCode) && buttons[buttonCode]){
			buttons[buttonCode] = false;
			return dir * -1;
	}
	return abfw::Vector3(0,0,0);
}

abfw::Vector3 InputManager::checkJump(){
	if (checkButtonPressed(Controls::JUMP) && !buttons[Controls::JUMP] && !player->isFalling() && !player->isJumping()){
		//store that the user is pushing it
		buttons[Controls::JUMP] = true;
		//keyframe test
		//EventHandler::getInstance()->playerJumped();
		player->jump();
		return abfw::Vector3(0,-1,0);
	}
	else
		if (checkButtonReleased(Controls::JUMP) && buttons[Controls::JUMP]){
			buttons[Controls::JUMP] = false;
			//return abfw::Vector3(0,1,0);
	}
	return abfw::Vector3(0,0,0);
}

void InputManager::checkTransformation(int button){
	if (checkButtonPressed(button) && !buttons[button]){
		std::map<int,const std::string>::iterator it = transformationMapping.find(button);
		if (it != transformationMapping.end()){
			std::string transformationName = it->second;
			buttons[button] = true;
			if (player->getCurrentTransformation()->getName() != transformationName){
				//change form 
				player->changeForm(transformationName);
				//it's the first time that we use this transformation
				if (transformationUse[transformationName] == true){
					EventHandler::getInstance()->firstTimeTransformation(transformationName);
				}
			}

			else{
				if (player->getCurrentTransformation()->getName() != "ball")
					//deatcivate form
					player->changeForm("ball");
			}
		}
	}
	/*
	else
		if (!(GetAsyncKeyState(button) & 0x8000f) && buttons[button]){
			buttons[button] = false;
			//EventHandler::getInstance()->playerChangeForm("ball");
			player->changeForm("ball");
		}
	*/

	if (checkButtonReleased(button) && buttons[button]){
		buttons[button] = false;
	}
	
	if (checkButtonPressed(Controls::CHEAT)){
		cheatButtonQueue.clear();
	}
}

bool InputManager::isTitleScreen(){
	return titleScreen;
}

void InputManager::addTransformationMapping(const std::string transformationName){
	//HARDCODED!!
	int keyCode = 0;
	if (transformationName == "melee"){
		keyCode = Controls::TRANSFORMATION_4;
	}else
	if (transformationName == "ranged"){
		keyCode = Controls::TRANSFORMATION_1;
	}else
	if (transformationName == "bird"){
		keyCode = Controls::TRANSFORMATION_3;
	}
	if (keyCode != 0){
		transformationMapping.insert(std::pair<int,const std::string>(keyCode,transformationName));
		transformationUse[transformationName] = true;
	}
}

bool InputManager::isScreenDisplayed(){
	return titleScreen || deathScreen || levelFinishedScreen;
}

void InputManager::setDeathScreen(){
	deathScreen = true;
}

void InputManager::setEndGamescreen(){
	levelFinishedScreen = true;
}

bool InputManager::checkButtonPressed(int button){
	bool result = GetAsyncKeyState(button) & 0x8000f;
	if (result){
		cheatButtonQueue.push_back(button);
		checkCheat();
	}
	return result;
	//return controller->buttons_pressed() & button;
}

bool InputManager::checkButtonReleased(int button){
//	return controller->buttons_released() & button;
	return !(GetAsyncKeyState(button) & 0x8000f);
}

void InputManager::checkCheat(){
	if (cheatButtonQueue.size() >= 8){
		bool good = true;
		for (int i = 0; i < 8 && good; i++){
			//just for fun
			switch (i){
			case 0:
			case 2:
				if (cheatButtonQueue[i] != Controls::K_RIGHT)
					good = false;
				break;
			case 1:
			case 3:
				if (cheatButtonQueue[i] != Controls::K_LEFT)
					good = false;
				break;
			case 5:
			case 7:
				if (cheatButtonQueue[i] != Controls::TRANSFORMATION_1)
					good = false;
				break;
			case 4:
			case 6:
				if (cheatButtonQueue[i] != Controls::TRANSFORMATION_4)
					good = false;
				break;
			default:
				good = false;
		}
	}
	if (good){
		AudioManager::getInstance()->stop("cave");
		AudioManager::getInstance()->play("trololo",true);
		cheatButtonQueue.clear();
	}else{
		cheatButtonQueue.clear();
	}
	}
}