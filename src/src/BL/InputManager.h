#pragma once
#include "objects\player\player.h"
#include <map>
#include "objects\player\transformations\Transformation.h"
#include <system/platform.h>
#include <input/sony_controller_input_manager.h>

class SpriteApp;

class InputManager {
protected:
	Player* player;
	bool buttons [256];
	std::map <int, const std::string> transformationMapping;
	std::map <std::string, bool> transformationUse;
	std::vector<int> cheatButtonQueue;

	SpriteApp* app;
	//need to rework this with an application manager or something similar
	bool titleScreen;
	bool instructionScreen;
	bool levelFinishedScreen;
	bool deathScreen;
	bool debugMode;

	/*
	*checks if the given button has been pressed, if yes the direction vector is returned. If
	*the button is released, this function returns -direction.
	*/
	abfw::Vector3 checkDirectionalButton(int buttonCode, abfw::Vector3 direction);
	abfw::Vector3 checkJump();
	void checkTransformation(int transformationButton);
	bool isScreenDisplayed();
protected:
	virtual bool checkButtonPressed(int button);
	virtual bool checkButtonReleased(int button);
	void checkCheat();
public:
	InputManager (Player*,SpriteApp*);
	virtual ~InputManager();
	bool isTitleScreen();
	virtual void update(float dTime);
	void addTransformationMapping(const std::string transformationName);
	void setDeathScreen();
	void setEndGamescreen();
};