#include "Level.h"
#include "collisions\CollisionSideEnum.h"
#include <string>

Level::Level (float w, float l, int gHeight, abfw::Vector3 g, std::string n)
: width(w), length(l), groundHeight(gHeight), gravity(g), name(n){
	gameObjectsMap = new std::map<std::string,GameObject*>();
	gameObjectsList = new std::vector<GameObject*>();
	scale = 1.f;
}

Level::~Level(){
	delete gameObjectsMap;
}

const float Level::getWidth()const{
	return width;
}
const float Level::getLength()const{
	return length;
}

const int Level::getGroundHeight() const{
	return groundHeight;
}

const abfw::Vector3 Level::getGravity()const{
	return gravity;
}
const std::string Level::getName()const{
	return name;
}

std::map<std::string,GameObject*>* Level::getgameObjectsMap()const{
	return gameObjectsMap;
}

GameObject* Level::getGameObjectWithName (const std::string name) const{
	return (*gameObjectsMap)[name];
}

void Level::addGameObject(GameObject* obj){
	gameObjectsList->push_back(obj);
	gameObjectsMap->insert(std::pair<std::string,GameObject*>(obj->getName(),obj));
}

const std::vector<GameObject*> Level::checkCollisions(const GameObject* obj, int maxNumber)const{
	int count = 0;
	std::vector<GameObject*> result;

	for ( std::vector<GameObject*>::iterator it = gameObjectsList->begin(); it != gameObjectsList->end();it++){
		if ( *it != obj){
			if(obj->checkCollisionWith(*it)){
				result.push_back(*it);
				count++;
			}
		}
		/*
		if (count == maxNumber){
			return result;
		}
		*/
	}
	return result;
}

void Level::update(float dTime){
	if (!toDelete.empty()){
		cleanLevel();
	}
	for ( std::vector<GameObject*>::iterator it = gameObjectsList->begin(); it != gameObjectsList->end(); it ++){
		(*it)->update(dTime);
	}
}

void Level::removeGameObject(std::string name){
	toDelete.push_back(name);
}

void Level::cleanLevel(){
	for (std::vector<std::string>::iterator it = toDelete.begin(); it != toDelete.end(); it++){
		GameObject* toRemove = (*gameObjectsMap)[*it];
		for (std::vector<GameObject*>::iterator it2 = gameObjectsList->begin(); it2 != gameObjectsList->end();){
			if (*it2 == toRemove){
				it2 = gameObjectsList->erase(it2);
				//delete toRemove;
				break;
			}else{
				it2++;
			}
		}
	}
	toDelete.erase(toDelete.begin(),toDelete.end());
}

float Level::getScale(){
	return scale;
}

//TODO: check for the right edge: add the width of the sprite!!
bool Level::inBounds(abfw::Vector2 pos){
	return pos.x <= width && pos.x >= 0 && pos.y <= groundHeight && pos.y >= 0;
}

//improve it by moving the block by the difference of collision (the part of the obj that
//collided)
bool Level::tryToFit(GameObject* obj, int range, int units)const{
	int offset = 0;
	abfw::Vector3 originalPosition = obj->getPosition();
	abfw::Vector3 dir (0,0,0);
	do{
		
		const std::vector<GameObject*> objs = checkCollisions(obj,1);
		std::vector<GameObject*>::const_iterator it = objs.begin();
		if (it == objs.end()){
			return true;
		}
		//really ugly
		const std::string collisionID = (*it)->getCollisionBehaviourID();
		if (collisionID == "clay" || collisionID == "enemy" || collisionID == "block" || collisionID == "breakable"){
			offset += units;
			obj->checkHorizontalCollisionSides(*it);
			//DO THE SAME FOR UP/DOWN and CEILING
			if (obj->getBoundingBox()->isCollidedAtSide(CollisionSide::LEFT)){
				dir = abfw::Vector3(1,0,0);
			}else{
				if (obj->getBoundingBox()->isCollidedAtSide(CollisionSide::RIGHT)){
					dir = abfw::Vector3(-1,0,0);
				}
			}
			obj->translate(dir * units);
		}else{
			return true;
		}
	}while (offset < range);
	obj->translate(obj->getPosition() - originalPosition);
	return false;

}