#pragma once
#include <maths\vector3.h>
#include <iostream>
#include <vector>
#include <map>
#include "objects\GameObject.h"
#include "collisions\CollisionBox.h"
#include <vector>

class Level{
private:
	void cleanLevel();
protected:
	float width;
	float length;
	int groundHeight;
	abfw::Vector3 gravity;
	std::string name;
	float scale;

	std::map<std::string,GameObject*>* gameObjectsMap;
	std::vector<GameObject*>* gameObjectsList;
	std::vector<std::string> toDelete;

public:
	Level (float w, float l, int groundHeight, abfw::Vector3 g, std::string name);
	~Level();

	virtual void update(float dTime);

	void addGameObject(GameObject*);
	void removeGameObject(std::string);

	const float getWidth()const;
	const float getLength()const;
	const int getGroundHeight()const;
	const abfw::Vector3 getGravity()const;
	const std::string getName()const;

	//max number limits the max colliders that we want
	const std::vector<GameObject*> checkCollisions(const GameObject*,int maxNumber = -1)const;

	std::map<std::string,GameObject*>* getgameObjectsMap()const;
	GameObject* getGameObjectWithName (const std::string) const;
	float getScale();
	bool inBounds(abfw::Vector2 pos);
	
	/*
	* This method tries to fit a game object within a certain (specified) range. It returns true
	* if the object can fit somewhere within the range, false otherwise
	*/
	bool tryToFit (GameObject* object, int range, int units)const;
};