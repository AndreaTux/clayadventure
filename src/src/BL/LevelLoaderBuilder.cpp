#include "LevelLoaderBuilder.h"
#include "collisions\collision_behaviour\SimpleBlockCollisionBehaviour.h"
#include "collisions\collision_behaviour\CheckpointCollisionBehaviour.h"
#include "objects\MovingBlock.h"
#include "..\GUI\GUIEventHandler.h"
#include "collisions\collision_behaviour\TransformationClayCollisionBehaviour.h"
#include "objects\player\transformations\MeleeTransformation.h"
#include "objects\player\transformations\BirdTransformation.h"
#include "objects\player\transformations\RangedTransformation.h"
#include "collisions\collision_behaviour\BreakableBlockCollisionBehaviour.h"
#include "collisions\collision_behaviour\SpikeCollisionBehaviour.h"
#include "collisions\collision_behaviour\EnemyCollisionBehaviour.h"
#include "objects\Enemy.h"
#include "..\AI\FSM\IdleState.h"
#include "..\AI\FSM\BullFiniteStateMachine.h"
#include "..\AI\FSM\LemurFiniteStateMachine.h"

void LevelLoaderBuilder::createMonster(ConfigReader* reader){
	//GameObject* obj = createGameObject(reader);
	std::string name	= reader->getString("name");
	float width			= reader->getFloat("width")/result->getScale();
	float height		= reader->getFloat("height")/result->getScale();
	float x				= reader->getFloat("x")/result->getScale();
	float y				= reader->getFloat("y")/result->getScale();
	std::string facing	= reader->getString("side");
	//change this end put in the race file
	std::string resistance = reader->getString("resist");

	abfw::Vector3 fov, fow;
	std::string texturePath = "";
	

	std::string monsterRace = name.substr(0,name.find("_"));
	std::string monsterDefinitionFile = monsterRace + ".xml";
		
	//take the texture
	ConfigReader* monsterReader = new ConfigReader(monsterDefinitionFile);
	texturePath = monsterRace + ".png";
	if (monsterReader->isValid()){
		//texturePath = monsterReader->getConfigReader("texture")->getString("path");
		ConfigReader* temp = monsterReader->getConfigReader("fow");
		fow = abfw::Vector3(temp->getFloat("x"), temp->getFloat("y"), 0);
		temp = monsterReader->getConfigReader("fov");
		fov = abfw::Vector3(temp->getFloat("x"), temp->getFloat("y"), 0);
	}
	Enemy* obj = new Enemy(name,new Status(100,30,30,30,30,180,100,80),abfw::Vector3(x,y,0),abfw::Vector3(0,0,0),abfw::Vector3(180,0,0),fov,fow,result);
	if (facing != ""){
		int facingInt = facing == "right" ? 1 : -1;
		obj->setFacing(facingInt);
	}

	if (resistance != ""){
		if (resistance == "melee")
			obj->setCollisionBehaviour(new EnemyCollisionBehaviour(name,BulletType::MELEE));
		//insert others
	}else{
		obj->setCollisionBehaviour(new EnemyCollisionBehaviour(name));
	}
	obj->setCollisionBox(new CollisionBox(abfw::Vector2(x,y),width,height));
	//AI
	if (monsterDefinitionFile == "bull.xml"){
		obj->setStateMachine(new BullFiniteStateMachine(obj));

	}
	if (monsterDefinitionFile == "lemur.xml"){
		obj->setStateMachine(new LemurFiniteStateMachine(obj));
	}

	//delete monsterReader;
	GUIEventHandler::getInstance()->addObjectToScene(obj->getName(),texturePath,obj->getPosition(),obj->getBoundingBox()->getWidth(),obj->getBoundingBox()->getHeight(),obj->getStringFacing());
	result->addGameObject(obj);

	
}

GameObject* LevelLoaderBuilder::createGameObject(ConfigReader* configReader){
	std::string name	= configReader->getString("name");
	float width			= configReader->getFloat("width")/result->getScale();
	float height		= configReader->getFloat("height")/result->getScale();
	float x				= configReader->getFloat("x")/result->getScale();
	float y				= configReader->getFloat("y")/result->getScale();

	GameObject* gameObject = new GameObject(name,abfw::Vector3(x,y,0));
	gameObject->setCollisionBox(new CollisionBox(abfw::Vector2(x,y),width,height));
	result->addGameObject(gameObject);
	GUIEventHandler::getInstance()->addObjectToDebugList(name,"",abfw::Vector3(x,y,0),width,height);
	return gameObject;
}
//TODO: a lot of replication of code..
MovingGameObject* LevelLoaderBuilder::createMovingGameObject(ConfigReader* configReader){
	std::string name	= configReader->getString("name");
	float width			= configReader->getFloat("width")/result->getScale();
	float height		= configReader->getFloat("height")/result->getScale();
	float x				= configReader->getFloat("x")/result->getScale();
	float y				= configReader->getFloat("y")/result->getScale();
	float velX			= configReader->getFloat("velX");
	float velY			= configReader->getFloat("velY");
	int	  dirX			= configReader->getInteger("dirX");
	int	  dirY			= configReader->getInteger("dirY");

	MovingGameObject* movingGameObject = new MovingGameObject(name,abfw::Vector3(x,y,0),abfw::Vector3(dirX,dirY,0),abfw::Vector3(velX,velY,0),result);
	movingGameObject->setCollisionBox(new CollisionBox(abfw::Vector2(x,y),width,height));
	GUIEventHandler::getInstance()->addObjectToScene(name,"",abfw::Vector3(x,y,0),width,height);
	return movingGameObject;
}

void LevelLoaderBuilder::createMovingBlock(ConfigReader* configReader){
	std::string name	= configReader->getString("name");
	float width			= configReader->getFloat("width")/result->getScale();
	float height		= configReader->getFloat("height")/result->getScale();
	float x				= configReader->getFloat("x")/result->getScale();
	float y				= configReader->getFloat("y")/result->getScale();
	float velX;
	float velY;
	float maxX	;
	float maxY	;
	int	  dirX	;
	int	  dirY	;
	std::vector<ConfigReader*> nestedConfigReader = configReader->getConfigReader("properties")->getConfigReaderList("property");
	for (std::vector<ConfigReader*>::iterator it = nestedConfigReader.begin(); it != nestedConfigReader.end(); it++){
		std::string attrName = (*it)->getString("name");

		if (attrName == "velX"){
			velX = (*it)->getFloat("value");
		}
		if (attrName == "velY"){
			velY = (*it)->getFloat("value");
		}
		if (attrName == "maxX"){
			maxX = (*it)->getFloat("value")/result->getScale();
		}
		if (attrName == "maxY"){
			maxY = (*it)->getFloat("value")/result->getScale();
		}
		if (attrName == "dirX"){
			dirX = (*it)->getInteger("value");
		}
		if (attrName == "dirY"){
			dirY = (*it)->getInteger("value");
		}
	}
	std::string texturePath = name.substr(0,name.find("_")) + ".png";
	GameObject* movingGameObject = new MovingBlock(name,abfw::Vector3(x,y,0),abfw::Vector3(dirX,dirY,0),abfw::Vector3(velX,velY,0),abfw::Vector3(maxX,maxY,0),result);
	movingGameObject->setCollisionBox(new CollisionBox(abfw::Vector2(x,y),width,height));
	GUIEventHandler::getInstance()->addObjectToScene(name,texturePath,abfw::Vector3(x,y,0),width,height);
	result->addGameObject(movingGameObject);
}

void LevelLoaderBuilder::createClay(ConfigReader* configReader){
	GameObject* clay = createGameObject(configReader);
	clay->setCollisionBehaviour(new ClayCollisionBehaviour(clay->getName()));
	GUIEventHandler::getInstance()->addObjectToScene(clay->getName(),"",clay->getPosition(),clay->getBoundingBox()->getWidth(),clay->getBoundingBox()->getHeight());
	GUIEventHandler::getInstance()->playAnimation(clay->getName(),"moving",true);
}

void LevelLoaderBuilder::createBlock(ConfigReader* configReader){
	GameObject* block = createGameObject(configReader);
	block->setCollisionBehaviour(new SimpleBlockCollisionBehaviour);
}

void LevelLoaderBuilder::createCheckpoint(ConfigReader* reader){
	GameObject* checkpoint = createGameObject(reader);
	if (reader->getString("endlevel") != ""){
		checkpoint->setCollisionBehaviour(new CheckpointCollisionBehaviour(CheckpointType::END_LEVEL));
	}else{
		checkpoint->setCollisionBehaviour(new CheckpointCollisionBehaviour);		
	}

	GUIEventHandler::getInstance()->addObjectToDebugList(checkpoint->getName(),"",checkpoint->getPosition(), checkpoint->getBoundingBox()->getWidth(),checkpoint->getBoundingBox()->getHeight());
}

void LevelLoaderBuilder::createSpike(ConfigReader* reader){
	GameObject* spike = createGameObject(reader);
	spike->setCollisionBehaviour(new SpikeCollisionBehaviour);
	GUIEventHandler::getInstance()->addObjectToDebugList(spike->getName(),"",spike->getPosition(), spike->getBoundingBox()->getWidth(),spike->getBoundingBox()->getHeight());
}
void LevelLoaderBuilder::createPowerupClay(ConfigReader* reader){
	GameObject* powerup = createGameObject(reader);
	if (powerup->getName() == "powerup_melee"){
		powerup->setCollisionBehaviour(new TransformationClayCollisionBehaviour(new MeleeTransformation(),powerup->getName()));
		GUIEventHandler::getInstance()->addObjectToScene(powerup->getName(),"",powerup->getPosition(),powerup->getBoundingBox()->getWidth(),powerup->getBoundingBox()->getHeight());
		GUIEventHandler::getInstance()->playAnimation(powerup->getName(),"moving",true);
	}
	if (powerup->getName() == "powerup_bird"){
		powerup->setCollisionBehaviour(new TransformationClayCollisionBehaviour(new BirdTransformation(),powerup->getName()));
		GUIEventHandler::getInstance()->addObjectToScene(powerup->getName(),"",powerup->getPosition(),powerup->getBoundingBox()->getWidth(),powerup->getBoundingBox()->getHeight());
		GUIEventHandler::getInstance()->playAnimation(powerup->getName(),"moving",true);

	}
	if (powerup->getName() == "powerup_ranged"){
		powerup->setCollisionBehaviour(new TransformationClayCollisionBehaviour(new RangedTransformation(),powerup->getName()));
		GUIEventHandler::getInstance()->addObjectToScene(powerup->getName(),"",powerup->getPosition(),powerup->getBoundingBox()->getWidth(),powerup->getBoundingBox()->getHeight());
		GUIEventHandler::getInstance()->playAnimation(powerup->getName(),"moving",true);

	}
	
}
void LevelLoaderBuilder::createBreakableBlock(ConfigReader* reader){
	GameObject* interactiveObject = createGameObject(reader);
	std::string texturePath = interactiveObject->getName().substr(0,interactiveObject->getName().find("_")) + ".png";
	interactiveObject->setCollisionBehaviour(new BreakableBlockCollisionBehaviour(interactiveObject->getName()));
	GUIEventHandler::getInstance()->addObjectToScene(interactiveObject->getName(),texturePath,interactiveObject->getPosition(), interactiveObject->getBoundingBox()->getWidth(),interactiveObject->getBoundingBox()->getHeight());
}

void LevelLoaderBuilder::createLevel(std::vector<ConfigReader*> configReader){
	float width;
	float height;
	float scale = 1.f;
	int groundHeight;
	std::string name;
	
	for (std::vector<ConfigReader*>::iterator it = configReader.begin(); it != configReader.end(); it++){
		if ((*it)->getString("name") == "width"){
			width = (*it)->getFloat("value")/scale;
		}
		if ((*it)->getString("name") == "height"){
			height = (*it)->getFloat("value")/scale;
		}
		if ((*it)->getString("name") == "name"){
			name = (*it)->getString("value");
		}
		if ((*it)->getString("name") == "groundHeight"){
			groundHeight = (*it)->getInteger("value")/scale;
		}
	}

	result = new Level(width,height,height-groundHeight,abfw::Vector3(0,10,0),name);
	GUIEventHandler::getInstance()->newScene();

}

Level* LevelLoaderBuilder::getResult(){
	return result;
}
