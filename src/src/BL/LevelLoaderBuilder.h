#pragma once
#include "..\util\ConfigReader.h"
#include "Level.h"
#include "..\BL\objects\MovingGameObject.h"

class LevelLoaderBuilder{
private:
	Level* result;

	GameObject*			createGameObject(ConfigReader*);
	MovingGameObject*	createMovingGameObject(ConfigReader*);
public:
	void createLevel(std::vector<ConfigReader*>properties);
	void createBlock(ConfigReader*);
	void createMonster(ConfigReader*);
	void createClay(ConfigReader*);
	void createMovingBlock(ConfigReader*);
	void createCheckpoint(ConfigReader*);
	void createSpike(ConfigReader*);
	void createPowerupClay(ConfigReader*);
	void createBreakableBlock(ConfigReader*);

	Level* getResult();

};