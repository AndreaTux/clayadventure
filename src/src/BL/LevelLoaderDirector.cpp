#include "LevelLoaderDirector.h"
#include "..\util\ConfigReader.h"
#include <vector>

LevelLoaderDirector::LevelLoaderDirector(LevelLoaderBuilder* b)
:builder(b){}

void LevelLoaderDirector::buildLevel(std::string file){
	ConfigReader* configReader = new ConfigReader(file);
	
	builder->createLevel(configReader->getConfigReader("properties")->getConfigReaderList("property"));
	
	//builder->createBlock(configReader->getConfigReader("player"));
	
	std::vector<ConfigReader*> objectsGroups = configReader->getConfigReaderList("objectgroup");

	//for each object groups
	for (std::vector<ConfigReader*>::iterator it = objectsGroups.begin(); it != objectsGroups.end(); it++){
		//get the name of the group
		std::string name = (*it)->getString("name");
		std::vector<ConfigReader*> configReaders = (*it)->getConfigReaderList("object");

		/*
		* Collisions
		*/
		if (name == "collisions"){
			//build the blocks
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createBlock((*it2));
			}
		}
		/*
		* Clay
		*/
		if (name == "clays"){
			//build the clay
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createClay((*it2));
			}
		}

		/*
		* Moving Objects
		*/
		if (name == "moving_objects"){
			//build the moving objs
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createMovingBlock((*it2));
			}
		}

		/*
		* Power-Up
		*/
		if (name == "power_ups"){
			//build the moving objs
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createPowerupClay((*it2));
			}
		}

		/*
		* Checkpoint
		*/
		if (name == "checkpoints"){
			//build the checkpoints
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createCheckpoint((*it2));
			}
		}

		/*
		* Interactive Objects
		*/
		if (name == "objects"){
			//build the checkpoints
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createBreakableBlock((*it2));
			}
		}
		/*
		* Dangers (spikes)
		*/
		if (name == "dangers"){
			//build the dangers
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createSpike((*it2));
			}
		}

		/*
		* Enemies
		*/
		if (name == "enemies"){
			//build the monsters
			for (std::vector<ConfigReader*>::iterator it2 = configReaders.begin(); it2 != configReaders.end(); it2++){
				builder->createMonster((*it2));
			}
		}

	}
	delete configReader;
}