#pragma once
#include "LevelLoaderBuilder.h"
#include <string>

class LevelLoaderDirector{
private:
	LevelLoaderBuilder* builder;

public:
	LevelLoaderDirector(LevelLoaderBuilder*);

	void buildLevel(std::string file);
};