#include "PS4InputManager.h"
#include <system/platform.h>

PS4InputManager::PS4InputManager(Player* p,SpriteApp* a,abfw::Platform* platform)
	:InputManager(p,a){
	controllerInputManager = platform->CreateSonyControllerInputManager();

}


PS4InputManager::~PS4InputManager(void)
{	
	delete controllerInputManager;
	controllerInputManager = 0;

}

void PS4InputManager::update(float dTime){
	// 0 is success
	if(controllerInputManager->Update() == 0){
		controller = controllerInputManager->GetController(0);
		InputManager::update(dTime);
	}
	
	
}

bool PS4InputManager::checkButtonPressed(int button){
	bool result = controller->buttons_pressed() & button;
	if (result){
		cheatButtonQueue.push_back(button);
		checkCheat();
	}
	return result;
}

bool PS4InputManager::checkButtonReleased(int button){
	return controller->buttons_released() & button;
}


