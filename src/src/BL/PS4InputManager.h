#pragma once
#include "InputManager.h"

class PS4InputManager : public InputManager
{
public:
	PS4InputManager(Player* p,SpriteApp* a,abfw::Platform* platform);
	PS4InputManager::~PS4InputManager();

	virtual bool checkButtonPressed(int button);
	virtual bool checkButtonReleased(int button);
	virtual void update(float dTime);

private:
	abfw::SonyControllerInputManager* controllerInputManager;
	const abfw::SonyController* controller;
};

