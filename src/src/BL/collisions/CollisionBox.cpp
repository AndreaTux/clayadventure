#include "CollisionBox.h"
#include "CollisionSideEnum.h"
class GameObject;
CollisionBox::CollisionBox(abfw::Vector2 pos, float w, float h)
	:position(pos),width(w),height(h),originalHeight(height),originalWidth(w){
	
	resetSidesOfImpact();
} //perform some checking, i.e., is the position in the level?

CollisionBox::CollisionBox(const CollisionBox* collision)
	:position(collision->position),width(collision->width),height(collision->height),originalHeight(height),originalWidth(width){
	
}

CollisionBox::~CollisionBox(){
}

// true if it intersects the other shape
//http://silentmatt.com/rectangle-intersection/
bool CollisionBox::intersects(CollisionBox* other){

	bool collided = false;

	// box one
	float thisX1 = position.x;
	float thisY1 = position.y;

	float thisX2 = thisX1 + width;
	float thisY2 = thisY1 + height;

	// box two
	float otherX1 = other->position.x;
	float otherY1 = other->position.y;

	float otherX2 = otherX1 + other->width;
	float otherY2 = otherY1 + other->height;

	if (thisX1 < otherX2 && thisX2 > otherX1 && thisY1 < otherY2 && thisY2 > otherY1){
		collided = true;
		resetSidesOfImpact();
	}
	return collided;
}

void CollisionBox::translate(float x, float y){
	position += abfw::Vector2(x,y);
}

void CollisionBox::translate(abfw::Vector2 vec){
	position += vec;
}

void CollisionBox::setPosition(abfw::Vector2 vec){
	position = vec;
}

const abfw::Vector2 CollisionBox::getPosition()const{
	return position;
}

void CollisionBox::resetSidesOfImpact(){
	for (int i =0; i < 4; i++){
		sidesOfImpact[i] = false;
	}
}

void CollisionBox::checkCollisionSides(const CollisionBox* other){
	// box one
	float thisX1 = position.x;
	float thisY1 = position.y;

	float thisX2 = thisX1 + width;
	float thisY2 = thisY1 + height;

	// box two
	float otherX1 = other->position.x;
	float otherY1 = other->position.y;

	float otherX2 = otherX1 + other->width;
	float otherY2 = otherY1 + other->height;

	if (thisX2 <= otherX1){
		sidesOfImpact[RIGHT] = true;
	}
	if (thisY2 <= otherY1){
		sidesOfImpact[BOTTOM] = true;
	}
	if (thisY1 >= otherY2){
		sidesOfImpact[TOP] = true;
	}
	if (thisX1 >= otherX2){
		sidesOfImpact[RIGHT] = true;
	}

}

void CollisionBox::checkHorizontalCollisionSides(const CollisionBox* other){
	// box one
	float thisX1 = position.x;
	float thisX2 = thisX1 + width;
	float thisY1 = position.y;
	float thisY2 = thisY1 + height;

	// box two
	float otherX1 = other->position.x;
	float otherX2 = otherX1 + other->width;
	float otherY1 = other->position.y;
	float otherY2 = otherY1 + other->height;

	if (thisX1 <= otherX2 && thisX1 > otherX1 && thisY2 < otherY2){
		sidesOfImpact[LEFT] = true;
	}
	if (thisX2 >= otherX1 && thisX2 < otherX2 && thisY2 < otherY2){
		sidesOfImpact[RIGHT] = true;
	}
}

bool CollisionBox::isCollidedAtSide(int side)const{
	return sidesOfImpact[side];
}

const bool* CollisionBox::getSidesOfImpact()const{
	return sidesOfImpact;
}

float CollisionBox::getWidth()const{
	return width;
}

float CollisionBox::getHeight()const{
	return height;
}

void CollisionBox::scale(float newSize){
	float oldHeight = height;
	float oldWidth	= width;

	height	= newSize * originalHeight;
	width	= newSize * originalWidth;

	abfw::Vector2 deltaSize (oldWidth - width, oldHeight - height);
	position += deltaSize;
}