#pragma once
#include <maths\vector2.h>

class GameObject;

class CollisionInfo;

class CollisionBox{
private:
	// position of the upper left corner
	abfw::Vector2 position;
	float width, height;
	float originalWidth, originalHeight;
	bool sidesOfImpact [4];

	void resetSidesOfImpact();
	

public:
	CollisionBox(abfw::Vector2 pos, float width, float height);
	CollisionBox(const CollisionBox*);

	~CollisionBox();

	void translate (float dx, float dy);

	void translate (abfw::Vector2);
	
	bool intersects(CollisionBox*);

	void setPosition(abfw::Vector2);

	const abfw::Vector2 getPosition()const;

	bool isCollidedAtSide(int side)const;
	/*
	* check the position of a bounding box compared to another
	*/
	void checkCollisionSides(const CollisionBox*);
	void checkHorizontalCollisionSides(const CollisionBox*);

	float getWidth()const;

	float getHeight()const;

	const bool* getSidesOfImpact()const;

	void scale(float newSize);

};