#pragma once
/*
* A and B objects collides...
*/
enum CollisionSide {
	
	//A bottom
	BOTTOM = 0,
	//A top
	TOP,
	//A left
	LEFT,
	//A right
	RIGHT
};