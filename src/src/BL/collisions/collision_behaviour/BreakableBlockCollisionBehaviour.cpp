#include "BreakableBlockCollisionBehaviour.h"
#include "..\..\EventHandler.h"
#include "..\..\..\Audio\AudioManager.h"

BreakableBlockCollisionBehaviour::BreakableBlockCollisionBehaviour(std::string n)
	:CollisionBehaviour("breakable"), name(n),bulletType(BulletType::NO_TYPE){}

void BreakableBlockCollisionBehaviour::interact(CollisionBehaviour* other,GameObject* thisObj,GameObject* box){
	other->interact(this,thisObj,box);
}


void BreakableBlockCollisionBehaviour::interact(PlayerCollisionBehaviour*,GameObject*,GameObject* box){
	EventHandler::getInstance()->blockPlayerForCollision(box->getBoundingBox());
}

void BreakableBlockCollisionBehaviour::interact(BulletCollisionBehaviour* bullet,GameObject*,GameObject*){
	EventHandler::getInstance()->removeObjectFromGame(bullet->getName());
	if (bulletType == BulletType::NO_TYPE || bullet->getType() == bulletType){
		EventHandler::getInstance()->playAnimation(name,"destroyed",false);
		AudioManager::getInstance()->play("break",false);
		//EventHandler::getInstance()->removeObjectFromGame(name);
	}
}

void BreakableBlockCollisionBehaviour::setBulletType(BulletType newType){
	bulletType = newType;
}
