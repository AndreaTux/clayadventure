#pragma once
#include "CollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "PlayerCollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "ClayCollisionBehaviour.h"
#include "..\..\objects\Bullet.h"
#include <string>

class BreakableBlockCollisionBehaviour : public CollisionBehaviour{
private:
	std::string name;
	BulletType bulletType;
public:
	BreakableBlockCollisionBehaviour(std::string);
	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(BulletCollisionBehaviour*,GameObject*,GameObject*);

	void setBulletType(BulletType);
};