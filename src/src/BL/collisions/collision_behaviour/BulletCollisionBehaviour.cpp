#include "BulletCollisionBehaviour.h"
#include "..\..\EventHandler.h"

BulletCollisionBehaviour::BulletCollisionBehaviour(std::string n, BulletType t)
	:CollisionBehaviour("bullet"), name(n),type(t){
}

void BulletCollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* box){
	otherBehaviour->interact(this,thisObj,box);
} 
void BulletCollisionBehaviour::interact(SimpleBlockCollisionBehaviour* otherBehaviour,GameObject*,GameObject*){
	EventHandler::getInstance()->removeObjectFromGame(name);
}

void BulletCollisionBehaviour::interact(PlayerCollisionBehaviour*, GameObject*,GameObject*){
	
}

const std::string BulletCollisionBehaviour::getName()const{
	return name;
}

const BulletType BulletCollisionBehaviour::getType()const{
	return type;
}
