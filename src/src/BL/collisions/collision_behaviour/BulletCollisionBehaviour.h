#pragma once
#include "CollisionBehaviour.h"
#include "PlayerCollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "SimpleBlockCollisionBehaviour.h"
#include "ClayCollisionBehaviour.h"
#include "..\..\objects\Bullet.h"
#include <string>

class BulletCollisionBehaviour : public CollisionBehaviour{
private:
	std::string name;
	BulletType type;
public:
	BulletCollisionBehaviour(std::string, BulletType t);

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(SimpleBlockCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*, GameObject*,GameObject*);
	const std::string getName()const;
	const BulletType getType()const;

};