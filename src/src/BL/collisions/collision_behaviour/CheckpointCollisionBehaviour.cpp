#include "CheckpointCollisionBehaviour.h"
#include "..\..\objects\GameObject.h"
#include "..\..\EventHandler.h"

CheckpointCollisionBehaviour::CheckpointCollisionBehaviour(CheckpointType t)
	:CollisionBehaviour("checkpoint"),type(t){}

void CheckpointCollisionBehaviour::interact(CollisionBehaviour* other,GameObject* thisObj,GameObject* obj){
	other->interact(this,thisObj,obj);
}
void CheckpointCollisionBehaviour::interact(PlayerCollisionBehaviour* other ,GameObject*,GameObject* obj){
	if (type == NORMAL){
		abfw::Vector2 position = obj->getBoundingBox()->getPosition();
		//set the bottom left corner (instead of the default top-left)
		position.y = position.y + obj->getBoundingBox()->getHeight();
		EventHandler::getInstance()->setPlayerCheckpoint(abfw::Vector3(position.x,position.y,0));
	}
	if (type == END_LEVEL){
		EventHandler::getInstance()->levelEnded();
	}

}