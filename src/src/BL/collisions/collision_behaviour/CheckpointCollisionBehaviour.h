#pragma once
#include "CollisionBehaviour.h"

enum CheckpointType{
	NORMAL = 0,
	END_LEVEL
};

class CheckpointCollisionBehaviour :public CollisionBehaviour{
public:
	CheckpointCollisionBehaviour(CheckpointType t = NORMAL);
	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
private:
	CheckpointType type;
};