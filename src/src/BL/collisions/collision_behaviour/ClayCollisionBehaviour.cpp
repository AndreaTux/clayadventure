#include "ClayCollisionBehaviour.h"
#include "..\..\EventHandler.h"

ClayCollisionBehaviour::ClayCollisionBehaviour(std::string n)
	:CollisionBehaviour("clay"),name(n){}

void ClayCollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* box){
	otherBehaviour->interact(this,thisObj,box);
} 

void ClayCollisionBehaviour::interact(PlayerCollisionBehaviour* p,GameObject*,GameObject*){
	EventHandler::getInstance()->collectClay();
	EventHandler::getInstance()->removeObjectFromGame(name);
}