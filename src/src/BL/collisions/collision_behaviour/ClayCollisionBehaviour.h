#pragma once
#include "CollisionBehaviour.h"
#include "PlayerCollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "SimpleBlockCollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "EnemyCollisionBehaviour.h"
#include <string>

class ClayCollisionBehaviour :public CollisionBehaviour{
protected:
	std::string name;
public:
	ClayCollisionBehaviour(std::string );

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
};