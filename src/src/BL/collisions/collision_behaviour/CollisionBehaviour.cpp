#include "CollisionBehaviour.h"


CollisionBehaviour::CollisionBehaviour(std::string n)
	:name(n){}

void CollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* box){
	otherBehaviour->interact(this,thisObj,box);
} 
void CollisionBehaviour::interact(SimpleBlockCollisionBehaviour* otherBehaviour,GameObject*,GameObject*){
	
}

void CollisionBehaviour::interact(BreakableBlockCollisionBehaviour*,GameObject*,GameObject*){
}
void CollisionBehaviour::interact(PlayerCollisionBehaviour* player,GameObject*,GameObject*){

}

void CollisionBehaviour::interact(ClayCollisionBehaviour* clay,GameObject*,GameObject*){

}

void CollisionBehaviour::interact(EnemyCollisionBehaviour*,GameObject*, GameObject*){
}

void CollisionBehaviour::interact(SpikeCollisionBehaviour*,GameObject*,GameObject*){
}

void CollisionBehaviour::interact(BulletCollisionBehaviour*,GameObject*,GameObject*){
}

void CollisionBehaviour::interact(CheckpointCollisionBehaviour*,GameObject*,GameObject*){
	
}

void CollisionBehaviour::interact(TransformationClayCollisionBehaviour*,GameObject*,GameObject*){
}

const std::string CollisionBehaviour::getName(){
	return name;
}