#pragma once
#include <string>
//forward declaration
class PlayerCollisionBehaviour;
class SimpleBlockCollisionBehaviour;
class BulletCollisionBehaviour;
class BreakableBlockCollisionBehaviour;
class ClayCollisionBehaviour;
class EnemyCollisionBehaviour;
class SpikeCollisionBehaviour;
class CheckpointCollisionBehaviour;
class TransformationClayCollisionBehaviour;
class GameObject;

class CollisionBehaviour{
private:
	std::string name;

public:
	CollisionBehaviour(std::string name);

	const std::string getName();

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(SimpleBlockCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(BulletCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(BreakableBlockCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(ClayCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(EnemyCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(SpikeCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(CheckpointCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(TransformationClayCollisionBehaviour*,GameObject*,GameObject*);
};