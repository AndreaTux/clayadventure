#include "EnemyCollisionBehaviour.h"
#include "..\..\EventHandler.h"

EnemyCollisionBehaviour::EnemyCollisionBehaviour(std::string objname, BulletType t)
	:CollisionBehaviour("enemy"),objectName(objname),bulletResistance(t){}

void EnemyCollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* other){
	otherBehaviour->interact(this,thisObj,other);
}


void EnemyCollisionBehaviour::interact(PlayerCollisionBehaviour* player,GameObject*,GameObject* o){
 	EventHandler::getInstance()->playerGetsHit();
	abfw::Vector3 forceDirection = o->getDirection();
	//EventHandler::getInstance()->addExternalForceToPlayer(o->getDirection(), abfw::Vector3(500,500,0));
	EventHandler::getInstance()->addExternalForceToPlayer(abfw::Vector3(1,1,0), abfw::Vector3(500,500,0));
	//EventHandler::getInstance()->blockPlayerForCollision(o->getBoundingBox());
}

void EnemyCollisionBehaviour::interact(BulletCollisionBehaviour* bullet,GameObject*,GameObject* obj){
	if (bullet->getType() != BulletType::ENEMY && bullet->getType() != bulletResistance){
		//EventHandler::getInstance()->removeObjectFromGame(obj->getName());
		EventHandler::getInstance()->playAnimation(obj->getName(),"destroyed_" + obj->getStringFacing(),false);
		EventHandler::getInstance()->removeObjectFromGame(bullet->getName());
		EventHandler::getInstance()->enemyKilled(obj->getName().substr(0,obj->getName().find("_")));
	}
}

std::string EnemyCollisionBehaviour::getObjectName(){
	return objectName;
}
