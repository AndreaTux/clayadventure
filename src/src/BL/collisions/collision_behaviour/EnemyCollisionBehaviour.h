#pragma once
#include "CollisionBehaviour.h"
#include "CollisionBehaviour.h"
#include "PlayerCollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "SimpleBlockCollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "..\..\objects\Bullet.h"

class EnemyCollisionBehaviour : public CollisionBehaviour{
private:
	std::string objectName;
	BulletType bulletResistance;
public:
	EnemyCollisionBehaviour(std::string objectName,BulletType type = NO_TYPE);
	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(BulletCollisionBehaviour*,GameObject*,GameObject*);
	std::string getObjectName();
};