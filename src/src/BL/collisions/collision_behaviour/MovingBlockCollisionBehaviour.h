#pragma once
#include "SimpleBlockCollisionBehaviour.h"

class MovingBlockCollisionBehaviour : public SimpleBlockCollisionBehaviour{
public:
	//MovingBlockCollisionbehaviour(

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
};