#include "PlayerCollisionBehaviour.h"
#include "..\..\EventHandler.h"
#include "..\..\objects\Bullet.h"

PlayerCollisionBehaviour::PlayerCollisionBehaviour()
	:CollisionBehaviour("player"){
}

void PlayerCollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* box){
 	otherBehaviour->interact(this,thisObj,box);
} 

void PlayerCollisionBehaviour::interact(EnemyCollisionBehaviour* enemy,GameObject* e, GameObject* player){
	EventHandler::getInstance()->playerGetsHit();
	//EventHandler::getInstance()->addExternalForceToPlayer(e->getDirection(), abfw::Vector3(500,200,0));
	EventHandler::getInstance()->addExternalForceToPlayer(abfw::Vector3(1,1,0), abfw::Vector3(500,500,0));
	EventHandler::getInstance()->blockForCollision(e->getName(),player->getBoundingBox());
}

void PlayerCollisionBehaviour::interact(BulletCollisionBehaviour* b,GameObject* bullet,GameObject* p){
	if(b->getType() == BulletType::ENEMY){
		EventHandler::getInstance()->playerGetsHit();
		EventHandler::getInstance()->addExternalForceToPlayer(abfw::Vector3(1,1,0),abfw::Vector3(200,200,0));
		EventHandler::getInstance()->removeObjectFromGame(b->getName());
	}
}