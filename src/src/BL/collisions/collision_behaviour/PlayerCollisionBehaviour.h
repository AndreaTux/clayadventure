#pragma once
#include "..\..\objects\player\player.h"
#include "CollisionBehaviour.h"
#include "SimpleBlockCollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "ClayCollisionBehaviour.h"

class PlayerCollisionBehaviour : public CollisionBehaviour{
private:
	Player* player;
public:
	PlayerCollisionBehaviour();

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(EnemyCollisionBehaviour*,GameObject*, GameObject*);
	virtual void interact(BulletCollisionBehaviour*,GameObject*,GameObject*);
};