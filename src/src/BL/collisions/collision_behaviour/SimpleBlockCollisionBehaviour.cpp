#include "SimpleBlockCollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "..\..\EventHandler.h"

SimpleBlockCollisionBehaviour::SimpleBlockCollisionBehaviour()
	:CollisionBehaviour("block"){}

void SimpleBlockCollisionBehaviour::interact(CollisionBehaviour* otherBehaviour,GameObject* thisObj,GameObject* box){
	otherBehaviour->interact(this,thisObj,box);
}
void SimpleBlockCollisionBehaviour::interact(PlayerCollisionBehaviour* otherBehaviour,GameObject*,GameObject* box){
	EventHandler::getInstance()->blockPlayerForCollision(box->getBoundingBox());
}

void SimpleBlockCollisionBehaviour::interact(BulletCollisionBehaviour* bullet,GameObject*,GameObject*){
	EventHandler::getInstance()->removeObjectFromGame(bullet->getName());
}

void SimpleBlockCollisionBehaviour::interact(EnemyCollisionBehaviour* enemy,GameObject*, GameObject* box){
 	EventHandler::getInstance()->blockForCollision(enemy->getObjectName(),box->getBoundingBox());
}