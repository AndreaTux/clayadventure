#pragma once
#include "CollisionBehaviour.h"
#include "BreakableBlockCollisionBehaviour.h"
#include "PlayerCollisionBehaviour.h"
#include "BulletCollisionBehaviour.h"
#include "ClayCollisionBehaviour.h"

class SimpleBlockCollisionBehaviour : public CollisionBehaviour{
public:
	SimpleBlockCollisionBehaviour();
	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(BulletCollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(EnemyCollisionBehaviour* enemy,GameObject*, GameObject* box);
};