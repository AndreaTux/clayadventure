#include "SpikeCollisionBehaviour.h"
#include "..\..\EventHandler.h"

SpikeCollisionBehaviour::SpikeCollisionBehaviour()
	:CollisionBehaviour("spike"){}

void SpikeCollisionBehaviour::interact(CollisionBehaviour* other ,GameObject* thisObj,GameObject* obj){
	other->interact(this,thisObj,obj);
}

void SpikeCollisionBehaviour::interact(PlayerCollisionBehaviour* playerBehaviour,GameObject*,GameObject* obj){
	EventHandler::getInstance()->playerGetsHit();
	EventHandler::getInstance()->putPlayerToCheckpoint();
}