#pragma once
#include "CollisionBehaviour.h"

class SpikeCollisionBehaviour : public CollisionBehaviour{
public:
	SpikeCollisionBehaviour();
	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
};