#include "TransformationClayCollisionBehaviour.h"
#include "..\..\EventHandler.h"
TransformationClayCollisionBehaviour::TransformationClayCollisionBehaviour(Transformation* t, std::string n)
	:ClayCollisionBehaviour(n),transformation(t){

}

void TransformationClayCollisionBehaviour::interact(CollisionBehaviour* other,GameObject* thisObj,GameObject* obj){
	other->interact(this,thisObj,obj);
}

void TransformationClayCollisionBehaviour::interact(PlayerCollisionBehaviour* other,GameObject*,GameObject* obj){
	EventHandler::getInstance()->learnTransformation(transformation);
	EventHandler::getInstance()->removeObjectFromGame(name);
}