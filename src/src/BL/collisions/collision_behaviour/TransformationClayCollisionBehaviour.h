#pragma once
#include "ClayCollisionBehaviour.h"

class TransformationClayCollisionBehaviour : public ClayCollisionBehaviour{
private:
	Transformation* transformation;
public:
	TransformationClayCollisionBehaviour(Transformation*,std::string);

	virtual void interact(CollisionBehaviour*,GameObject*,GameObject*);
	virtual void interact(PlayerCollisionBehaviour*,GameObject*,GameObject*);
};