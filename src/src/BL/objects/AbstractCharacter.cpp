#include "AbstractCharacter.h"
#include "..\collisions\CollisionSideEnum.h"
#include "..\EventHandler.h"

AbstractCharacter::AbstractCharacter(std::string name,Status* s, abfw::Vector3& pos, abfw::Vector3& dir,abfw::Vector3& sp,Level* l)
	:MovingGameObject(name,pos,dir,sp,l),status(s),jumping(false){
		//collision = new CollisionBox(abfw::Vector2(pos.x,pos.y),0,0);
}

AbstractCharacter::~AbstractCharacter(){
	//delete status;
}


Status* AbstractCharacter::getStatus(){
	return status;
}


void AbstractCharacter::block(const CollisionBox* collider){
	this->collision->checkCollisionSides(collider);
	//TODO: check if it can be done by checking player velocity/acceleration
	if (collision->isCollidedAtSide(CollisionSide::TOP) || collision->isCollidedAtSide(CollisionSide::BOTTOM)){
		//if the player has collided bottom and is jumping, stop the jump
		if (collision->isCollidedAtSide(CollisionSide::BOTTOM)){
			if (jumping){
				endJump();
			}
		}
		blockVertical(collider);
	}
	if (collision->isCollidedAtSide(CollisionSide::RIGHT) || collision->isCollidedAtSide(CollisionSide::LEFT)){
		blockHorizontal();
	}
}

void AbstractCharacter::blockVertical(){
	currentSpeed.y = 0;
	currentJumpAcceleration = 0;
	gravityAccumulator = 0;

	
}

void AbstractCharacter::blockVertical(const CollisionBox* obj){
	if (falling || jumping){
		//BOTTOM of the player, so the player is falling: put the player at the same height of the collision box
		if(collision->isCollidedAtSide(CollisionSide::BOTTOM) &&  !collision->isCollidedAtSide(CollisionSide::TOP) && 
			!collision->isCollidedAtSide(CollisionSide::RIGHT) && !collision->isCollidedAtSide(CollisionSide::LEFT)){
			position.y = obj->getPosition().y - collision->getHeight();
		}
	}
	blockVertical();
}

void AbstractCharacter::blockHorizontal(){
	currentSpeed.x = 0;
	//direction.x = 0;
}

void AbstractCharacter::endJump(){
	jumping = false;
	addDirection(abfw::Vector3(0,1,0));
	currentJumpAcceleration = 0;
}
