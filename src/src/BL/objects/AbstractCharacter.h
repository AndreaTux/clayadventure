#pragma once
#include "AbstractCharacter.h"
#include "MovingGameObject.h"
#include "..\Level.h"
#include "Status.h"

class AbstractCharacter : public MovingGameObject {
protected:
	Status* status;
	bool jumping;
	bool falling;
	float currentJumpAcceleration;
	float gravityAccumulator;
	
protected:
	virtual void blockVertical();
	virtual void blockVertical(const CollisionBox*);
	virtual void blockHorizontal();
	void endJump();
public:
	AbstractCharacter(std::string,Status*,abfw::Vector3& pos = abfw::Vector3(0,0,0), abfw::Vector3&  dir = abfw::Vector3(0,0,0), 
		abfw::Vector3& speed = abfw::Vector3(0,0,0),Level* = 0);
	~AbstractCharacter();	

	virtual Status* getStatus();

	void block(const CollisionBox*);

	//add damage and status stuff
};