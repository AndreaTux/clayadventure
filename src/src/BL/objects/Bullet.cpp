#include "Bullet.h"
#include "..\EventHandler.h"
#include "..\collisions\collision_behaviour\BulletCollisionBehaviour.h"
#include "..\..\util\IDMaker.h"
#include <sstream>
#include <math.h>

Bullet::Bullet(abfw::Vector3 pos,abfw::Vector3 dir,float speed, int maxDistance, Level* l, BulletType t)
	:MovingGameObject("bullet_",pos,dir,abfw::Vector3(speed,0,0),l),startingPointX(pos.x),maximumDistance(maxDistance),type(t){
		setCollisionBox(new CollisionBox(abfw::Vector2(pos.x,pos.y),16,16));
		std::stringstream ss;
		ss << IDMaker::getNext();
		name += ss.str();
		//TODO Why it crashes when put into the init list?
		behaviour = new BulletCollisionBehaviour(name,type);
}


void Bullet::update(float dTime){
	float distance = position.x - startingPointX;
	if (std::abs(distance) >= maximumDistance){
		EventHandler::getInstance()->removeObjectFromGame(name);
	}else{	
		MovingGameObject::update(dTime);
	}
	
}