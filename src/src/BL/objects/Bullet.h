#pragma once
#include "MovingGameObject.h"

enum BulletType {
	NO_TYPE = 0,
	MELEE = 1,
	SHOT = 2,
	ENEMY = 3
};

class Bullet :public MovingGameObject{
private:
	float startingPointX;
	float maximumDistance;
	BulletType type;
public:
	Bullet(abfw::Vector3 pos,abfw::Vector3 dir,float speed, int maxDistance, Level*, BulletType = NO_TYPE);
	virtual void update(float dTime);
};