#pragma once
#include "Enemy.h"
#include "..\..\AI\FSM\IdleState.h"
#include "..\..\AI\FSM\FiniteStateMachine.h"
#include "..\..\AI\FSM\ChargeState.h"
#include "..\EventHandler.h"

Enemy::Enemy(std::string name,Status* status,abfw::Vector3& pos, abfw::Vector3&  dir, abfw::Vector3& speed, abfw::Vector3 fov, abfw::Vector3 fow, Level* level)
	:AbstractCharacter(name,status,pos,dir,speed,level),target(0){

		fieldOfView = fov;
		fieldOfWalk = fow;
		targetPoint = 0;
		stateMachine = 0;
		hitABlock = false;
}

Enemy::~Enemy(){
	delete stateMachine;
	if (targetPoint != 0){
		delete targetPoint;
	}
}

void Enemy::update(float dTime){
	if (hitABlock){
		hitABlock = false;
	}
	AbstractCharacter::update(dTime);
	if(stateMachine != 0)
		stateMachine->update(this,dTime);
}

void Enemy::blockHorizontal(){
	AbstractCharacter::blockHorizontal();
	deleteTargetPoint();
	EventHandler::getInstance()->playAnimation(name,"idle_" + getStringFacing(),true);
	hitABlock = true;
}

void Enemy::blockVertical(){
	AbstractCharacter::blockVertical();
	EventHandler::getInstance()->playAnimation(name,"idle_" + getStringFacing(),true);
	deleteTargetPoint();
}

void Enemy::deleteTargetPoint(){
	if (targetPoint != 0){
		delete targetPoint;
	}
	targetPoint = 0;
	direction = abfw::Vector3(0,0,0);
}

bool Enemy::hasHitABlock(){
	return hitABlock;
}

void Enemy::setStateMachine(FiniteStateMachine<Enemy>* s){
	this->stateMachine = s;
}

FiniteStateMachine<Enemy>* Enemy::getStateMachine(){
	return stateMachine;
}
