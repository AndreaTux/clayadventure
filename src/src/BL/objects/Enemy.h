#pragma once
#include "AbstractCharacter.h"
#include "..\..\AI\FSM\FiniteStateMachine.h"
#include "..\..\util\ConfigReader.h"
#include "..\..\AI\FSM\BullFiniteStateMachine.h"
#include "..\..\AI\FSM\ShootState.h"
#include "..\..\AI\FSM\LemurFiniteStateMachine.h"

class Enemy : public AbstractCharacter{
	friend class IdleState;
	friend class ChargeState;
	friend class StunnedState;
	friend class BullFiniteStateMachine;
	friend class ShootState;
	friend class LemurFiniteStateMachine;
private:
	GameObject* target;
	abfw::Vector3 fieldOfView;
	abfw::Vector3 fieldOfWalk;
	abfw::Vector3* targetPoint;
	FiniteStateMachine<Enemy>* stateMachine;
	bool hitABlock;

protected:
virtual void blockVertical();
virtual void blockHorizontal();
void deleteTargetPoint();

public:
	//Enemy(ConfigReader* reader);
	Enemy();
	Enemy(std::string,Status*,abfw::Vector3& pos = abfw::Vector3(0,0,0), abfw::Vector3&  dir = abfw::Vector3(0,0,0), 
		abfw::Vector3& speed = abfw::Vector3(0,0,0),abfw::Vector3 fov = abfw::Vector3(150,0,0), abfw::Vector3 fow = abfw::Vector3(180,0,0), Level* = 0);
	~Enemy();

	virtual void update(float dTime);

	bool hasHitABlock();

	void setStateMachine(FiniteStateMachine<Enemy>* s);

	FiniteStateMachine<Enemy>* getStateMachine();

};