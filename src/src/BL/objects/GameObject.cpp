#include "GameObject.h"
#include "..\EventHandler.h"

GameObject::GameObject(std::string n,abfw::Vector3& pos)
	: position(pos), name(n),behaviour(0){
		collision = 0;
		father = 0;
}

GameObject::GameObject(std::string n,abfw::Vector3 pos, abfw::Vector3 dir)
	:name(n),position(pos),direction(dir),behaviour(0),facing(0){
		collision = 0;
		father = 0;

}

GameObject::~GameObject(){
	if(collision != 0){
		//delete collision;
	}
	if (behaviour != 0){
		delete behaviour;
	}
}

abfw::Vector3 GameObject::getPosition() const{
	return position;
}

const std::string GameObject::getName() const{
	return name;
}


bool GameObject::checkCollisionWith(GameObject* other)const{
	return collision->intersects(other->collision);
}

void GameObject::setCollisionBox(CollisionBox* coll){
	collision = coll;
}

const CollisionBox* GameObject::getBoundingBox()const{
	return collision;
}

void GameObject::update(float dTime){
}

void GameObject::collisionInteraction(GameObject* other){
	if (behaviour != 0){
		behaviour->interact(other->behaviour,this,other);
	}
}

void GameObject::attachChild (GameObject* child){
	children.insert(std::pair<std::string,GameObject*>(child->getName(),child));
}

void GameObject::detatchChild(std::string id){
	children.erase(id);
}

void GameObject::translate(abfw::Vector3 delta){
	//check for collision!!
	collision->translate(abfw::Vector2(delta.x,delta.y));
	position += delta;
	bool spontaneous = true;

	if (father != 0){
		spontaneous = false;
	}
	EventHandler::getInstance()->objectMoved(name,delta,spontaneous);
}

void GameObject::addDirection(const abfw::Vector3& newDir){
	direction += newDir;
}

void GameObject::setDirection(const abfw::Vector3& newDir){
	direction = newDir;
}

abfw::Vector3 GameObject::getDirection()const{
	return direction;
}

void GameObject::setCollisionBehaviour(CollisionBehaviour* behaviour){
	this->behaviour = behaviour;
}

void GameObject::checkCollisionSides (GameObject* other)const{
	collision->checkCollisionSides(other->collision);
}

void GameObject::checkHorizontalCollisionSides(GameObject* other)const{
	collision->checkHorizontalCollisionSides(other->collision);
}

const std::string GameObject::getCollisionBehaviourID(){
	return behaviour->getName();
}

int GameObject::getFacing(){
	return facing;
}

void GameObject::setFacing(int f){
	facing = f;
}

std::string GameObject::getStringFacing(){
	return facing == -1 ? "left" : "right";
}