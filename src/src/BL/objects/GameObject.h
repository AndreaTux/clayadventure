#pragma once
#include <maths/vector3.h>
#include "..\collisions\CollisionBox.h"
#include "..\collisions\collision_behaviour\CollisionBehaviour.h"
#include <iostream>
#include <map>

class GameObject{
protected:
	abfw::Vector3 position;
	abfw::Vector3 direction;
	std::string name;
	CollisionBox* collision;
	CollisionBehaviour* behaviour;
	std::map<std::string,GameObject*> children;
	GameObject* father;
	// 1 right side, -1 left side
	int facing;

public:
	GameObject(std::string = "null", abfw::Vector3& = abfw::Vector3(0,0,0));
	GameObject(std::string,abfw::Vector3 pos = abfw::Vector3(0,0,0), abfw::Vector3 dir = abfw::Vector3(0,0,0));
	
	~GameObject();
	virtual void update(float dTime);

	abfw::Vector3 getPosition() const;

	bool checkCollisionWith (GameObject *)const;

	void checkCollisionSides (GameObject*)const;

	void checkHorizontalCollisionSides(GameObject*)const;

	const std::string getName() const;

	//just for testing
	void setCollisionBox(CollisionBox*);
	
	const CollisionBox* getBoundingBox()const;

	void collisionInteraction (GameObject*);

	void attachChild (GameObject*);

	void detatchChild (std::string );

	virtual void translate (abfw::Vector3 delta);

	void addDirection(const abfw::Vector3&);

	void setDirection(const abfw::Vector3&);
	
	abfw::Vector3 getDirection()const;

	void setCollisionBehaviour(CollisionBehaviour*);

	const std::string getCollisionBehaviourID();

	void setFacing(int);

	int getFacing();

	std::string getStringFacing();

};