#include "MovingBlock.h"
#include "..\..\util\IDMaker.h"
#include "..\collisions\collision_behaviour\MovingBlockCollisionBehaviour.h"
MovingBlock::MovingBlock(std::string name,abfw::Vector3 pos,abfw::Vector3 dir, abfw::Vector3 speed, abfw::Vector3 max,Level* l)
	:MovingGameObject(name,pos,dir,speed,l),maxDistance(max),startingPoint(pos){
		setCollisionBehaviour(new MovingBlockCollisionBehaviour);
}

void MovingBlock::update(float dTime){
	abfw::Vector3 distance = startingPoint - position;
	if (direction.x != 0){
		if (std::abs(distance.x) >= maxDistance.x){
			//this prevent block to gets stuck
		
			if (std::abs(distance.x) > maxDistance.x){
				position.x = startingPoint.x + maxDistance.x * direction.x;
			}
		
			direction *= -1;
			//brutally stops
			currentSpeed.x = 0;
		
		}
	}
	if (direction.y != 0){
		if (std::abs(distance.y) >= maxDistance.y){
				//this prevent block to gets stuck
		
				if (std::abs(distance.y) > maxDistance.x){
					position.y = startingPoint.y + maxDistance.y * direction.y;
				}
		
				direction *= -1;
				//brutally stops
				currentSpeed.y = 0;
		
			}
		}
	MovingGameObject::update(dTime);
}