#pragma once
#include "MovingGameObject.h"

class MovingBlock : public MovingGameObject {
private:
	abfw::Vector3 startingPoint;
	abfw::Vector3 maxDistance;

public:
	MovingBlock(std::string name,abfw::Vector3 pos,abfw::Vector3 dir, abfw::Vector3 speed,abfw::Vector3 maxDistance,Level*);
	virtual void update(float dTime);
};