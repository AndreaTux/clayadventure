#include "MovingGameObject.h"
#include "..\EventHandler.h"

MovingGameObject::MovingGameObject(std::string name,abfw::Vector3 pos,abfw::Vector3 dir, abfw::Vector3 acc,Level* l)
	:GameObject(name,pos,dir),acceleration(acc),level(l),maxSpeed(acc),currentSpeed(abfw::Vector3(0,0,0)){
}


void MovingGameObject::update(float dTime){
	//compute the current speed
	currentSpeed += abfw::Vector3(direction.x * acceleration.x, direction.y * acceleration.y, direction.z * acceleration.z);

	if (currentSpeed.x > maxSpeed.x){
		currentSpeed.x = maxSpeed.x;
	}else
	if(currentSpeed.x < -1 * maxSpeed.x){
		currentSpeed.x = -1 * maxSpeed.x;
	}

	//position where the object SHOULD be next frame
	abfw::Vector3 presumedPosition = position + currentSpeed * dTime;

	abfw::Vector2 delta = abfw::Vector2(presumedPosition.x - position.x, presumedPosition.y - position.y);
	//translate the collision, to test if the movement is possible
	collision->translate(delta);
	std::vector<GameObject*> colliders = level->checkCollisions(this);

	//for each item that collides with the object, apply the behaviour of collision
	if (!colliders.empty()){
		//put the collision box back -- should be in the block code!!
		collision->translate(delta * -1);
		for (std::vector<GameObject*>::iterator it = colliders.begin(); it != colliders.end(); it++){
			collision->checkCollisionSides((*it)->getBoundingBox());
			this->collisionInteraction(*it);
		}
		presumedPosition = position + currentSpeed * dTime;
		delta = abfw::Vector2(presumedPosition.x - position.x, presumedPosition.y - position.y);
		collision->translate(delta);
	}
	//if there is a change in position..
	if (delta.x != 0 || delta.y != 0){
		if (delta.x > 0 && facing != 1){
			facing = 1;
		}else{
		if (delta.x <0 && facing != -1){
			facing = -1;
		}
		}
		//..update the object!
		position = presumedPosition;
		EventHandler::getInstance()->objectMoved(name,abfw::Vector3(delta.x,delta.y,0));
		//update the children
		for(std::map<std::string,GameObject*>::iterator it = children.begin(); it != children.end(); it++){
			it->second->translate(abfw::Vector3(delta.x,delta.y,0));
		}
	}

	if (direction.x == 0){
		//brutally stop
		currentSpeed.x = 0;
	}
	
}

void MovingGameObject::setLevel(Level* l){
	level = l;
}

abfw::Vector3 MovingGameObject::getAcceleration() const{
	return acceleration;
}

abfw::Vector3 MovingGameObject::getCurrentSpeed()const{
	return currentSpeed;
}

void MovingGameObject::setAcceleration(abfw::Vector3 newAcc){
	this->acceleration = newAcc;
}

abfw::Vector3 MovingGameObject::getMaxSpeed()const{
	return maxSpeed;
}

void MovingGameObject::setMaxSpeed(abfw::Vector3 newSpeed){
	this->maxSpeed = newSpeed;
}