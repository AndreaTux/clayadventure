#pragma once
#include "GameObject.h"
#include "..\Level.h"

class MovingGameObject : public GameObject{
protected:
	abfw::Vector3 acceleration;
	abfw::Vector3 currentSpeed;
	abfw::Vector3 maxSpeed;
	Level* level;

public:
	MovingGameObject(std::string name,abfw::Vector3 pos,abfw::Vector3 dir, abfw::Vector3 acc,Level*);

	virtual void update(float dTime);

	abfw::Vector3 getAcceleration()const;

	abfw::Vector3 getMaxSpeed()const;

	abfw::Vector3 getCurrentSpeed()const;

	virtual void setLevel(Level*);

	void setAcceleration(abfw::Vector3);

	void setMaxSpeed(abfw::Vector3);


};