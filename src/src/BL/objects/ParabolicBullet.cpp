#include "ParabolicBullet.h"
#include "..\..\util\IDMaker.h"
#include "..\EventHandler.h"
#include "..\collisions\collision_behaviour\BulletCollisionBehaviour.h"
#include "..\EventHandler.h"
#include "Bullet.h"
#include <sstream>
#include <math.h>
#define PI 3.141592653589793238463

ParabolicBullet::ParabolicBullet(abfw::Vector3 t, abfw::Vector3 pos, abfw::Vector3 dir,float speed, Level* l)
	:MovingGameObject("parabolic_bullet_",pos,dir,abfw::Vector3(0,0,0),l),timeToImpact(speed),target(t),origin(pos){

		std::stringstream ss;
		ss << IDMaker::getNext();
		name += ss.str();
		/*FIND ANGLE*/
		double x = target.x - origin.x;
		double y = target.y - origin.y;
		double g = level->getGravity().y * -60;
		float initialVelocityF = 400.f;
		double angle1, angle2;
		double tmp;

		do{
			tmp = std::pow(initialVelocityF, 4) - g * (g * std::pow(x, 2) + 2 * y * pow(initialVelocityF, 2));
			if (tmp < 0){
				initialVelocityF += 50;
			}
		}while (tmp < 0);
			// no solution
		
		if (x == 0){
			angle1 = PI/2;
			if (y > 0)
				angle2 = -PI/2;
			else
				angle2 = PI/2;
		}
		else{
			angle1 = atan((pow(initialVelocityF, 2) + sqrt(tmp)) / (g * x));
			angle2 = atan((pow(initialVelocityF, 2) - sqrt(tmp)) / (g * x));

			//if the player is on the right side
			if (x < 0){
				double fromAngleToPI = PI/2 - angle2;
				angle2 = PI/2 + fromAngleToPI;
			}
			if (x > 0){
				//double fromAngleToPI = 3/2 * PI - angle1;
				angle2 *= -1;

			}
			//angle1 += PI/2;
			//angle2 += PI/2;
		}

		//adjust for hit the center of the player
		angle2 += 0.10f;
		initialVelocity.x =  initialVelocityF * std::cos(angle2);
		initialVelocity.y =  initialVelocityF * std::sin(angle2);
		
		/* FIXED ANGLE */
		/*
		double range = abs(target.x - origin.x);
		double v0 = sqrt((range * (level->getGravity().y *60))/(2*sin(45 * PI / 180.f)* cos(45 * PI / 180.f)));
		//float v = (-10 * x)/(2*sin(45* 3.14f / 180.f)*cos(45* 3.14f / 180.f));
		//float initSpeed = x / std::cos((90 + 45) * 3.14f / 180.f);
		initialVelocity.x = v0 * std::cos((45) * PI / 180.f);
		initialVelocity.y = v0 * std::sin((45) * PI / 180.f);
		*/

		/* TIME TO IMPACT METHOD */
		/*
		initialVelocity.x = (target.x - origin.x)/timeToImpact;
		initialVelocity.y = (target.y + 0.5 * level->getGravity().y * 60 * timeToImpact * timeToImpact - origin.y)/timeToImpact;
		*/
		
		this->setCollisionBox(new CollisionBox(abfw::Vector2(position.x,position.y),32,32));
		this->setCollisionBehaviour(new BulletCollisionBehaviour(name,BulletType::ENEMY));
		currentSpeed.y = initialVelocity.y;
		currentSpeed.x = 0;
		timeCounter = 0;
		gravityAcc = 0;
		
}

void ParabolicBullet::update(float dTime){
	abfw::Vector3 presumedPosition;
	
	presumedPosition.x = (initialVelocity.x )* dTime + position.x;
	presumedPosition.y = currentSpeed.y * dTime + position.y;
	/*
	float x1 =  (0.5 * level->getGravity().y * dTime * dTime);
	float x2 = (currentSpeed.y * dTime);
	presumedPosition.y = x1 + x2 + position.y;
	*/

	abfw::Vector2 delta = abfw::Vector2(presumedPosition.x - position.x, position.y - presumedPosition.y);
	//translate the collision, to test if the movement is possible
	collision->translate(delta);
	std::vector<GameObject*> colliders = level->checkCollisions(this);

	//for each item that collides with the object, apply the behaviour of collision
	if (!colliders.empty()){
		for (std::vector<GameObject*>::iterator it = colliders.begin(); it != colliders.end(); it++){
			collision->checkCollisionSides((*it)->getBoundingBox());
			this->collisionInteraction(*it);
		}
	}
	//if there is a change in position..
	if (delta.x != 0 || delta.y != 0){
		//..update the object!
		position = presumedPosition;
		EventHandler::getInstance()->objectMoved(name,abfw::Vector3(delta.x,delta.y,0));
	}
		if (position.y >= level->getGroundHeight()){

		EventHandler::getInstance()->removeObjectFromGame(name);
	}
	currentSpeed.y -= level->getGravity().y;
	//presumedPosition.y -= level->getGravity().y;
	
}