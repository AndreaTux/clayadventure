#pragma once
#include "MovingGameObject.h"
class ParabolicBullet : public MovingGameObject{
private:
	float timeToImpact;
	abfw::Vector3 target;
	abfw::Vector3 initialVelocity;
	abfw::Vector3 origin;
	float timeCounter;
	float gravityAcc;
public:
	ParabolicBullet(abfw::Vector3 target, abfw::Vector3 pos, abfw::Vector3 dir,float flightTime,Level* l);
	virtual void update(float dTime);
};