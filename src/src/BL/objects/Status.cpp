#include "Status.h"

Status::Status(int h, int pAttack, int rAttackPower, int pDef, int rDefence, float mSpeed, float atckSpeed, float jForce)
	:hp(h),physicalAttackPower(pAttack),rangedAttackPower(rAttackPower),physicalDefence(pDef),rangedDefence(rDefence),movementSpeed(mSpeed),attackSpeed(atckSpeed),jumpForce(jForce){

}

const Status Status::operator+(const Status& other)const{
	const Status result (
		hp					+ other.hp,
		physicalAttackPower + other.physicalAttackPower,
		rangedAttackPower	+ other.rangedAttackPower,
		physicalDefence		+ other.physicalDefence,
		rangedDefence		+ other.rangedDefence,
		movementSpeed		+ other.movementSpeed,
		attackSpeed			+ other.attackSpeed,
		jumpForce			+ other.jumpForce);

		return result;
}

Status& Status::operator+=(const Status& other){
	hp					+= other.hp;
	physicalAttackPower += other.physicalAttackPower;
	rangedAttackPower	+= other.rangedAttackPower;
	physicalDefence		+= other.physicalDefence;
	rangedDefence		+= other.rangedDefence;
	movementSpeed		+= other.movementSpeed;
	attackSpeed			+= other.attackSpeed;
	jumpForce			+= other.jumpForce;

	return *this;
}

const Status Status::operator-(const Status& other)const{
	const Status result (
		hp					- other.hp,
		physicalAttackPower - other.physicalAttackPower,
		rangedAttackPower	- other.rangedAttackPower,
		physicalDefence		- other.physicalDefence,
		rangedDefence		- other.rangedDefence,
		movementSpeed		- other.movementSpeed,
		attackSpeed			- other.attackSpeed,
		jumpForce			- other.jumpForce);

		return result;
}

Status& Status::operator-=(const Status& other){
	hp					-= other.hp;
	physicalAttackPower -= other.physicalAttackPower;
	rangedAttackPower	-= other.rangedAttackPower;
	physicalDefence		-= other.physicalDefence;
	rangedDefence		-= other.rangedDefence;
	movementSpeed		-= other.movementSpeed;
	attackSpeed			-= other.attackSpeed;
	jumpForce			-= other.jumpForce;

	return *this;
}



int Status::getHp()const{
	return hp;
}

int Status::getPhysicalAttackPower()const{
	return physicalAttackPower;
}

int Status::getRangedAttackPower()const{
	return rangedAttackPower;
}

int Status::getPhysicalDefence()const{
	return physicalDefence;
}

int Status::getRangedDefence()const{
	return rangedDefence;
}

float Status::getMovementSpeed()const{
	return movementSpeed;
}

float Status::getAttackSpeed()const{
	return attackSpeed;
}

float Status::getJumpForce()const{
	return jumpForce;
}