#pragma once

class Status{

private:
	int hp;
	int physicalAttackPower;
	int rangedAttackPower;
	int physicalDefence;
	int rangedDefence;
	float movementSpeed;
	float attackSpeed;
	float jumpForce;
public:
	Status(int hp, int pAttack, int rangedAttackPower, int pDef, int rDefence, float mSpeed, float atckSpeed, float jForce);

	int getHp()const;
	int getPhysicalAttackPower()const;
	int getRangedAttackPower()const;
	int getPhysicalDefence()const;
	int getRangedDefence()const;
	float getMovementSpeed()const;
	float getAttackSpeed()const;
	float getJumpForce()const;

	const Status operator+(const Status&)const;
	Status& operator+=(const Status&);

	const Status operator-(const Status&)const;
	Status& operator-=(const Status&);
};