#include "Player.h"
#include "..\..\collisions\CollisionSideEnum.h"
#include "..\..\EventHandler.h"
#include "transformations\BallTransformation.h"
#include "transformations\MeleeTransformation.h"
#include "transformations\RangedTransformation.h"
#include "transformations\BirdTransformation.h"
#include "..\Bullet.h"
#include "..\..\collisions\collision_behaviour\BulletCollisionBehaviour.h"
#include <math.h>

Player::Player(std::string name,abfw::Vector3& pos, abfw::Vector3& dir, abfw::Vector3& sp)
	:AbstractCharacter(name,new Status (10,0,0,5,5,200,0,400),pos,dir,sp),points(0),externalForces(abfw::Vector3(0,0,0)),jumpIntention(false),stopped(true),attacking(false){
		
		behaviour = new PlayerCollisionBehaviour;
		currentTransformation = new BallTransformation();
		addTransformation(currentTransformation);
		//addTransformation(new MeleeTransformation());
		//addTransformation(new RangedTransformation());
		//addTransformation(new BirdTransformation());
		acceleration.x = status->getMovementSpeed();
		acceleration.y = status->getJumpForce();
		collision = currentTransformation->getCollisionBox();
		currentJumpAcceleration = 0;
		gravityAccumulator = 0;
		checkpoint = position;
		externalForcesCounter = abfw::Vector3(0,0,0);
		falling = false;
		invulnerable = false;
		invulnerabilityTime = invulnerabilityCounter = 2;
		shootingTime = 0.5;
		shootingCounter = 0;

		size = 1;
		maxSize = 1.8f;
		sizeChangeRate = 0.2f;
		currentSpeed = abfw::Vector3(0,0,0);
}

Player::~Player(){
	for (std::map<std::string,Transformation*>::iterator it = transformations.begin(); it != transformations.end();it++){
		delete (*it).second;
	}
}

void Player::action(){
	if (currentTransformation->getName() == "ranged"){
		//only if the time elapsed is enough
		if (shootingCounter <= 0){
			shootingCounter = shootingTime;
			currentTransformation->action();
		}
	}else{
		currentTransformation->action();
	}
}

void Player::addTransformation(Transformation* transformation){
	transformations.insert(std::pair<std::string,Transformation*>(transformation->getName(),transformation));

}

void Player::update(float dTime){

if (size >= 1){
	if (invulnerable){
		invulnerabilityCounter -= dTime;
		if (invulnerabilityCounter <= 0){
			invulnerable = false;
			invulnerabilityCounter = invulnerabilityTime;
			EventHandler::getInstance()->invulerabilityEnded();
		}
	}

 	//update the speed basing on acceleration and current direction (and eventually on external forces)
	if (externalForces.x == 0 && externalForces.y == 0)
		currentSpeed = abfw::Vector3(acceleration.x  * direction.x, currentJumpAcceleration * direction.y, 0);
	else{
		currentSpeed = externalForces;
		//currentSpeed.y += gravityAccumulator;
	}
	
	//reduce the external forces
	if (externalForcesCounter.x > 0){
		externalForcesCounter.x -= 50;
	}
	if (externalForcesCounter.y > 0){
		externalForcesCounter.y -= 50;
	}
	
	//external forces counter needed for applying THE SAME AMOUNT of force each frame
	//if the counter is zero
	if (externalForcesCounter.x == 0 && externalForcesCounter.y == 0){
		//reset the counter
		externalForcesCounter = externalForces;
		//reset external forces
		externalForces = abfw::Vector3(0,0,0);
	}
	

	//add the level gravity
	abfw::Vector3 g = level->getGravity();
	//sum gravity
	gravityAccumulator += g.y;
	if (jumping){
		//if we reached the highest point
		if (currentJumpAcceleration <= 0){
			if (currentTransformation->getName() != "bird")
				//reduce the jump acceleration
				currentJumpAcceleration -= g.y;
			else
				//if bird, reduce it by an half
				currentJumpAcceleration -= g.y/3.f;
		}// highest point
		else{
			//reduce the jump acceleration
			currentJumpAcceleration -= g.y;
		}
	}else{
		//if not jumping add the gravity
		//TODO: if the player is stopped, the gravity could be ignored
		currentSpeed.y += gravityAccumulator;
	}

	//store the presumed destination, i.e. where the player should be after this update
	abfw::Vector3 presumedDestination = position + currentSpeed * dTime;
	abfw::Vector2 delta (presumedDestination.x - position.x, presumedDestination.y - position.y);
	//translate the collision box, for checking collisions
	collision->translate(delta);

	//get the list of collision object
	std::vector<GameObject*> collisionObject = level->checkCollisions(this);

	bool outLevel = !level->inBounds(collision->getPosition());
	//if there is a collision or is outside the level
	if (!collisionObject.empty() || outLevel){
		abfw::Vector2 collisionPosition = collision->getPosition();
		//if the character is out of the level, put it into the bounds
		if (outLevel){
			if (collisionPosition.x < 0 || collisionPosition.x > level->getWidth()){
				blockHorizontal();
			}
			if (collisionPosition.y > level->getGroundHeight() || collisionPosition.y < 0){
				blockVertical();
			}
		}
		//put the collision box back, in order to check collision positions -- should be in the block code!!
		collision->translate(delta * -1);
		

		//interact with collided object, in case of collisions
		for (std::vector<GameObject *>::iterator it = collisionObject.begin(); it != collisionObject.end(); it++){
			//check collision sides for the current object
			collision->checkCollisionSides((*it)->getBoundingBox());
			//check the behaviour of the collision
			this->collisionInteraction(*it);
		}

		//restore position if position changed
		if (collision->getPosition().y != position.y){ //something changed the player position
			presumedDestination = position;
			position = abfw::Vector3(collision->getPosition().x,collision->getPosition().y,position.z);
			//only the x axis need to be recalculated
			presumedDestination.x = position.x + currentSpeed.x * dTime;
		}else{ // else recalculate both x and y

		//WARNING -- not multiplicating by the direction
		presumedDestination = position + currentSpeed * dTime;
		}
		delta = abfw::Vector2(presumedDestination.x - position.x, presumedDestination.y - position.y);
		//for (std::map<std::string,Transformation*>::iterator it = transformations.begin(); it!= transformations.end(); it++){
			//(*it).second->getCollisionBox()->translate(delta);
		//}
		collision->translate(delta);
	}

	//TODO: check little bias from collision box and player position

	//hit the ground
	if (presumedDestination.y + collision->getHeight() >= level->getGroundHeight()){
		//set the persumed destination to be equal to be on the ground
		presumedDestination.y = level->getGroundHeight() - collision->getHeight();
		collision->setPosition(abfw::Vector2(collision->getPosition().x,presumedDestination.y));
		gravityAccumulator = 0;
		currentSpeed.y = 0;

		if (jumping){
			endJump();
			attacking = false;
		}
		/*
		if (falling){
			falling = false;
			attacking = false;
			EventHandler::getInstance()->objectLanded(name,facing);
		}
		*/
	}
	//re-calculate delta
	delta = abfw::Vector2(presumedDestination.x - position.x, presumedDestination.y - position.y);
	//if there is a change in position...
	if (presumedDestination.x != position.x || presumedDestination.y != position.y){
		//falling
		if (delta.y > 0){
			if (!falling){
				falling = true;
				if (!jumping){
				EventHandler::getInstance()->objectFalling(name);
			}
			}
			if (father != 0){
				father->detatchChild(name);
				father = 0;
			}
			
		}
		//...inform the event handler
		bool spontaneous = true;
		if (externalForces.x != 0 || direction.x == 0 ){
			spontaneous = false;
		}
		EventHandler::getInstance()->objectMoved(name,abfw::Vector3(presumedDestination.x - position.x, presumedDestination.y - position.y,0),spontaneous);
		stopped = false;
		//and move the character
		position = presumedDestination;
	}else{
		if (!jumpIntention && !stopped && !jumping && !attacking){
			EventHandler::getInstance()->objectStopped(name);
			stopped = true;
		}

	}
	//if we are not moving in the y axis but we were falling, stop that
	if (delta.y == 0 && falling){
		falling = false;
		EventHandler::getInstance()->objectLanded(name,facing);
	}
	
	//update facing
	//if the x direction is not null and we have no external forces (thus we moved)
	if (direction.x != 0 && externalForces.x == 0){
		if (facing != direction.x){
			facing = direction.x;
		}
	}

	//decelerate if the characters stops
	if (direction.x == 0){
		currentSpeed.x = 0;
	}

}//still alive
 //update the shooting counter
shootingCounter -= dTime;
}

void Player::jump(){
	jumping = true;
	//minus because the force is upwards
	currentJumpAcceleration = status->getJumpForce();
	if(father != 0){
		father->detatchChild(name);
		father = 0;
	}
	jumpIntention = false;
	EventHandler::getInstance()->playerJumps();
}

bool Player::isJumping()const{
	return jumping;
}

void Player::addExternalForce(abfw::Vector3 force,abfw::Vector3 intensity){
	/*
	//if the force has no direction
	force.y = -1;
	if (force.x == 0){
		//do the opposite of the player's direction
		force = abfw::Vector3(-1 * direction.x * intensity.x, -1 * direction.y * intensity.y, 0); 
	}
	if (jumping){
		force.y = -1.5f;
	}
	externalForces = abfw::Vector3(force.x * intensity.x,force.y * intensity.y,0);
	externalForcesCounter = abfw::Vector3(std::abs(externalForces.x),std::abs(externalForces.y),0);
	*/
	abfw::Vector3 originalForce = force;
	//if no direction
	if (direction.x == 0 && direction.y == 0){
		//do the opposite of the facing
		force *= -1 * facing;
	}else{
		force = abfw::Vector3(-1 * direction.x * force.x, -1 * direction.y * force.y, 0); 
	}
	if (jumping){
		force.y = -originalForce.y * 1.5f;
	}
	externalForces = abfw::Vector3(force.x * intensity.x,force.y * intensity.y,0);
	externalForcesCounter = abfw::Vector3(std::abs(externalForces.x),std::abs(externalForces.y),0);
}

const Transformation* Player::getTransformation(std::string name)const{
	return transformations.at(name);
}

void Player::changeForm(std::string formName){

	//store current player info
	CollisionBox* oldCollisionBox = collision;
	abfw::Vector3 oldPosition = position;
	std::string oldTransformationName = currentTransformation->getName();
	//obtain the new info from the new transformation
	Transformation* newTransformation = transformations.at(formName);
	CollisionBox* newCollision = newTransformation->getCollisionBox();
	float offsetY = newCollision->getHeight() - this->collision->getHeight();
	//change position
	this->position = abfw::Vector3(position.x,position.y - offsetY,position.z);
	this->collision = newCollision;
	this->collision->setPosition(abfw::Vector2(position.x,position.y));

	//if (level->checkCollisions(this).empty() && level->inBounds(collision->getPosition())){
	if (level->tryToFit(this,50,10)){
		const Status* oldStatus = currentTransformation->getStatusAlterations();
		//subtract the old status changes
		*this->status -= *oldStatus;
		//change the current form
		currentTransformation = newTransformation;
		//add the current status changes
		*this->status += *currentTransformation->getStatusAlterations();
		//update accelerationf
		acceleration.x = status->getMovementSpeed();
		acceleration.y = status->getJumpForce();
		//inform GUI that the player changed his form
		EventHandler::getInstance()->playerChangeForm(oldTransformationName,formName);
		//stop attacking
		if (attacking){
			attacking = false;
		}
	}else{
		this->collision = oldCollisionBox;
		position = oldPosition;
	}	

	/*
	if (formName == "bird"){
		permanentExternalForces = abfw::Vector3(0,-10,0);
	}else{
		permanentExternalForces= abfw::Vector3(0,0,0);
	}*/
	
}

const Transformation* Player::getCurrentTransformation()const{
	return currentTransformation;
}

void Player::shoot(){
	abfw::Vector3 startinBulletPosition;
	
	//player faces right
	if (facing == 1){
		startinBulletPosition = abfw::Vector3(position.x + collision->getWidth() + 0.5, position.y + collision->getHeight()/2 - 10, position.z);
	}else
		if (facing == -1){
		startinBulletPosition = abfw::Vector3(position.x - 16.5, position.y + collision->getHeight()/2 - 10, position.z);
	}
	
		EventHandler::getInstance()->addObjectToGame(new Bullet(startinBulletPosition,abfw::Vector3(facing,0,0),230,200,level,BulletType::SHOT),"ball_projectile.png");
		EventHandler::getInstance()->playerAction();
			
	

}
const int Player::getPoints()const{
	return points;
}

void Player::gainPoints(int value){
	points+= value;
}

float Player::grow(){
	if (size < maxSize){
		size += sizeChangeRate;
		for (std::map<std::string,Transformation*>::iterator it = transformations.begin(); it!= transformations.end(); it++){
			(*it).second->getCollisionBox()->scale(size);
		}
		position = abfw::Vector3(collision->getPosition().x, collision->getPosition().y,position.z);
	}
	attacking = false;
	return size;
}

float Player::shrink(){
	size -= sizeChangeRate;
	if (size < 1){
		//announce death
		size = 0;
	}else{
		for (std::map<std::string,Transformation*>::iterator it = transformations.begin(); it!= transformations.end(); it++){
			(*it).second->getCollisionBox()->scale(size);
		}
		position = abfw::Vector3(collision->getPosition().x,collision->getPosition().y,position.z);
		invulnerable = true;
	}
	attacking = false;
	//speed.x -= 5;
	return size;
}

float Player::getSize()const{
	return size;
}

float Player::getMaxSize()const{
	return maxSize;
}


// factorize with shoot
void Player::smash(){
	abfw::Vector3 startinBulletPosition;
	Bullet* bullet;
	//player faces right
	if (facing == 1){
		startinBulletPosition = abfw::Vector3(position.x + collision->getWidth() + 0.5, position.y + collision->getHeight()/2, position.z);
	}else
		if (facing == -1){
		startinBulletPosition = abfw::Vector3(position.x - 16.5, position.y + collision->getHeight()/2, position.z);
	}
	bullet = new Bullet(startinBulletPosition,abfw::Vector3(facing,0,0),190,10,level,BulletType::MELEE);
	bullet->setCollisionBox(new CollisionBox(abfw::Vector2(startinBulletPosition.x,startinBulletPosition.y),16,32));
	level->addGameObject(bullet);
	//EventHandler::getInstance()->addObjectToGame(bullet);
}

void Player::goToCheckPoint(){
	//TODO: do some checks in the new position: is it free? is it in the level bounds?
	abfw::Vector3 delta = checkpoint - position;
	//add the current shape height to the checkpoint position plus an offset
	float offset = 50;
	delta.y -= currentTransformation->getCollisionBox()->getHeight() + offset; 
	position = position + delta;
	currentSpeed.x = currentSpeed.y = 0;
	collision->translate(abfw::Vector2(delta.x,delta.y));
	EventHandler::getInstance()->objectMoved(name,delta);
}

void Player::setCheckpoint(abfw::Vector3 checkpoint){
	this->checkpoint = checkpoint;
}

void Player::addFather(GameObject* obj){
	father = obj;
}

void Player::intentionToJump(){
	jumpIntention = true;
}


bool Player::isFalling(){
	return falling;
}

bool Player::isInvulnerable(){
	return invulnerable;
}

bool Player::isAttacking(){
	return attacking;
}

void Player::setAttacking(bool a){
attacking = a;
	
}


void Player::blockVertical(){
	if (jumping || falling){
		attacking = false;
	}
	AbstractCharacter::blockVertical();
}

void Player::blockHorizontal(){
	if (jumping || falling){
		attacking = false;
	}
	AbstractCharacter::blockHorizontal();
}