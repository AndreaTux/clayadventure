#pragma once
#include "..\AbstractCharacter.h"
#include "..\..\Level.h"
#include <vector>

class Transformation;

class Player : public AbstractCharacter{
private:

	std::map<std::string,Transformation*> transformations;
	Transformation* currentTransformation;
	int points;
	abfw::Vector3 checkpoint;
	abfw::Vector3 externalForces;
	bool invulnerable;
	double invulnerabilityTime;
	double invulnerabilityCounter;

	double shootingTime;
	double shootingCounter;
	
	float jumpIntention;
	abfw::Vector3 externalForcesCounter;
	
	float size;
	float maxSize;
	float sizeChangeRate;
	bool stopped;
	//flag for melee attack: true if the animation of attacking is not finished yet.
	//This is also set by the GUI component via events
	bool attacking;

public:
	Player(std::string name, abfw::Vector3& pos, abfw::Vector3& dir, abfw::Vector3& sp);
	~Player();
	void update(float dTime);
	void action();
	void jump();

	void blockVertical();
	void blockHorizontal();
	
	bool isJumping()const;
	void addTransformation(Transformation*);
	const Transformation* getTransformation(std::string)const;
	const Transformation* getCurrentTransformation()const;
	const int getPoints()const;
	void gainPoints(int value);
	void addExternalForce(abfw::Vector3,abfw::Vector3);

	void changeForm(std::string formName);
	void shoot();
	float grow();
	float shrink();
	void smash();

	float getSize()const;
	float getMaxSize()const;

	void goToCheckPoint();
	void setCheckpoint(abfw::Vector3);

	void addFather(GameObject*);

	void intentionToJump();
	bool isFalling();

	bool isInvulnerable();
	bool isAttacking();

	void setAttacking(bool attacking);
};