#pragma once
#include "Transformation.h"

class BirdTransformation : public Transformation{
public:
	BirdTransformation();

	virtual void action();
};