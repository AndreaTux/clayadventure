#include "MeleeTransformation.h"
#include "..\..\..\EventHandler.h"
#include "..\..\..\collisions\collision_behaviour\SimpleBlockCollisionBehaviour.h"

MeleeTransformation::MeleeTransformation()
	:Transformation("melee",Status(0,5,0,10,10,-100,0,-100),new CollisionBox(abfw::Vector2(0,0),32,62)){
}

void MeleeTransformation::action(){
	EventHandler::getInstance()->playerSmashTriggered();
}