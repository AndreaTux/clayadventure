#pragma once
#include "Transformation.h"

class MeleeTransformation : public Transformation{
public:
	MeleeTransformation();

	virtual void action();
};