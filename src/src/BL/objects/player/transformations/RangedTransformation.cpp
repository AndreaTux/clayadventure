#include "RangedTransformation.h"
#include "..\..\..\EventHandler.h"
#include "..\..\..\collisions\collision_behaviour\SimpleBlockCollisionBehaviour.h"

RangedTransformation::RangedTransformation()
	:Transformation("ranged",Status(0,5,0,10,10,-50,0,-50),new CollisionBox(abfw::Vector2(0,0),52,32)){
}

void RangedTransformation::action(){
	//EventHandler::getInstance()->writeOnScreen(40,300,"|Ranged| Shoot!");
	EventHandler::getInstance()->playerShoots();
}
