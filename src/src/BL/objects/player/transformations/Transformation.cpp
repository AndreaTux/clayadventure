#include "Transformation.h"

Transformation::Transformation(std::string n, Status delta,CollisionBox* coll)
	:name(n), deltaStatus(delta),collision(coll) {}

Transformation::~Transformation(){
	delete collision;
}

const Status* Transformation::getStatusAlterations()const{
	return &deltaStatus;
}

const std::string Transformation::getName()const{
	return name;
}

CollisionBox* Transformation::getCollisionBox()const{
	return collision;
}