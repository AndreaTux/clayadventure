#pragma once 
#include "..\player.h"
#include "..\..\..\collisions\CollisionBox.h"
#include <string>

class Transformation {
protected:
	const Status deltaStatus;
	CollisionBox* collision;
	std::string name;

public:
	Transformation(std::string name, Status delta, CollisionBox*);
	~Transformation();

	const Status* getStatusAlterations()const;

	CollisionBox* getCollisionBox()const;
	/*
	* Defines the behaviour of the action of the transformation (when pressed the action button).
	*/
	virtual void action() = 0;

	const std::string getName()const;
};