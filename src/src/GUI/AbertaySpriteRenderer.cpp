#include "AbertaySpriteRenderer.h"
#include <maths\math_utils.h>

AbertaySpriteRenderer::AbertaySpriteRenderer(abfw::Platform& platform)
	:abertayRenderer(platform.CreateSpriteRenderer()){
		font = new abfw::Font();
		font->Load("comic_sans", platform);
}

AbertaySpriteRenderer::~AbertaySpriteRenderer(){
	delete font;
}

void AbertaySpriteRenderer::renderSprite(abfw::Sprite* sprite){
	abertayRenderer->DrawSprite(*sprite);
}

void AbertaySpriteRenderer::begin(){
	abertayRenderer->Begin();
}

void AbertaySpriteRenderer::end(){
	abertayRenderer->End();
}

void AbertaySpriteRenderer::renderText(int x, int y, std::string text, abfw::Colour col){
	font->RenderText(abertayRenderer, abfw::Vector3(x,y,0.f),1.0f, col.GetRGBA(), abfw::TJ_LEFT, text.c_str());
}

void AbertaySpriteRenderer::setProjectionMatrix(abfw::Matrix44 mat){
	abertayRenderer->set_projection_matrix(mat);
}