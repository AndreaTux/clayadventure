#pragma once
#include <graphics\sprite_renderer.h>
#include <system\platform.h>
#include <graphics\sprite.h>
#include "Renderer.h"
#include <graphics\font.h>
#include <graphics\Colour.h>

class AbertaySpriteRenderer : public Renderer{
private:
	abfw::SpriteRenderer* abertayRenderer;
	abfw::Vector3 camera_eye;
	abfw::Vector3 camera_lookat;
	abfw::Vector3 camera_up;
	float camera_fov;
	float near_plane;
	float far_plane;
	abfw::Font* font;
public:
	
	AbertaySpriteRenderer(abfw::Platform& platform);
	~AbertaySpriteRenderer();
	virtual void setProjectionMatrix(abfw::Matrix44);
	virtual void renderSprite(abfw::Sprite*);
	virtual void renderText(int x, int y, std::string, abfw::Colour = abfw::Colour(1,1,1));
	virtual void begin();
	virtual void end();

};