#include"AbertaySpriteScene.h"
#include "AbertaySpriteRenderer.h"

AbertaySpriteScene::AbertaySpriteScene()
	:player(0){
}

void AbertaySpriteScene::setPlayer(SpritePlayer* p){
	Scene::addObject(p);
	player = p;
}

void AbertaySpriteScene::render(Renderer* brenderer){
	//TODO ugly cast...
	abfw::Matrix44 projection;
	projection.OrthographicFrustumLHD3D(camera->getLeft(),camera->getRight(),camera->getTop(),camera->getBottom(),camera->getNearPlane(),camera->getFarPlane());
	brenderer->setProjectionMatrix(projection);
	brenderer->begin();
	AbertaySpriteRenderer* renderer = dynamic_cast<AbertaySpriteRenderer*>(brenderer);
	for (std::map<std::string,abfw::Sprite*>::iterator it = sprites.begin(); it != sprites.end(); it++){
		renderer->renderSprite(it->second);
	}
	//render the player
	if(player != 0 && player->isVisible()){
		renderer->renderSprite(player->getSprite());
	}
	if (debug)
		drawDebug(renderer);
	brenderer->end();
}

void AbertaySpriteScene::addObject(SceneObject* obj){
	Scene::addObject(obj);
	AbertaySpriteSceneObject* spriteObj = dynamic_cast<AbertaySpriteSceneObject*>(obj);
	sprites[obj->getName()] = spriteObj->getSprite();
	//debug one
	AbertaySpriteSceneObject* s = new AbertaySpriteSceneObject(spriteObj);
	debugSprites[obj->getName()] = s->getSprite();
}

void AbertaySpriteScene::removeObject(std::string name){
	Scene::removeObject(name);
	sprites.erase(name);
	debugSprites.erase(name);
}

void AbertaySpriteScene::drawDebug(Renderer* brenderer){
	AbertaySpriteRenderer* renderer = dynamic_cast<AbertaySpriteRenderer*>(brenderer);
	for (std::map<std::string,abfw::Sprite*>::iterator it = debugSprites.begin(); it != debugSprites.end(); it++){
		if ((*it).first != "background"){
			
			if (sprites.find((*it).first) != sprites.end()){
				(*it).second->set_position(sprites[(*it).first]->position());
			}
			renderer->renderSprite((*it).second);
		}
	}
}

void AbertaySpriteScene::addObjectToDebugList(SceneObject* obj){
	AbertaySpriteSceneObject* aObj = dynamic_cast<AbertaySpriteSceneObject*>(obj);
	debugSprites[obj->getName()] = aObj->getSprite();
}
