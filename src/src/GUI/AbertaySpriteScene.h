#pragma once
#include <map>
#include "SpritePlayer.h"
#include "Scene.h"
#include <graphics\sprite.h>
#include "AbertaySpriteSceneObject.h"

class AbertaySpriteScene :public Scene{
private:
	std::map<std::string,abfw::Sprite*> sprites;
	std::map<std::string,abfw::Sprite*> debugSprites;
	SpritePlayer* player;
public:
	AbertaySpriteScene();
	virtual void setPlayer(SpritePlayer*);
	virtual void render(Renderer*);
	virtual void addObject(SceneObject*);
	virtual void removeObject(std::string);
	virtual void drawDebug(Renderer*);
	virtual void addObjectToDebugList(SceneObject*);
};