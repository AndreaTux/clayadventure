#include "AbertaySpriteSceneObject.h"
#include "SpriteSheetAnimation.h"
#include <graphics\sprite.h>
#include <system\platform.h>
#include <graphics\image_data.h>
#include <assets\png_loader.h>
#include "gui_utils\GUIUtils.h"
#include "..\util\ConfigReader.h"
#include "SpriteSheetKeyFrameAnimation.h"
#include "GUIEventHandler.h"
#include "animation\AnimationStateMachine.h"
#include "..\util\StringUtil.h"

AbertaySpriteSceneObject::AbertaySpriteSceneObject(std::string n,std::string texturePath, float width, float height, abfw::Vector3 pos, std::string side)
	:SceneObject(n,pos),originalHeight(height),originalWidth(width),side(side),animationFinishedEventSent(false){
	
	sprite = new abfw::Sprite;
	sprite->set_width(width);
	sprite->set_height(height);
	
	if (texturePath != "null" && texturePath != ""){
		texture = GUIUtils::createTextureFormFile(texturePath);
		sprite->set_texture(texture);
	}
	
	sprite->set_position(pos);
	int raceIndex = n.find("_");
	animationManager = new AnimationStateMachine;
	if (raceIndex != -1){
		loadAnimations(n.substr(0,raceIndex) + ".xml");
	}
		loadAnimations(n + ".xml");
	
	
}

AbertaySpriteSceneObject::AbertaySpriteSceneObject(const AbertaySpriteSceneObject* other)
	:SceneObject(other->getName(),other->position),originalHeight(other->originalHeight),originalWidth(other->originalWidth){
		sprite = new abfw::Sprite;
		//just for debugging, so TODO: move it outside (when debugging is needed)
		sprite->set_colour(abfw::Colour(1,1,1,0.6f).GetABGR());
		sprite->set_height(other->sprite->height());
		sprite->set_width(other->sprite->width());
		sprite->set_position(abfw::Vector3(position.x,position.y,-1));
}

AbertaySpriteSceneObject::~AbertaySpriteSceneObject(){
	delete texture;
	delete sprite;
	delete animationManager;
}

void AbertaySpriteSceneObject::setColour(abfw::Colour col){
	sprite->set_colour(col.GetABGR());
}

void AbertaySpriteSceneObject::update(float dTime){
	sprite->set_position(position);
	if (!animationManager->isEmpty())
		animationManager->updateAnimation(this,dTime);
	
}

void AbertaySpriteSceneObject::updatePosition(abfw::Vector3 delta,bool autonomous){
	//change side only if the object moved intentionally
	if (autonomous){
		if (delta.x > 0 && side != "right"){
			side = "right";
		}
		if (delta.x < 0 && side != "left"){
			side = "left";
		}
	}
	SceneObject::updatePosition(delta,autonomous);
}

abfw::Sprite* AbertaySpriteSceneObject::getSprite()const{
	return sprite;
}

void AbertaySpriteSceneObject::scale(float newSize){
	float oldHeight = sprite->height();
	float oldWidth	= sprite->width();
	sprite->set_height(originalHeight * newSize);
	sprite->set_width(originalWidth * newSize);
	//difference between the scaled and the actual position
	abfw::Vector3 deltaSize ((oldWidth - sprite->width())/2.f, (oldHeight - sprite->height())/2.f,0) ;
	position += deltaSize;
	sprite->set_position(position);
}


float AbertaySpriteSceneObject::getOriginalHeight(){
	return originalHeight;
}

float AbertaySpriteSceneObject::getOriginalWidth(){
	return originalWidth;
}

void AbertaySpriteSceneObject::loadAnimations(std::string filePath){
	ConfigReader* reader = new ConfigReader(filePath);
	//if the animation file exists
	if (reader->isValid()){
		//get the spritesheets --maybe unnecessary
		ConfigReader* spriteSheetsReader = reader->getConfigReader("spritesheets");
		if(spriteSheetsReader != 0){
			//get all the spritesheets
			std::vector<ConfigReader*> readers = spriteSheetsReader->getConfigReaderList("spritesheet");
				//for each spritesheet
				for (std::vector<ConfigReader*>::iterator it = readers.begin(); it != readers.end(); it++){
					//get the info
					std::string clusterName	= (*it)->getString("name");
					int rows				= (*it)->getInteger("row");
					int cols				= (*it)->getInteger("column");
					
					//get each animation for the current spritesheet
					std::vector<ConfigReader*> innerReader = (*it)->getConfigReaderList("animation");
					//for each animation
					for(std::vector<ConfigReader*>::iterator anim = innerReader.begin(); anim != innerReader.end(); anim++){
						//get its info
						Animation* currentAnimation;
						std::string name			= (*anim)->getString("name");
						int begin					= (*anim)->getInteger("begin");
						int end						= (*anim)->getInteger("end");
						float duration				= (*anim)->getFloat("duration");
						std::string keyFrames		= (*anim)->getString("keyframes");
						
						//take the side of the animation (format is: NAME_SIZE)
						int pos = clusterName.find('_');
						std::string sideOfAnimation = "";
						
						if (pos != -1)
							sideOfAnimation = clusterName.substr(pos);
						//loading more time the same texture..fix this with a resource manager
						if (keyFrames != ""){
							
							std::vector<std::string> tokens = StringUtil::splitStringByDelimiter(",",keyFrames);
							std::map<int,bool> keyframesMap;

							for (std::vector<std::string>::iterator it = tokens.begin(); it != tokens.end(); it++){
								keyframesMap[atoi((*it).c_str())] = true;
							}
							
							SpriteSheetKeyFrameAnimation* tempAnimation =  new SpriteSheetKeyFrameAnimation(name + sideOfAnimation,clusterName + ".png",duration,(end-begin)+1,rows,cols,this,begin,keyframesMap);
							//put the destroyed callback
							if (name.substr(0,name.find("_")) == "destroyed"){
								tempAnimation->setCallbackAction(end, [this] () {GUIEventHandler::getInstance()->sceneObjectRemoved(this->name);});
							}
							currentAnimation = tempAnimation;
						}else{
							currentAnimation = new SpriteSheetAnimation(name + sideOfAnimation,clusterName + ".png",duration,abs(end-begin)+1,rows,cols,this,begin);
						}
						//animation specific adjustment
						if (name == "jump"){
							currentAnimation->setHoldOnLastFrame(true);
						}
						
						//to be deleted
						insertAnimation(currentAnimation);

						animationManager->addAnimation(currentAnimation);

				}
			}
		}//if spritesheets 
		
		// check for the initial animation state
		if(reader->getConfigReader("state") != 0){
			std::string stateName = reader->getConfigReader("state")->getString("name");
			if (stateName != ""){
				this->animationManager->setDefaultAnimation(animations[stateName]);
			}
		}
		//check for initial side
		if(reader->getConfigReader("side") != 0){
			std::string value = reader->getConfigReader("side")->getString("value");
			if (value != ""){
				this->side = value;
			}
		}

	}//if is valid
	delete reader;
}

std::string AbertaySpriteSceneObject::getSide(){
	return side;
}

void AbertaySpriteSceneObject::setSide(std::string s){
	side = s;
}
