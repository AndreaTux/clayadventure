#pragma once
#include "SceneObject.h"
#include <graphics/sprite.h>
#include <graphics/colour.h>
#include <system/platform.h>
#include <string>
#include "SpriteSheetAnimationCluster.h"

class AbertaySpriteSceneObject : public SceneObject{
protected:
	abfw::Sprite* sprite;
	const abfw::Texture* texture;
	float originalWidth, originalHeight;
	std::string side;
	bool animationFinishedEventSent;

protected:
	void loadAnimations(std::string descriptionFilePath);
public:
	AbertaySpriteSceneObject(std::string name, std::string texturePath, float width, float height, abfw::Vector3 pos, std::string side = "right");
	AbertaySpriteSceneObject(const AbertaySpriteSceneObject*);
	~AbertaySpriteSceneObject();
	//just for testing -- until texture will be available
	void setColour(abfw::Colour);

	virtual void update(float dTime);
	
	virtual void updatePosition (abfw::Vector3,bool a);

	abfw::Sprite* getSprite()const;

	virtual void scale(float newSize);

	float getOriginalWidth();
	float getOriginalHeight();

	std::string getSide();

	void setSide(std::string);

};