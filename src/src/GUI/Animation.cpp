#include "Animation.h"

Animation::Animation (std::string n, float duration, int frameN)
	:name(n),frameDuration(duration),numberOfFrames(frameN),frameCount(frameDuration),currentFrame(0),reverse(false),state(AnimationState::STOPPED),holdOnLastFrame(false){
}

void Animation::stop(){
	currentFrame = 0;
	//frameCount = frameDuration;
	state = AnimationState::STOPPED;
	reverse = false;
}

void Animation::play(bool loop){
	this->loop = loop;
	state = PLAYING;
}

void Animation::pause(){
	state = AnimationState::PAUSE;
}


int Animation::getNumberOfFrames()const{
	return numberOfFrames;
}

float Animation::getFrameDuration()const{
	return frameDuration;
}

const std::string Animation::getName()const{
	return name;
}

void Animation::speed(float speedDelta){
	numberOfFrames *= speedDelta;
}

AnimationState Animation::getState()const{
	return state;
}

void Animation::setReverse(bool value){
	reverse = value;
}

void Animation::revert(){
	reverse = !reverse;
}

bool Animation::isReverted(){
	return reverse;
}

void Animation::setHoldOnLastFrame(bool b){
	holdOnLastFrame = b;
}

void Animation::playFromFrame(int frame, bool loop){
	currentFrame = frame;
	play(loop);
}

const std::string Animation::getTypeName()const{
	return typeName;
}