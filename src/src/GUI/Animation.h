#pragma once
#include <string>

enum AnimationState{
	PLAYING = 0,
	PAUSE = 1,
	STOPPED = 2
};



class Animation{
protected:
	//update rate, the time needed for one frame to be displayed
	float frameDuration;
	float frameCount;
	int numberOfFrames;
	int currentFrame;
	bool reverse;
	std::string name;
	std::string typeName;
	AnimationState state;
	bool loop;
	bool holdOnLastFrame;
public:

	Animation (std::string name, float frameDuration, int numberOfFrames);

	virtual void play(bool loop) = 0;
	virtual void playFromFrame(int startFrame, bool loop);
	virtual void update(float dTime) = 0;
	virtual void stop();
	virtual void pause();
	void setReverse(bool);
	void setHoldOnLastFrame(bool);
	void revert();
	bool isReverted();

	int getNumberOfFrames()const;
	float getFrameDuration()const;
	const std::string getName()const;
	const std::string getTypeName()const;
	void speed(float speedDelta);
	AnimationState getState()const;
	
};

