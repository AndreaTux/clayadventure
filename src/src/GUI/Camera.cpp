#include "Camera.h"

Camera::Camera(float l, float r, float t, float b, float near, float far)
	:left(l),right(r),top(t),bottom(b),nearPlane(near),farPlane(far),rightBound(0),bottomBound(0),topBound(0){
		windowWidth = windowHeight = 0;
}


void Camera::move(abfw::Vector3 delta){
	//boundaries

	//limit from the left/right side
	if (left + delta.x >= 0 && right + delta.x <= rightBound ){
		left	+= delta.x;
		right	+= delta.x;
	}
	//limit from the top/bottom side
	if( top + delta.y >= topBound && bottom + delta.y <= bottomBound ){
 		top		+= delta.y;
		bottom	+= delta.y;
	}
}

float Camera::getLeft(){
	return left;
}
float Camera::getRight(){
	return right;
}
float Camera::getTop(){
	return top;
}
float Camera::getBottom(){
	return bottom;
}
float Camera::getNearPlane(){
	return nearPlane;
}
float Camera::getFarPlane(){
	return farPlane;
}

void Camera::setBoundaries(float bottom,float top, float right){
	topBound = top;
	bottomBound = bottom;
	rightBound = right;
}

void Camera::setWindowSize(int w, int h){
	windowWidth = w;
	windowHeight = h;
}

