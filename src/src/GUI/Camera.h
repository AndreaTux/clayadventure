#pragma once 
#include <maths\vector3.h>

class Camera{
protected:
	float left;
	float right;
	float top;
	float bottom;

	float nearPlane;
	float farPlane;

	float bottomBound;
	float rightBound;
	float topBound;

	int windowWidth;
	int windowHeight;

public:
	Camera(float left, float right, float top, float bottom, float near = 0.1f, float = 100);

	virtual void move(abfw::Vector3 delta);
	//It is assumed that the level has 0,0 as the left and top bounds
	void setBoundaries(float bottomBound, float topBound, float rightBound);
	void setWindowSize(int w, int h);

	float getLeft();
	float getRight();
	float getTop();
	float getBottom();
	float getNearPlane();
	float getFarPlane();
};