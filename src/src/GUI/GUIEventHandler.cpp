#include "GUIEventHandler.h"
#include "HUD.h"
#include "AbertaySpriteScene.h"
#include "..\BL\EventHandler.h"
#include "animation\AnimationManager.h"

GUIEventHandler* GUIEventHandler::instance = 0;
SpritePlayer* GUIEventHandler::player = 0;
Renderer* GUIEventHandler::renderer = 0;
AbertaySpriteScene* GUIEventHandler::scene = 0;

GUIEventHandler::GUIEventHandler(){}

void GUIEventHandler::init(Renderer* rend){
	renderer = rend;
}

GUIEventHandler* GUIEventHandler::getInstance(){
	if (instance == 0){
		instance = new GUIEventHandler;
	}
	return instance;
}


void GUIEventHandler::objectMoved(std::string who, abfw::Vector3 delta,bool byUser){
	scene->moveObject(who,delta,byUser);
}

void GUIEventHandler::writeOnScreen(int x, int y, std::string text){
	HUD::getInstance()->addText(x,y,text);
}

void GUIEventHandler::playerChangedForm(std::string oldFormName, std::string formName){
	//player->changeForm(formName);
	dynamic_cast<SpritePlayer*>(scene->getObject("player"))->changeForm(formName);
	
	HUD::getInstance()->getSprite(oldFormName)->set_colour(abfw::Colour(1,1,1,.3f).GetABGR());
	HUD::getInstance()->getSprite(formName)->set_colour(abfw::Colour(1,0.8f,0.4f).GetABGR());
	
}

void GUIEventHandler::addBulletToScene(std::string name, std::string texturePath, abfw::Vector3 pos){
	scene->addObject(new AbertaySpriteSceneObject(name,texturePath,16,16,pos));
}

void GUIEventHandler::removeSceneObject(std::string name){
	scene->removeObject(name);
}

void GUIEventHandler::scaleObj(std::string name, float newSize){
	scene->scaleObject(name,newSize);
}

void GUIEventHandler::objectStopped(std::string who)const{
	SceneObject* obj = scene->getObject(who);
	if ( obj != 0){
		obj->getAnimationManager()->objectStopped(scene->getObject(who));
	}
}

void GUIEventHandler::jumpRequest(){
	/*
	AbertaySpriteSceneObject* player = (AbertaySpriteSceneObject*)scene->getObject("player");
	player->playAnimation("jump_" + player->getSide() ,false);
	*/
	scene->getObject("player")->getAnimationManager()->objectJumped(scene->getObject("player"));

}
void GUIEventHandler::addObjectToScene(std::string name, std::string texturepath, abfw::Vector3 pos, float w, float h, std::string side){
	AbertaySpriteSceneObject* object = new AbertaySpriteSceneObject(name,texturepath,w,h,abfw::Vector3(pos.x + w/2.f, pos.y + h/2.f,1),side);
	scene->addObject(object);
}

void GUIEventHandler::addPlayer(abfw::Vector3 pos, float width, float height){
	SpritePlayer* player = new SpritePlayer(width,height,pos);
	dynamic_cast<AbertaySpriteScene*>(scene)->setPlayer(player);
}

void GUIEventHandler::newScene(){
	if (scene != 0){
		delete scene;
	}
	scene = new AbertaySpriteScene;
}

AbertaySpriteScene* GUIEventHandler::getCurrentScene(){
	return scene;
}

void GUIEventHandler::setDebugMode(bool d){
	scene->setDebug(d);
}

void GUIEventHandler::addObjectToDebugList(std::string name, std::string texturePath, abfw::Vector3 pos, float w, float h){
	AbertaySpriteSceneObject* object = new AbertaySpriteSceneObject(name,texturePath,w,h,abfw::Vector3(pos.x + w/2.f, pos.y + h/2.f,-1));
	object->setColour(abfw::Colour(1,1,1,0.6f));
	scene->addObjectToDebugList(object);
}

void GUIEventHandler::playerAction(int side){
	/*
	SceneObject* player = scene->getObject("player");
	std::string sideStr = "_left";
	if (side == 1){
		sideStr = "_right";
	}
	player->stackAndPlayAnimation("attack" + sideStr,false);
	*/
	scene->getObject("player")->getAnimationManager()->objectAttacked(scene->getObject("player"));
}

void GUIEventHandler::objectLanded(std::string n, std::string side){
	SceneObject* obj = scene->getObject(n);
	if ( obj != 0){
		obj->getAnimationManager()->objectLanded(scene->getObject(n));
	}
}

void GUIEventHandler::playAnimation(std::string who, std::string animation, bool loop){
	SceneObject* obj = scene->getObject(who);
	if ( obj != 0){
		obj->getAnimationManager()->playAnimation(animation,loop);
	}
}

void GUIEventHandler::animationFinished(std::string who, std::string animation){
	EventHandler::getInstance()->animationFinished(who,animation);
}

void GUIEventHandler::objectFalling(std::string who){
	scene->getObject(who)->getAnimationManager()->objectFell(scene->getObject(who));
}

void GUIEventHandler::objectHit(std::string who){
	scene->getObject(who)->getAnimationManager()->objectHit(scene->getObject(who));
}

void GUIEventHandler::sceneObjectRemoved(std::string who){
	removeSceneObjectInNextFrame(who);
	EventHandler::getInstance()->objectRemoved(who);
}

void GUIEventHandler::removeSceneObjectInNextFrame(std::string who){
	scene->removeObjectInNextFrame(who);
}

void GUIEventHandler::objectRespawn(std::string name){
	SceneObject* obj = scene->getObject(name);
	if ( obj != 0){
		obj->getAnimationManager()->resetCurrentAnimation();
	}
}

void GUIEventHandler::invulnerabilityEnded(){
	((SpritePlayer*)scene->getObject("player"))->invulnerabilityEnded();
}

void GUIEventHandler::playerAttacks(){
	EventHandler::getInstance()->playerAttackStarted();
}

void GUIEventHandler::playerFinishesAttacking(){
	EventHandler::getInstance()->playerAttackEnded();
}

void GUIEventHandler::powerupCollected(std::string name){
	HUD::getInstance()->addSpriteToColorInterpolationList(name);
}

void GUIEventHandler::firstTimeTransformation(std::string transformatioName){
	HUD::getInstance()->stopInterpolating(transformatioName);
}