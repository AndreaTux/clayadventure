#pragma once
#include "AbertaySpriteScene.h"
#include "Renderer.h"
#include "SpritePlayer.h"
#include <string>
#include <system\Platform.h>

class GUIEventHandler {
private:
	static AbertaySpriteScene* scene;
	static Renderer* renderer;
	static SpritePlayer* player;

	static GUIEventHandler* instance;

	GUIEventHandler();

public:
	void init (Renderer*);
	static GUIEventHandler* getInstance();
	virtual void writeOnScreen(int x, int y, std::string text);
	virtual void objectMoved(std::string who, abfw::Vector3 delta,bool byUser);
	virtual void objectStopped(std::string who)const;
	virtual void playerChangedForm (std::string oldFormName, std::string name);

	virtual void addBulletToScene(std::string, std::string, abfw::Vector3 pos);
	//to create the level..right place?
	void addObjectToScene(std::string name, std::string texturepath, abfw::Vector3 pos, float width, float height,std::string initSide = "right");
	void addPlayer(abfw::Vector3 pos, float width, float height);

	//from BL
	virtual void removeSceneObject(std::string);
	//from GUI
	virtual void sceneObjectRemoved(std::string);
	virtual void removeSceneObjectInNextFrame(std::string);

	virtual void scaleObj (std::string, float newSize);
	virtual void jumpRequest();

	void newScene();

	void setDebugMode(bool);
	void addObjectToDebugList(std::string name, std::string texturepath, abfw::Vector3 pos, float width, float height);
	
	void playerAction(int side);
	void objectLanded(std::string name,std::string side);
	void playAnimation(std::string who, std::string animation, bool loop);
	void animationFinished(std::string who, std::string animation);

	void objectFalling(std::string);

	void objectHit(std::string);
	void objectRespawn(std::string);

	void invulnerabilityEnded();

	AbertaySpriteScene* getCurrentScene();

	void playerAttacks();
	void playerFinishesAttacking();

	void powerupCollected(std::string name);
	void firstTimeTransformation(std::string transformationName);
};