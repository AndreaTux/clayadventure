#include "HUD.h"
#include "AbertaySpriteRenderer.h"

HUD* HUD::instance = 0;

HUD* HUD::getInstance(){
	if (instance == 0){
		instance = new HUD();
	}

	return instance;
}

void HUD::cleanUp(){
	//delete instance;
}

HUD::HUD()
	:interpolationSign(-1){
}

void HUD::addText(int x, int y, const std::string text, abfw::Colour col){
	textList.push_back(TextInfo(x,y,text,col));
}

void HUD::removeText(TextInfo* info){

}

void HUD::draw(Renderer* renderer){
	for (std::vector<TextInfo>::iterator it = textList.begin(); it != textList.end(); ){
		renderer->renderText(it->x, it->y, it->text,it->col);
		it = textList.erase(it);
	}
	//UGLY
	AbertaySpriteRenderer* spriteRenderer = dynamic_cast<AbertaySpriteRenderer*>(renderer);
	for (std::map<std::string,abfw::Sprite*>::iterator it = sprites.begin(); it != sprites.end(); it++){
		spriteRenderer->renderSprite(it->second);
	}
	
}

void HUD::addSprite(std::string name,abfw::Sprite* sprite){
	sprites[name] = (sprite);
}

abfw::Sprite* HUD::getSprite(std::string name){
	return sprites[name];
}

void HUD::update (float dTime){
	if (!colorInterpolationSprites.empty()){
		//hardcoded
		for (std::vector<std::string>::iterator it = colorInterpolationSprites.begin(); it != colorInterpolationSprites.end(); it++){
			if (interpolationSign == -1)
				interpolateSpriteColor (dTime,*it,abfw::Vector3(.5,.2f,.6f),abfw::Vector3(1,0.8f,0.4f));
			else
				if (interpolationSign == 1)
					interpolateSpriteColor (dTime,*it,abfw::Vector3(1.5,1,0.8f),abfw::Vector3(1,1,1));

		}
	}
}

void HUD::interpolateSpriteColor(float dTime, std::string spriteName, abfw::Vector3 interpolationRate, abfw::Vector3 finalColor){
	abfw::Colour currentColor;
	currentColor.SetFromAGBR(sprites[spriteName]->colour());
	currentColor.a = 1;
	float delta = 0.001;

	if (interpolationSign ==  1){
	//TODO improve me
		if (currentColor.r < finalColor.x - delta|| currentColor.g < finalColor.y -delta || currentColor.b < finalColor.z -delta){
			if (currentColor.r != finalColor.x){
				currentColor.r += interpolationRate.x * dTime;
				if (currentColor.r > finalColor.x){
					currentColor.r = finalColor.x;
				}
			}

			if (currentColor.g != finalColor.y){
				currentColor.g += interpolationRate.y * dTime;
				if (currentColor.g > finalColor.y){
					currentColor.g = finalColor.y;
				}
			}

			if (currentColor.b != finalColor.z){
				currentColor.b += interpolationRate.z * dTime;
				if (currentColor.b > finalColor.z){
					currentColor.b = finalColor.z;
				}
			}
		}else{
			interpolationSign *= -1;
		}
}
	else{
		if (currentColor.r > finalColor.x + delta || currentColor.g > finalColor.y + delta  || currentColor.b > finalColor.z + delta ){
			if (currentColor.r != finalColor.x){
				currentColor.r -= interpolationRate.x * dTime;
				if (currentColor.r < finalColor.x){
					currentColor.r = finalColor.x;
				}
			}

			if (currentColor.g != finalColor.y){
				currentColor.g -= interpolationRate.y * dTime;
				if (currentColor.g < finalColor.y){
					currentColor.g = finalColor.y;
				}
			}

			if (currentColor.b != finalColor.z){
				currentColor.b -= interpolationRate.z * dTime;
				if (currentColor.b < finalColor.z){
					currentColor.b = finalColor.z;
				}
			}
		}else{
			interpolationSign *= -1;
		}
	}

	sprites[spriteName]->set_colour(currentColor.GetABGR());
}

void HUD::addSpriteToColorInterpolationList(std::string name){
	colorInterpolationSprites.push_back(name);
}

void HUD::deleteSpriteToColorInterpolationList (std::string name){
	for (std::vector<std::string>::iterator it = colorInterpolationSprites.begin(); it != colorInterpolationSprites.end(); ){
		if (*it == name){
			it = colorInterpolationSprites.erase(it);
			return;
		}else{
			it++;
		}
	}
}

void HUD::stopInterpolating(std::string name){
	for (std::vector<std::string>::iterator it = colorInterpolationSprites.begin(); it != colorInterpolationSprites.end(); ){
		if(*it == name){
			colorInterpolationSprites.erase(it);
			return;
		}else{
			it++;
		}
	}
}