#pragma once
#include <string>
#include <vector>
#include <map>
#include "Renderer.h"
#include <graphics\sprite.h>
#include <graphics\Colour.h>
class HUD{
public:
	struct TextInfo{
	public:
		TextInfo(int x1, int y1, const std::string t, abfw::Colour c){
			x = x1;
			y = y1;
			text = t;
			col = c;
		}
	int x,y;
	std::string text;
	abfw::Colour col;
	};

private:
	std::vector<TextInfo> textList;
	std::map<std::string,abfw::Sprite*> sprites;
	std::vector<std::string> colorInterpolationSprites;
	static HUD* instance;
	int interpolationSign;
	HUD();

private:
	void interpolateSpriteColor (float dTime, std::string spriteName, abfw::Vector3 interpolationRate, abfw::Vector3 finalColor);

public:
	static HUD* getInstance();

	void addText(int x, int y, const std::string, abfw::Colour = abfw::Colour(1,1,1));
	void addSprite(std::string name,abfw::Sprite*);
	abfw::Sprite* getSprite(std::string name);
	void removeText(TextInfo*);
	void draw(Renderer*);
	void cleanUp();
	void update(float dTime);
	void addSpriteToColorInterpolationList(std::string name);
	void deleteSpriteToColorInterpolationList (std::string name);
	void stopInterpolating(std::string name);
};