#pragma once
#include <string>
#include <graphics\Colour.h>
#include <maths\matrix44.h>
class Renderer{
public:
	virtual void begin() = 0;
	virtual void end() = 0;
	virtual void setProjectionMatrix(abfw::Matrix44) = 0;
	//TODO: modify to general colour class
	virtual void renderText(int x, int y, std::string text,abfw::Colour) = 0;
};