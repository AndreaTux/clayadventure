#include "Scene.h"
#include <string>
typedef std::map<std::string,SceneObject*>::iterator SceneIterator;

Scene::Scene()
	:debug(false){
	scene = new std::map<std::string,SceneObject*>();
}


Scene::~Scene(){
	delete scene;
	if (camera != 0){
		delete camera;
	}
}

void Scene::setCamera(Camera* cam){
	camera = cam;
}

void Scene::addObject(SceneObject* obj){
	scene->insert(std::pair<std::string,SceneObject*>(obj->getName(),obj));
}

void Scene::removeObject(std::string id){
	scene->erase(id);
}

void Scene::update(float dTime){
	if (!toDelete.empty()){
		cleanup();
	}
	for (SceneIterator it = scene->begin(); it != scene->end(); it++){
		if((*scene)[it->first] != 0)
			(*it).second->update(dTime);
	}
}

void Scene::moveObject(std::string id, abfw::Vector3 delta,bool byUser){
	if ((*scene)[id] != 0){
		(*scene)[id]->updatePosition(delta,byUser);
		if (id == "player"){
			camera->move(delta);
		}
	}
}

void Scene::scaleObject(std::string id, float newSize){
	(*scene)[id]->scale(newSize);
}

SceneObject* Scene::getObject(std::string name){
	return (*scene)[name];
}

void Scene::objectStopped(std::string id)const{
	scene->at(id)->stopped();
}

void Scene::setDebug(bool debg){
	this->debug = debg;
}

void Scene::cleanup(){
	for (std::vector<std::string>::iterator it = toDelete.begin(); it != toDelete.end(); it++){
		removeObject(*it);
	}

	toDelete.clear();
}

void Scene::removeObjectInNextFrame(std::string who){
	toDelete.push_back(who);
}
