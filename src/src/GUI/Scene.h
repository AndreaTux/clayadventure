#pragma once
#include <map>
#include "SceneObject.h"
#include "Renderer.h"
#include "Camera.h"
#include <vector>

class Scene{
protected:
	std::map<std::string,SceneObject*>* scene;
	std::vector<std::string> toDelete;
	Camera* camera;
	bool debug;

	void cleanup();

public:
	Scene();
	~Scene();

	virtual void addObject(SceneObject*);
	virtual void removeObject(std::string);
	virtual void removeObjectInNextFrame(std::string);
	virtual void moveObject(std::string,abfw::Vector3,bool byUser);
	virtual void objectStopped(std::string)const;
	virtual void scaleObject(std::string,float);
	virtual void render(Renderer*) = 0;
	virtual void update(float dTime);
	virtual void drawDebug(Renderer*) = 0;
	virtual SceneObject* getObject(std::string);

	void setDebug(bool);
	virtual void addObjectToDebugList(SceneObject*) = 0;

	void setCamera(Camera*);
};