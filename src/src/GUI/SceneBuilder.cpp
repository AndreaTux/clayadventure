#include "SceneBuilder.h"
#include "AbertaySpriteScene.h"
#include "gui_utils\GUIUtils.h"

SceneBuilder::SceneBuilder(){
	result = new AbertaySpriteScene;
}

void SceneBuilder::addPlayer(ConfigReader* configReader){
	std::string name	= configReader->getString("name");
	float width			= configReader->getFloat("width");
	float height		= configReader->getFloat("height");
	float x				= configReader->getFloat("x");
	float y				= configReader->getFloat("y");
	SpritePlayer* guiPlayer = new SpritePlayer(width,height,abfw::Vector3(x + width/2.f, y + height/2.f,0));
	result->setPlayer(guiPlayer);
}

void SceneBuilder::addSceneObject(ConfigReader* configReader,std::string texture){
	std::string name	= configReader->getString("name");
	float width			= configReader->getFloat("width");
	float height		= configReader->getFloat("height");
	float x				= configReader->getFloat("x");
	float y				= configReader->getFloat("y");
	AbertaySpriteSceneObject* sceneObject = new AbertaySpriteSceneObject (name,texture,width,height,abfw::Vector3(width/2.f,height/2.f,0));
}