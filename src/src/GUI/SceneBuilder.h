#pragma once
#include "AbertaySpriteScene.h"
#include "..\util\ConfigReader.h"

class SceneBuilder{
private:
	AbertaySpriteScene* result;
public:
	SceneBuilder();
	
	void addSceneObject(ConfigReader* configReader,std::string texture);
	void addPlayer(ConfigReader* configReader);

	Scene* getResult();
};