#include "SceneObject.h"
#include "GUIEventHandler.h"
#include "animation\AnimationManager.h"
#include <stdexcept>

SceneObject::SceneObject(std::string n,abfw::Vector3 pos)
	:name(n),position(pos),currentAnimation(0){

}

SceneObject::~SceneObject(){
	if (!animations.empty()){
		for (std::map<const std::string,Animation*>::iterator it = animations.begin(); it != animations.end();it++){
			delete it->second;
		}
	}
}

void SceneObject::changeAnimationManager(AnimationManager* other){
	animationManager = other;
}

const abfw::Vector3 SceneObject::getPosition()const{
	return position;
}

void SceneObject::updatePosition(abfw::Vector3 delta,bool byUser){
	position += delta;
	if (!animationManager->isEmpty()){
		animationManager->objectMoved(this,delta, byUser);
	}
}

std::string SceneObject::getName()const{
	return name;
}

void SceneObject::playAnimation(const std::string name,bool loop){
	if (animations.find(name) == animations.end()){
		return;
	}
	if(currentAnimation != 0){
		currentAnimation->stop();
	}
	
	animationStack.clear();
	animationStack.push_front(AnimationInfo(animations[name],loop));
	currentAnimation = animationStack.front().animation;
	currentAnimation->play(animationStack.front().looped);
}

void SceneObject::stackAndPlayAnimation(std::string name, bool loop){
	if (animations.find(name) == animations.end()){
		return;
	}
	if(currentAnimation != 0){
		currentAnimation->stop();
	}
	
	animationStack.push_front(AnimationInfo(animations[name],loop));
	currentAnimation = animationStack.front().animation;
	currentAnimation->play(loop);
}

void SceneObject::animationFinished(){
	animationManager->animationFinished(this);
	/*
	//if we have a current animation
	if (currentAnimation != 0){
		//stop it
		currentAnimation->stop();
		//inform the event handler
		GUIEventHandler::getInstance()->animationFinished(name,currentAnimation->getName());
		//delete the animation that just finished from the stack
		animationStack.pop_front();
		// if we have at least another animation left
		if(!animationStack.empty()){
			//make it current and play it
			currentAnimation = animationStack.front().animation;
			currentAnimation->play(animationStack.front().looped);
		}else{
			currentAnimation = 0;
		}
	}
	*/
}

void SceneObject::stopped(){
	
}

void SceneObject::insertAnimation(Animation* n){
	//animations.insert(std::pair<const std::string,Animation*>(n->getName(),n));
	animations[n->getName()] = n;
}

Animation* SceneObject::getAnimation(std::string name){
	Animation* animation = 0;
	try{
		animation = animations.at(name);
	}catch(std::out_of_range){
		return animation;
	}
	return animation;
}

std::map <const std::string,Animation*> SceneObject::getAnimations(){
	return animations;
}

Animation* SceneObject::getCurrentAnimation(){
	return currentAnimation;
}

void SceneObject::setCurrentAnimation(Animation* animation){
	this->currentAnimation = animation;
}

AnimationManager* SceneObject::getAnimationManager(){
	return animationManager;
}