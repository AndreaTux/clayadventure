#pragma once
#include <maths\vector3.h>
#include <string>
#include <map>
#include <list>
#include "Animation.h"

class AnimationManager;

struct AnimationInfo{
	AnimationInfo(Animation* a, bool b){
		animation = a;
		looped = b;
	}

	Animation* animation;
	bool looped;
};

class SceneObject{
protected:
	//center of the object
	abfw::Vector3 position;
	//name is the id
	std::string name;
	AnimationManager* animationManager;

	//to delete
	Animation* currentAnimation;
	std::map <const std::string,Animation*> animations;
	std::list<AnimationInfo> animationStack;

public:

	SceneObject(std::string ,abfw::Vector3 pos = abfw::Vector3(0,0,0));
	~SceneObject();

	virtual void updatePosition(abfw::Vector3 delta,bool byuser);

	virtual void update(float dTime) = 0;

	virtual void scale(float newSize) = 0;

	//overwrites the current animation
	virtual void playAnimation(std::string,bool);

	//add an animation
	virtual void stackAndPlayAnimation(std::string,bool);

	virtual void animationFinished();

	virtual void stopped();
	
	const abfw::Vector3 getPosition()const;

	void insertAnimation(Animation*); 

	std::string getName() const;
	// should not be public
	Animation* getAnimation(std::string name);

	Animation* getCurrentAnimation();

	void setCurrentAnimation(Animation*);

	std::map <const std::string,Animation*> getAnimations();

	AnimationManager* getAnimationManager();

	//should be protected
	void changeAnimationManager(AnimationManager* other);


};