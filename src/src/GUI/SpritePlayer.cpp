#include "SpritePlayer.h"
#include "animation\PlayerAnimationStateMachine.h"
#include "SpriteSheetKeyFrameAnimation.h"
#include "..\BL\EventHandler.h"
#include "..\Audio\AudioManager.h"

SpritePlayer::SpritePlayer(float width, float height, abfw::Vector3 pos)
	:AbertaySpriteSceneObject("player","",width,height,pos),invulnerabilityMode(false),drawingTime(0.1f),timeCounter(drawingTime),visible(true){
		transformations.insert(std::pair<std::string,AbertaySpriteSceneObject*>("ranged",new AbertaySpriteSceneObject("ranged","ranged_left.png",width + 20,height,position)));
		
		transformations.insert(std::pair<std::string,AbertaySpriteSceneObject*>("melee",new AbertaySpriteSceneObject("melee","melee_left.png",width, height + 30,abfw::Vector3(position.x, position.y + 15,0))));
		//whooo..don't do this Andrea, think something better
		//SMASH CALLBACK
		SpriteSheetKeyFrameAnimation* anim = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["melee"]->getAnimation("attack_right"));
		SpriteSheetKeyFrameAnimation* anim2 = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["melee"]->getAnimation("attack_left"));
		auto func = [] () {EventHandler::getInstance()->playerSmash();};
		anim->setCallbackAction(9,func);
		anim2->setCallbackAction(9,func);
		//WALK SOUND CALLBACK
		SpriteSheetKeyFrameAnimation* anim3 = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["melee"]->getAnimation("walk_right"));
		SpriteSheetKeyFrameAnimation* anim4 = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["melee"]->getAnimation("walk_left"));
		auto func2 = [] () {AudioManager::getInstance()->play("footstep_melee",false);};
		anim3->setCallbackAction(20,func2);
		anim4->setCallbackAction(20,func2);

		transformations.insert(std::pair<std::string,AbertaySpriteSceneObject*>("ball",new AbertaySpriteSceneObject("ball","ball.png",width, height,position)));
		transformations["ball"]->getAnimation("jump_right")->setHoldOnLastFrame(true);

		transformations.insert(std::pair<std::string,AbertaySpriteSceneObject*>("bird",new AbertaySpriteSceneObject("bird","null",width, height + 20,abfw::Vector3(position.x, position.y + 10,0))));

		//WALK SOUND BIRD
		SpriteSheetKeyFrameAnimation* anim5 = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["bird"]->getAnimation("walk_right"));
		SpriteSheetKeyFrameAnimation* anim6 = dynamic_cast<SpriteSheetKeyFrameAnimation*>(transformations["bird"]->getAnimation("walk_left"));
		anim5->setCallbackAction(4,func2);
		anim6->setCallbackAction(4,func2);

		//END ATTACKING
		auto endAttack = [] (){GUIEventHandler::getInstance()->playerFinishesAttacking();};
		((SpriteSheetKeyFrameAnimation*)transformations["ranged"]->getAnimation("attack_right"))->setCallbackAction(5,endAttack);
		((SpriteSheetKeyFrameAnimation*)transformations["ranged"]->getAnimation("attack_left"))->setCallbackAction(5,endAttack);
		anim->setCallbackAction(14,endAttack);
		anim2->setCallbackAction(14,endAttack);


		//ball change of animation fsm
		transformations["ball"]->changeAnimationManager(new PlayerAnimationStateMachine(transformations["ball"]->getAnimationManager()->getAnimations()));
		//get the current sprite
		sprite = transformations.at("ball")->getSprite();
		//set the current transformation
		currentTransformation = transformations.at("ball");
		
		animationManager = currentTransformation->getAnimationManager();
		

}


void SpritePlayer::changeForm(std::string formName){
	//stop the current animation
	if (currentAnimation != 0){
		this->animationFinished();
	}
	abfw::Sprite* newSprite = transformations.at(formName)->getSprite();
	float offsetX = newSprite->width() - this->sprite->width();
	float offsetY = newSprite->height() - this->sprite->height();

	this->sprite = newSprite;
	position.x += offsetX/2;
	position.y -= offsetY/2;
	sprite->set_position(position);
	originalHeight = (transformations.at(formName))->getOriginalHeight();
	originalWidth = (transformations.at(formName))->getOriginalWidth();

	transformations[formName]->setSide(side);
	//take the current side
	//side = currentTransformation->getSide();
	//set the new transformation
	currentTransformation = transformations[formName];
	//take the animations from the new transformation -- delete
	animations = currentTransformation->getAnimations();
	// transfer the stack to the new form
	((AnimationStateMachine*)currentTransformation->getAnimationManager())->setStack(((AnimationStateMachine*)animationManager)->getAnimationStack());
	//take the new form animation manager
	animationManager = currentTransformation->getAnimationManager();
	if (((AnimationStateMachine*)animationManager)->getAnimationStack().empty()){
		if (currentTransformation->getName() == "ball")
			animationManager->playAnimation("idle_right",true);
		else
			animationManager->playAnimation("idle_" + side, true);
	}

}

void SpritePlayer::scale(float newSize){
	if (originalHeight * newSize <= sprite->height() && originalWidth * newSize <= sprite->width()){
		//has been hit
		invulnerabilityMode = true;
	}
	//scale THIS object, for position scaling
	AbertaySpriteSceneObject::scale(newSize);
	//scale every OTHER sprite
	for (std::map<std::string,AbertaySpriteSceneObject*>::iterator it = transformations.begin(); it != transformations.end(); it++){
		//avoid the current transformation because it has already be scaled
		if (it->second->getSprite() != sprite)
			(*it).second->scale(newSize);
	}
}

void SpritePlayer::stopped(){
	if (currentAnimation != 0 ){
		if(currentTransformation->getName() == "ball"){
			currentAnimation->pause();
		}else{
			animationFinished();
		}
	}
	
}

void SpritePlayer::update(float dTime){
	if (invulnerabilityMode){
		timeCounter -= dTime;
		if (timeCounter <= 0){
			timeCounter = drawingTime;
			visible = !visible;
		}
	}
	AbertaySpriteSceneObject::update(dTime);
}

std::string SpritePlayer::getCurrentTransformationName(){
	return currentTransformation->getName();
}

bool SpritePlayer::isInvulnerable(){
	return invulnerabilityMode;
}

bool SpritePlayer::isVisible(){
	return visible;
}

void SpritePlayer::invulnerabilityEnded(){
	timeCounter = drawingTime;
	visible = true;
	invulnerabilityMode = false;
}