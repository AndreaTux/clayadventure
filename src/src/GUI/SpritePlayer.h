#pragma once
#include "AbertaySpriteSceneObject.h"
#include <map>

class SpritePlayer : public AbertaySpriteSceneObject{
private:
	std::map<std::string,AbertaySpriteSceneObject*> transformations;
	AbertaySpriteSceneObject* currentTransformation;

	bool invulnerabilityMode;
	bool visible;
	float drawingTime;
	float timeCounter;
public:
	SpritePlayer(float w, float h, abfw::Vector3);

	void update(float dTime);

	void changeForm(std::string);

	virtual void stopped();

	virtual void scale(float newSize);

	std::string getCurrentTransformationName();

	bool isInvulnerable();

	bool isVisible();

	void invulnerabilityEnded();
};