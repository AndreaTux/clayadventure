#include "SpriteSheetAnimation.h"
#include <graphics\image_data.h>
#include <assets\png_loader.h>
#include "gui_utils\GUIUtils.h"
#include "AbertaySpriteSceneObject.h"

SpriteSheetAnimation::SpriteSheetAnimation(std::string name, std::string texturePath, float frameDuration,
	int numberOfFrames, int rowFrames, int colFrames,AbertaySpriteSceneObject* s, int init)
	:Animation(name,frameDuration,numberOfFrames), framePerRow(rowFrames), framePerColumn(colFrames),sprite(s->getSprite()),texture(0),initFrame(init),player(s){
	
	typeName = name.substr(0,name.find("_"));

	if (texturePath != "null" || texturePath != ""){
		texture = GUIUtils::createTextureFormFile(texturePath);
	}
}

void SpriteSheetAnimation::play(bool loop){
	if(state == AnimationState::STOPPED){
		if (texture != 0){
			currentFrame = initFrame;
			sprite->set_texture(texture);
			sprite->set_uv_width(1.f/framePerRow);
			sprite->set_uv_height(1.f/framePerColumn);
			initUV();
		}
	}
	Animation::play(loop);
	
	
}

void SpriteSheetAnimation::stop(){
	Animation::stop();
	initUV();
}

void SpriteSheetAnimation::initUV(){
	if(!reverse){		
		sprite->set_uv_position(abfw::Vector2((initFrame % framePerRow) * 1.f/framePerRow,(initFrame / framePerRow) * 1.f/framePerColumn));

		currentFrame = initFrame;
	}else{
		sprite->set_uv_position(abfw::Vector2(((initFrame + framePerRow-1) % framePerRow) * 1.f/framePerRow,((numberOfFrames-1)/framePerRow) * 1.f/framePerColumn));
		currentFrame = numberOfFrames-1;
	}
}


void SpriteSheetAnimation::update(float dTime){
	//if the animation is playing
	if (state == PLAYING){
		abfw::Vector2 uvPos = sprite->uv_position();
		//only if we need to update the frame
		frameCount -= dTime;
		if (frameCount < 0.0f){
			frameCount = frameDuration;
			if (reverse){
				uvPos = updateReverse();
			}else{	
				currentFrame++;
				//if the frame is within the animation
				if (currentFrame < initFrame + numberOfFrames){
					//update the x value of uv
					uvPos.x += 1.f/framePerRow;
					//if we are out our UV
					if(uvPos.x >= 0.999){
						//reset the x
						uvPos.x = 0;
						//increase the y of uv
						uvPos.y += 1.f/framePerColumn;
						//if we are out our UV
						if (uvPos.y >= 0.999){
							//reset y
							uvPos.y = 0;
						}
					}
				}else{
					//if the animation is ended (i.e. the frame is outside the animation)
					if(!loop){
						if(!holdOnLastFrame){
							//stop the animation if there is no loop or need to hold the last frame
							stop();
							player->animationFinished();
						}else{
							//go back in the last animation frame
							currentFrame--;
							//pause the animation if there is the need to hold on the last frame
							pause();
						}
					}else{
						if (holdOnLastFrame){
							currentFrame--;
							pause();
						}else{
						//init the animation if loop is requested
						loopFinished();
						uvPos = sprite->uv_position();
						}
					}
				}
			}
		} // frame count < 0
		//only if the animation is playing, update the UVs
		if (state == PLAYING){
			sprite->set_uv_position(uvPos);
		}
	}//state is playing		
	
}


abfw::Vector2 SpriteSheetAnimation::updateReverse(){
	abfw::Vector2 uvPos = sprite->uv_position();
	currentFrame--;
	if ( currentFrame >= initFrame){
		uvPos.x -= 1.f/framePerRow;
		if (uvPos.x < -0.001){
			uvPos.x = ((initFrame + framePerRow-1) % framePerRow) * 1.f/framePerRow;
			uvPos.y -= 1.f/framePerColumn;
			if (uvPos.y < -0.001){
				uvPos.y = ((numberOfFrames-1)/framePerRow) * 1.f/framePerColumn;
			}
		}
	}else{
		//if the animation is ended (i.e. the frame is outside the animation)
			if(!loop){
				if(!holdOnLastFrame){
					//stop the animation if there is no loop or need to hold the last frame
					stop();
				}else{
					//pause the animation if there is the need to hold on the last frame
					pause();
				}
			}else{
				//init the animation if loop is requested
				loopFinished();
				uvPos = sprite->uv_position();
			}
	}
	return uvPos;
}


int SpriteSheetAnimation::getInitFrame(){
	return initFrame;
}

int SpriteSheetAnimation::getLastFrame(){
	return initFrame + numberOfFrames;
}

void SpriteSheetAnimation::loopFinished(){
	initUV();
}