#pragma once
#include "Animation.h"
#include <graphics\sprite.h>
#include <system\platform.h>

class AbertaySpriteSceneObject;

class SpriteSheetAnimation : public Animation{
protected:
	int framePerRow;
	int framePerColumn;
	int initFrame;
	const abfw::Texture* texture;
	AbertaySpriteSceneObject* player;
	
protected:
	abfw::Vector2 updateReverse();
	void initUV();

	virtual void loopFinished();
public:
	SpriteSheetAnimation(std::string name, std::string spriteSheetPath, float frameDuration,
		int numberOfFrames, int framePerRow, int framePerColumn,AbertaySpriteSceneObject*,int initFrame = 0);

	virtual void play(bool);
	virtual void stop();

	virtual void update(float dTime);
	abfw::Sprite* sprite;

	int getInitFrame();
	int getLastFrame();
};