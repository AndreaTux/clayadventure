#include "SpriteSheetAnimationCluster.h"
#include "SpriteSheetAnimation.h"

SpriteSheetAnimationCluster::SpriteSheetAnimationCluster (ConfigReader* reader,int r, int c, std::string n,abfw::Sprite* s)
	:rows(r),cols(c),name(n),sprite(s){
	std::vector<ConfigReader*> innerReader = reader->getConfigReaderList("animation");
	for(std::vector<ConfigReader*>::iterator anim = innerReader.begin(); anim != innerReader.end(); anim++){
			Animation* currentAnimation;
			std::string name	= (*anim)->getString("name");
			int begin			= (*anim)->getInteger("begin");
			int end				= (*anim)->getInteger("end");
			float duration		= (*anim)->getFloat("duration");

			currentAnimation = new SpriteSheetAnimation(name,"",duration,begin-end,rows,cols,s,begin);
			innerAnimations.insert(std::pair<std::string,Animation*>(name,currentAnimation));
	}
}

Animation* SpriteSheetAnimationCluster::getAnimation(std::string name){
	std::map<std::string,Animation*>::iterator it = innerAnimations.find(name);
	if (it == innerAnimations.end()){
		return 0;
	}
	return (*it).second;
}

SpriteSheetAnimationCluster::~SpriteSheetAnimationCluster(){
	for (std::map<std::string,Animation*>::iterator it = innerAnimations.begin(); it != innerAnimations.end(); it++){
		delete (*it).second;
	}
}