#pragma once
#include "Animation.h"
#include "..\util\ConfigReader.h"
#include <graphics\sprite.h>
#include <map>

class SpriteSheetAnimationCluster{
private:
	std::map<std::string,Animation*> innerAnimations;
	std::string name;
	int rows;
	int cols;
	abfw::Sprite* sprite;
public:
	SpriteSheetAnimationCluster ( ConfigReader*,int rows, int cols,std::string name,abfw::Sprite*);
	~SpriteSheetAnimationCluster();
	Animation* getAnimation(std::string name);
};