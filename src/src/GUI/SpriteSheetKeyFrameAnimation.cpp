#include "SpriteSheetKeyFrameAnimation.h"

SpriteSheetKeyFrameAnimation::SpriteSheetKeyFrameAnimation(std::string name, std::string texturePath, float frameDuration,
		int numberOfFrames, int rowFrames, int colFrames,AbertaySpriteSceneObject* s,int initFrame,std::map<int,bool> keyf)
		:SpriteSheetAnimation(name,texturePath,frameDuration,numberOfFrames,rowFrames,colFrames,s,initFrame),keyframe(keyf){

			resetCallbackDoneTrack();
}

void SpriteSheetKeyFrameAnimation::update(float dTime){
	SpriteSheetAnimation::update(dTime);
	if (keyframe.find(currentFrame) != keyframe.end() && !callbackDone[currentFrame]){
		if (callbacks.find(currentFrame) != callbacks.end()){
			callbacks[currentFrame]();
		}
		callbackDone[currentFrame] = true;
	}
}

void SpriteSheetKeyFrameAnimation::setCallbackAction(int keyFrame, std::function<void()> callback){
	callbacks[keyFrame] = callback;
}

void SpriteSheetKeyFrameAnimation::stop(){
	SpriteSheetAnimation::stop();
	resetCallbackDoneTrack();
}

void SpriteSheetKeyFrameAnimation::resetCallbackDoneTrack(){
	for (std::map<int,bool>::iterator it = keyframe.begin(); it != keyframe.end(); it++){
			callbackDone[it->first] = false;
	}
}

void SpriteSheetKeyFrameAnimation::loopFinished(){
	SpriteSheetAnimation::loopFinished();
	resetCallbackDoneTrack();
}