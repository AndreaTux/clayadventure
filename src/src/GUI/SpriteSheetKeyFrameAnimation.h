#pragma once
#include "SpriteSheetAnimation.h"
#include <map>
#include <functional>

//typedef void (*CallbackAction)();
class SpriteSheetKeyFrameAnimation : public SpriteSheetAnimation{
private:
	std::map<int,bool> keyframe;
	std::map<int,bool> callbackDone;
	std::map <int, std::function<void ()> > callbacks;

private:
	void resetCallbackDoneTrack();

protected:
	void loopFinished();
public:
	SpriteSheetKeyFrameAnimation(std::string name, std::string texturePath, float frameDuration,
		int numberOfFrames, int rowFrames, int colFrames,AbertaySpriteSceneObject* s, int initFrame,std::map<int,bool>keyFrames);
	void setCallbackAction(int keyFrame, std::function <void ()>);
	virtual void update(float);
	virtual void stop();
};