#include "TargetCamera.h"

TargetCamera::TargetCamera(float l, float r, float t, float b, float n, float f,const SceneObject* targ)
	:Camera(l,r,t,b,n,f),target(targ){

}

void TargetCamera::move(abfw::Vector3 delta){
	abfw::Vector3 targetPos = target->getPosition();
	//if the movement its towards right, the character should be at least at the center of the screen
	//in order to move the camera
	if (delta.x > 0 && targetPos.x >= right - (windowWidth / 2.f)	||
		delta.x < 0 && targetPos.x <= left + (windowWidth/ 2.f) )
			Camera::move(abfw::Vector3(delta.x,0,0));

	if(
		//down
		delta.y > 0 && targetPos.y >= (top + windowHeight/ 2.f)	||
		//up
		delta.y < 0 && targetPos.y <= (bottom - windowHeight/2.f)) {
			Camera::move(abfw::Vector3(0,delta.y,0));	
	}
}