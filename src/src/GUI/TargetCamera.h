#pragma once
#include "SceneObject.h"
#include "Camera.h"
//follow a target
class TargetCamera : public Camera{
private:
	const SceneObject* target;

public:
	TargetCamera(float left, float right, float top, float bottom, float near = 0.1f, float = 100,const SceneObject* target = 0);

	virtual void move(abfw::Vector3);

};