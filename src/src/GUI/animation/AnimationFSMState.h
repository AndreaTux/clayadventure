#pragma once
#include "..\..\AI\FSM\State.h"
#include "..\SceneObject.h"
#include "..\events\SceneObjectListener.h"
#include <string>

enum AnimationType{
	JUMP = 0,
	IDLE,
	ATTACK,
	KNOCK_BACK,
	TRANSFORMATION,
	FALLING,
	WALK
};

class AnimationFSMState : public State<SceneObject>{
protected:
	bool loop;
public:
	AnimationFSMState (std::string animationName);

	void enter (SceneObject*);
	void exit (SceneObject*);
	void execute (SceneObject*,float dTime);
	void setLoop(bool loop);
	bool getLoop();
};