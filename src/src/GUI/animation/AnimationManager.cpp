#include "AnimationManager.h"

void AnimationManager::addAnimation(Animation* a){
	animations[a->getName()] = a;
}

void AnimationManager::setDefaultAnimation(Animation* defaultAnim){
	this->defaultAnimation = defaultAnim;
}



AnimationManager::AnimationManager(std::map<const std::string, Animation*> ani){
	animations = ani;
	currentAnimation = 0;
	defaultAnimation = 0;
}

AnimationManager::AnimationManager()
	:currentAnimation(0){}

bool AnimationManager::isEmpty(){
	return animations.empty();
}

void AnimationManager::playAnimation(std::string name, bool loop){
	if (animations.find(name) != animations.end()){
		if(currentAnimation != 0){
			currentAnimation->stop();
		}
		currentAnimation = animations[name];
		currentAnimation->play(loop);
	}
}

std::map<const std::string,Animation*> AnimationManager::getAnimations(){
	return animations;
}

void AnimationManager::resetCurrentAnimation(){
	currentAnimation = 0;
}