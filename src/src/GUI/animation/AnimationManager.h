#pragma once
#include "..\events\SceneObjectListener.h"
#include "..\Animation.h"

class AnimationManager :public SceneObjectListener{
protected:
	Animation* currentAnimation;
	Animation* defaultAnimation;
	std::map <const std::string,Animation*> animations;
public:
	AnimationManager();
	
	AnimationManager(std::map<const std::string, Animation*> animations);

	virtual void playAnimation(std::string name, bool loop);

	virtual void addAnimation(Animation*);

	virtual void animationFinished(SceneObject*) = 0;
	
	virtual void updateAnimation(SceneObject*, float dTime) = 0;

	void setDefaultAnimation(Animation* defaultAnimaton);

	std::map<const std::string,Animation*> getAnimations();

	virtual void resetCurrentAnimation();

	bool isEmpty();
	

};