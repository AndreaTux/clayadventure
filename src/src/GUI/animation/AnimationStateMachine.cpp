#include "AnimationStateMachine.h"
#include "AnimationFSMState.h"
#include "..\GUIEventHandler.h"
#include "..\SpriteSheetAnimation.h"

#include "..\AbertaySpriteSceneObject.h"

AnimationStateMachine::AnimationStateMachine(){}

AnimationStateMachine::AnimationStateMachine(std::map<const std::string, Animation*> ani)
	:AnimationManager(ani){
	
	if (defaultAnimation != 0){
		pushState(animations[defaultAnimation->getName()]);
		currentAnimation = frontState();
		currentAnimation->play(true);
	}
}

void AnimationStateMachine::popState(){
	if (!animationStack.empty()){
		currentAnimation->stop();
		animationStack.pop_front();
		if (!animationStack.empty()){
			currentAnimation = frontState();
		}
		else{
			currentAnimation = 0;
		}
	}
}

void AnimationStateMachine::pushState(Animation* a){
	animationStack.push_front(a);
	if(currentAnimation != 0){
		currentAnimation->stop();
	}
	currentAnimation = frontState();
}


Animation* AnimationStateMachine::frontState(){
	return animationStack.front();	
}

void AnimationStateMachine::setStack(std::list<Animation*> animations){

	animationStack.clear();

	if (!animations.empty()){
		if (animations.front()->getTypeName() == "jump"){
			if (this->animations.find(animations.front()->getName()) != this->animations.end()){
				pushState(this->animations[animations.front()->getName()]);
				if (currentAnimation != 0){
					currentAnimation->playFromFrame(((SpriteSheetAnimation*)currentAnimation)->getLastFrame()-1,false);
				}
			}
		}
	}

	
}


void AnimationStateMachine::objectMoved(SceneObject* object, abfw::Vector3 delta, bool autonomous){

	if (!autonomous)
		return;

	if (currentAnimation->getTypeName() == "idle"){
		popState();
		pushState(animations["walk_" + ((AbertaySpriteSceneObject*)object)->getSide()]);
		currentAnimation->play(true);
	}
	if (currentAnimation->getTypeName() == "jump"){
		if (delta.x > 0 && currentAnimation->getName() != "jump_right"){
			popState();
			pushState(animations["jump_right"]);
			currentAnimation->setHoldOnLastFrame(true);
			currentAnimation->playFromFrame(((SpriteSheetAnimation*)currentAnimation)->getLastFrame()-1,false);
			((AbertaySpriteSceneObject*)object)->setSide("right");
		}
		if (delta.x < 0 && currentAnimation->getName() != "jump_left"){
			popState();
			pushState(animations["jump_left"]);
			currentAnimation->setHoldOnLastFrame(true);
			currentAnimation->playFromFrame(((SpriteSheetAnimation*)currentAnimation)->getLastFrame()-1,false);
			((AbertaySpriteSceneObject*)object)->setSide("left");
		}
	}
	if (currentAnimation->getTypeName() == "walk"){
		if (delta.x > 0 && currentAnimation->getName() != "walk_right" || delta.x < 0 && currentAnimation->getName() != "walk_left"){
			popState();
			pushState(animations["walk_" + ((AbertaySpriteSceneObject*)object)->getSide()]);
			currentAnimation = frontState();
			currentAnimation->play(true);
		}
		
	}

}


void AnimationStateMachine::objectLanded (SceneObject* o){
	if (currentAnimation != 0 && (currentAnimation->getTypeName() == "jump" || currentAnimation->getTypeName() == "attack" )){
		animationStack.clear();
	}
	pushState(animations["idle_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
	currentAnimation->play(true);
}

void AnimationStateMachine::objectJumped (SceneObject* o){
	if (currentAnimation->getTypeName() != "attack" || currentAnimation->getTypeName() != "knockBack"){
		if (currentAnimation != 0){
			popState();
		}
		pushState(animations["jump_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation->play(false);
	}
}

void AnimationStateMachine::objectFell (SceneObject* o){
	if (currentAnimation != 0){
		popState();
		pushState(animations["jump_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation->playFromFrame(((SpriteSheetAnimation*)currentAnimation)->getLastFrame()-1,false);
	}
}

void AnimationStateMachine::objectHit  (SceneObject* o)	{
	if (animations.find("knockBack_" + ((AbertaySpriteSceneObject*)o)->getSide()) != animations.end()){
		if(currentAnimation != 0 && currentAnimation->getTypeName() != "jump")
			popState();
		pushState(animations["knockBack_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation->play(false);
	}
}

void AnimationStateMachine::objectAttacked (SceneObject* o){
	if (currentAnimation->getTypeName() != "knockBack" && currentAnimation->getTypeName() != "attack"){
		if (currentAnimation->getTypeName() == "attack" || currentAnimation->getTypeName() == "walk"){
			popState();
		}
		pushState(animations["attack_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation->play(false);
		if (o->getName() == "player")
			GUIEventHandler::getInstance()->playerAttacks();
	}
}

void AnimationStateMachine::objectChangedForm(SceneObject* o){
	if (currentAnimation != 0){
		currentAnimation->stop();
	}
}

void AnimationStateMachine::objectCreated (SceneObject* o){
	
}

void AnimationStateMachine::objectDestroyed (SceneObject*){
	}

	 void AnimationStateMachine::objectStunned	(SceneObject*){
	}

void AnimationStateMachine::updateAnimation(SceneObject* o, float dTime){
	if (currentAnimation == 0 && animationStack.empty()){
		pushState(animations["idle_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation = frontState();
		currentAnimation->play(true);
	}else{
		if (currentAnimation == 0){
			std::string newAnimationName = frontState()->getTypeName() + "_" + ((AbertaySpriteSceneObject*)o)->getSide();
			if (animations.find(newAnimationName) != animations.end()){
				currentAnimation = animations[newAnimationName];
			}
		}
	}

	if (currentAnimation->getState() == PLAYING){
		currentAnimation->update(dTime);
	}else{
		if (currentAnimation->getTypeName() == "idle")
			currentAnimation->play(true);
	}
}

void AnimationStateMachine::objectStopped(SceneObject* o){

	if (!animationStack.empty() && frontState()->getTypeName() == "walk"){
		popState();
		pushState(animations["idle_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		currentAnimation->play(true);
	}else{
		popState();
		currentAnimation = 0;
	}
	
}

void AnimationStateMachine::playAnimation(std::string name, bool loop){
	if (currentAnimation != 0 && currentAnimation->getTypeName() == "destroyed"){
		return;
	}
	//try to find the animation by the name given
	if (animations.find(name) != animations.end()){
		if(currentAnimation != 0){
			popState();
		}
		pushState(animations[name]);
		currentAnimation->play(loop);
	}
}

void AnimationStateMachine::animationFinished(SceneObject* o){
	if (currentAnimation != 0){
		GUIEventHandler::getInstance()->animationFinished(o->getName(),currentAnimation->getName());
		popState();
		if (!animationStack.empty()){
			currentAnimation = frontState();
		}else{
			pushState(animations["idle_" + ((AbertaySpriteSceneObject*)o)->getSide()]);
		}
		currentAnimation->play(true);
	}
}

std::list<Animation*> AnimationStateMachine::getAnimationStack(){
	return animationStack;
}

void AnimationStateMachine::resetCurrentAnimation(){
	AnimationManager::resetCurrentAnimation();
	animationStack.clear();
}