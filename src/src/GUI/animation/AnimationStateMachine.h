#pragma once
#include "AnimationManager.h"
#include "AnimationFSMState.h"

class AnimationStateMachine : public AnimationManager{
protected:
	std::list<Animation*> animationStack;

	void popState();
	void pushState(Animation*);
	Animation* frontState();

public:
	AnimationStateMachine();
	AnimationStateMachine(std::map<const std::string, Animation*>);

	virtual void resetCurrentAnimation();

	void animationFinished(SceneObject*);

	void objectMoved(SceneObject*, abfw::Vector3 delta,bool autonom);

	void objectLanded (SceneObject*);

	void objectJumped (SceneObject*);

	void objectFell (SceneObject*);

	void objectHit  (SceneObject*);

	void objectAttacked (SceneObject*);

	void objectCreated (SceneObject*);

	void objectDestroyed (SceneObject*);

	void objectStunned	(SceneObject*);

	void objectStopped (SceneObject*);

	void updateAnimation(SceneObject* o, float dTime);

	void objectChangedForm (SceneObject*);

	void playAnimation(std::string, bool loop);

	void setStack(std::list<Animation*> s);

	std::list<Animation*> getAnimationStack();

};