#include "IdleAnimationState.h"

IdleAnimationState::IdleAnimationState (bool loop)
	:AnimationFSMState("idle",loop){
}

void IdleAnimationState::objectMoved(SceneObject* object, abfw::Vector3 delta){
	std::string movingAnimationName = "";
	if (delta.x > 0){
		movingAnimationName = "right";
	}
	if (delta.x < 0){
		movingAnimationName = "left";
	}

	if (movingAnimationName != ""){
		object->stackAndPlayAnimation(movingAnimationName, true);
	}
}

void objectLanded (SceneObject*){
	}

void objectJumped (SceneObject*){
	}

void objectFell (SceneObject*);

void objectHit  (SceneObject*);

void objectAttacked (SceneObject*);

void objectCreated (SceneObject*);

void objectDestroyed (SceneObject*);

void objectStunned	(SceneObject*);
	