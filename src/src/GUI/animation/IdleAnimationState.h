#pragma once
#include "..\..\AI\FSM\State.h"
#include "AnimationFSMState.h"

class IdleAnimationState : public AnimationFSMState{
public:
	IdleAnimationState (bool loop)
		:AnimationFSMState("idle",loop){}

	virtual void objectMoved(SceneObject*, abfw::Vector3 delta);

	virtual void objectLanded (SceneObject*);

	virtual void objectJumped (SceneObject*);

	virtual void objectFell (SceneObject*);

	virtual void objectHit  (SceneObject*);

	virtual void objectAttacked (SceneObject*);

	virtual void objectCreated (SceneObject*);

	virtual void objectDestroyed (SceneObject*);

	virtual void objectStunned	(SceneObject*);
	
	
};