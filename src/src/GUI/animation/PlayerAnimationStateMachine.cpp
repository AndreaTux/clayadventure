#include "PlayerAnimationStateMachine.h"
#include "..\SpritePlayer.h"
#include "..\SpriteSheetAnimation.h"

PlayerAnimationStateMachine::PlayerAnimationStateMachine(std::map<const std::string, Animation*> ani)
	:AnimationStateMachine(ani){};

void PlayerAnimationStateMachine::objectStopped (SceneObject* o){
	if (currentAnimation != 0 && currentAnimation->getTypeName() == "walk"){	
		currentAnimation->pause();
	}
}

void PlayerAnimationStateMachine::objectLanded (SceneObject* o){
	if (currentAnimation != 0 && currentAnimation->getTypeName() == "jump"){
		popState();
		pushState(animations["idle_right"]);
		currentAnimation->play(true);
	}
}

void PlayerAnimationStateMachine::objectJumped(SceneObject* o){
	if (currentAnimation->getTypeName() != "attack" || currentAnimation->getTypeName() != "knockBack"){
		if (currentAnimation != 0){
			popState();
		}
		pushState(animations["jump_right"]);
		currentAnimation->play(true);
	}
}

void PlayerAnimationStateMachine::objectMoved (SceneObject* o, abfw::Vector3 delta,bool autonomous){
	if (!autonomous)
		return;
	if (delta.x != 0 && currentAnimation->getTypeName() != "jump"){
		if (currentAnimation == 0 || currentAnimation->getTypeName() != "walk"){
				popState();
				pushState(animations["walk_right"]);
				currentAnimation = frontState();
				if (delta.x < 0){
					currentAnimation->setReverse(true);
				}
			}else{
				if (currentAnimation->getTypeName() == "walk"){
					if (currentAnimation->isReverted() && delta.x > 0){
						currentAnimation->setReverse(false);
					}
					if (!currentAnimation->isReverted() && delta.x < 0){
						currentAnimation->setReverse(true);
					}

					if (currentAnimation->getState() != PLAYING){
						currentAnimation->play(true);
					}
				}
		
		}
	}
}

void PlayerAnimationStateMachine::objectFell (SceneObject* o){
	if (currentAnimation != 0){
		popState();
		pushState(animations["jump_right"]);
		currentAnimation->playFromFrame(((SpriteSheetAnimation*)currentAnimation)->getLastFrame()-1,false);
	}
}

void PlayerAnimationStateMachine::updateAnimation(SceneObject* o, float dTime){
	if (((SpritePlayer*)o)->getCurrentTransformationName() != "ball"){
		PlayerAnimationStateMachine::updateAnimation(o,dTime);
	}
	else{
		if (currentAnimation == 0 && animationStack.empty()){
				pushState(animations["idle_right"]);
				currentAnimation = frontState();
				currentAnimation->play(true);
			}else{
				if (currentAnimation == 0){
					std::string newAnimationName = frontState()->getName();
					if (animations.find(newAnimationName) != animations.end()){
						currentAnimation = animations[newAnimationName];
					}
				}
		}
	}
	if (currentAnimation->getState() == PLAYING){
		currentAnimation->update(dTime);
	}
}

