#pragma once
#include "AnimationStateMachine.h"

class PlayerAnimationStateMachine : public AnimationStateMachine{
public:
	PlayerAnimationStateMachine(std::map<const std::string, Animation*>);

	void updateAnimation(SceneObject* o, float dTime);

	void objectStopped (SceneObject*);
	void objectMoved (SceneObject*, abfw::Vector3 delta, bool autonomous);
	void objectJumped (SceneObject*);
	void objectLanded (SceneObject*);
	void objectFell (SceneObject*);

};