#pragma once
#include "..\SceneObject.h"

class SceneObjectListener{
public:

	virtual void objectMoved(SceneObject*, abfw::Vector3 delta, bool autonomous)		= 0;

	virtual void objectLanded (SceneObject*)											= 0;

	virtual void objectJumped (SceneObject*)											= 0;

	virtual void objectFell (SceneObject*)												= 0;

	virtual void objectHit  (SceneObject*)												= 0;

	virtual void objectAttacked (SceneObject*)											= 0;

	virtual void objectCreated (SceneObject*)											= 0;

	virtual void objectDestroyed (SceneObject*)											= 0;

	virtual void objectStunned	(SceneObject*)											= 0;

	virtual void objectStopped (SceneObject*)											= 0;

	virtual void objectChangedForm (SceneObject*)										= 0;

};