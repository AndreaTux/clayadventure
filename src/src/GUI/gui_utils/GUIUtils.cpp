#include "GUIUtils.h"
#include <graphics\image_data.h>
#include <assets\png_loader.h>
#include <assert.h>

abfw::Platform* GUIUtils::platform = 0;

const abfw::Texture* GUIUtils::createTextureFormFile(std::string fileName){
	assert(platform != 0);

	abfw::Texture* texture = NULL;
	abfw::ImageData imageData;

	abfw::PNGLoader pngLoader;
	pngLoader.Load(fileName.c_str(),*platform,imageData);

	if(imageData.image()){
		texture = platform->CreateTexture(imageData);
	}
	return texture;
}

void GUIUtils::init(abfw::Platform*p){
	platform = p;
}