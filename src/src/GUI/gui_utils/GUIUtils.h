#pragma once
#include <system\platform.h>
#include <graphics\texture.h>
#include <string.h>

class GUIUtils{
private:
	static abfw::Platform* platform;
public:
	static void init(abfw::Platform*);
	static const abfw::Texture* createTextureFormFile(std::string);
};