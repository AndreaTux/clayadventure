#include <system/d3d11/platform_d3d11.h>
#include "sprite_app.h"
#include <iostream>
unsigned int sceLibcHeapSize = 128*1024*1024;	// Sets up the heap area size as 128MiB.

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	// initialisation
	abfw::PlatformD3D11 platform(hInstance, 960, 544, false, true);
	SpriteApp myApp(platform);
	myApp.Run();
	return 0;
}
