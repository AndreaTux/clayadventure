#include "sprite_app.h"
#include <system/platform.h>
#include <sstream>
#include <graphics/sprite_renderer.h>
#include <graphics/texture.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>
#include <assets/png_loader.h>
#include <graphics/image_data.h>
#include <graphics/font.h>
#include <input/touch_input_manager.h>
#include <maths/vector3.h>
#include <input/sony_controller_input_manager.h>
#include <maths/math_utils.h>
#include <system/debug_log.h>
#include "util\ConfigReader.h"
#include "BL\collisions\collision_behaviour\PlayerCollisionBehaviour.h"
#include "BL\collisions\collision_behaviour\SimpleBlockCollisionBehaviour.h"
#include "BL\collisions\collision_behaviour\ClayCollisionBehaviour.h"
#include "GUI\AbertaySpriteScene.h"
#include "GUI\GUIEventHandler.h"
#include "GUI\AbertaySpriteRenderer.h"
#include "BL\EventHandler.h"
#include "GUI\HUD.h"
#include "BL\objects\MovingBlock.h"
#include "BL\collisions\collision_behaviour\BreakableBlockCollisionBehaviour.h"
#include "GUI\TargetCamera.h"
#include "Audio\AudioManager.h"
#include "GUI\gui_utils\GUIUtils.h"
#include "BL\collisions\collision_behaviour\CheckpointCollisionBehaviour.h"
#include "BL\collisions\collision_behaviour\SpikeCollisionBehaviour.h"
#include "BL\collisions\collision_behaviour\TransformationClayCollisionBehaviour.h"
#include "BL\objects\player\transformations\MeleeTransformation.h"
#include "BL\objects\player\transformations\BirdTransformation.h"
#include "BL\objects\player\transformations\RangedTransformation.h"
#include "util\ConfigReader.h"
#include "BL\LevelLoaderBuilder.h"
#include "BL\LevelLoaderDirector.h"
#include "GUI\gui_utils\GUIUtils.h"
#include "util\RandomGenerator.h"
#include "GUI\AbertaySpriteSceneObject.h"
#include <time.h>
#include "BL\PS4InputManager.h"
#include "BL\Controls.h"


class InputManager;

SpriteApp::SpriteApp(abfw::Platform& platform) :
	Application(platform),
	sprite_renderer_(NULL),
	font_(NULL)

	
{
	uiArrow = uiMelee = uiBird = uiBall = uiRanged = 0;
	updateTitleScreen = true;
	gameFinished = false;
	titleScreenTimeCounter = titleScreenTime = 0.5f;
}

void SpriteApp::Init()
{
	sprite_renderer_ = platform_.CreateSpriteRenderer();
	renderer = new AbertaySpriteRenderer(platform_);
	GUIUtils::init(&platform_);
	RandomGenerator::initSeed(time(0));


	InitFont();

	GUIEventHandler::getInstance()->init(renderer);

	LevelLoaderBuilder* levelBuilder = new LevelLoaderBuilder;
	LevelLoaderDirector* levelDirector = new LevelLoaderDirector(levelBuilder);

	levelDirector->buildLevel("level_v3.xml");
	level = levelBuilder->getResult();
	scene = GUIEventHandler::getInstance()->getCurrentScene();

	
	player = new Player("player",abfw::Vector3(0,level->getGroundHeight()-32,0),abfw::Vector3(0,0,0),abfw::Vector3(0,0,0));

	//set level
	player->setLevel(level);
	//level adds
	level->addGameObject(player);

	//GUI
	float screenWidth = platform_.width();
	float screenHeight = platform_.height();
	
	//AbertaySpriteSceneObject* titleScreen = new AbertaySpriteSceneObject("scr","title.png",screenWidth,screenHeight,abfw::Vector3(screenWidth/2.f,screenHeight/2.f,-1.f));
	
	displayScreen(screenWidth/2,screenHeight/2.f,0,screenWidth,screenHeight,"title.png","scr");
	
	cam = new Camera(0,platform_.width(),0,platform_.height(),0.1,100);
	cam->setBoundaries(level->getLength(),0,level->getWidth());
	cam->setWindowSize(platform_.width(),platform_.height());
	
	SpritePlayer* guiPlayer = new SpritePlayer(32,32,abfw::Vector3(player->getPosition().x + 16, player->getPosition().y + 16,0));
	
	dynamic_cast<AbertaySpriteScene*>(scene)->setPlayer(guiPlayer);
	
	scene->setCamera(cam);
	//scene->addObject(titleScreen);

	//EVENTS

	audioManager = AudioManager::getInstance();
	audioManager->init();
	audioManager->play("screen_theme",true);
	
	//check the input type
	ConfigReader* settingReader = new ConfigReader("settings.xml");
	std::string controllerType = settingReader->getConfigReader("controls")->getString("type");
	Controls::initControls(controllerType);
	if (controllerType == "ps4")
		inputManager = new PS4InputManager(player,this,&platform_);
	else
		inputManager = new InputManager(player,this);


	EventHandler::getInstance()->init(player,level,audioManager,inputManager,this);

	delete levelBuilder, levelDirector;

}

void SpriteApp::CleanUp()
{
	CleanUpFont();
	delete sprite_renderer_;
	delete renderer;
	delete player;
	delete level;
	delete scene;
	delete uiRanged;
	delete uiArrow;
	delete uiMelee;
	delete uiBall;
	delete uiBird;
	delete inputManager;
	delete GUIEventHandler::getInstance();
	HUD::getInstance()->cleanUp();
	EventHandler::getInstance()->cleanUp();
	
	HUD* instance = HUD::getInstance();
//	delete instance; -> what's wrong with this and the ps4 controller?!
	delete EventHandler::getInstance();
	audioManager->shutdown();
	//delete audioManager;what's wrong with this and the ps4 controller?!
	sprite_renderer_ = NULL;
}

bool SpriteApp::Update(float frame_time)
{
	fps_ = 1.0f / frame_time;

	//TODO: use the HUD in a way that it doesn't delete the text as soon as we set it
	if (inputManager->isTitleScreen()){
		titleScreenTimeCounter -= frame_time;
		if (titleScreenTimeCounter <= 0){
			titleScreenTimeCounter = titleScreenTime;
			updateTitleScreen = true;
		}
	}

	inputManager->update(frame_time);
	
	audioManager->update(frame_time);
	level->update(frame_time);
	HUD::getInstance()->update(frame_time);
	scene->update(frame_time);

	return !gameFinished;
}

void SpriteApp::Render()
{
	scene->render(renderer);
	DrawHUD();
}
void SpriteApp::InitFont()
{
	font_ = new abfw::Font();
	font_->Load("comic_sans", platform_);
}

void SpriteApp::CleanUpFont()
{
	delete font_;
	font_ = NULL;
}

void SpriteApp::DrawHUD()
{
	
	/* DEBUG STUFF
	char* fps =  new char[128];
	char* pos =  new char[128];
	char* box =  new char[128];
	char* points = new char[128];
	char* guiPlayer = new char[128];

	sprintf(fps,"FPS: %.1f",fps_);

	HUD::getInstance()->addText(cam->getLeft() + 850,cam->getBottom() - 50, fps);
	const CollisionBox* c = player->getBoundingBox();
	sprintf(pos,"Player: %.1f %.1f",player->getPosition().x, player->getPosition().y);
	HUD::getInstance()->addText(cam->getLeft() + 10,cam->getTop() + 10, pos,abfw::Colour(1,0,0));
	
	sprintf(box,"Collision: %.1f %.1f", c->getPosition().x, c->getPosition().y);
	HUD::getInstance()->addText(cam->getLeft() + 10,cam->getTop() + 35,box,abfw::Colour(1,0,0));
	
	sprintf(points,"Points: %d",player->getPoints());
	HUD::getInstance()->addText(cam->getLeft() + 10,cam->getTop() + 55,points,abfw::Colour(1,0,0));

	sprintf(points,"GUI: %.1f %.1f",scene->getObject("player")->getPosition().x, scene->getObject("player")->getPosition().y);
	HUD::getInstance()->addText(cam->getLeft() + 10,cam->getTop() + 75,points,abfw::Colour(1,0,0));
	*/
	//TRANSFORMATION UI
	if(uiArrow != 0){
		uiArrow->set_position(abfw::Vector3(cam->getRight() - uiArrow->width() - 10 , cam->getTop() + uiArrow->height(),-1.f));
		uiMelee->set_position(abfw::Vector3(uiArrow->position().x, uiArrow->position().y + uiArrow->height()/2 + uiMelee->height()/2,-1.f));
		uiRanged->set_position(abfw::Vector3(uiArrow->position().x - uiArrow->width()/2 - uiArrow->width()/3, uiArrow->position().y,-1.f));
		uiBird->set_position(abfw::Vector3(cam->getRight() - uiBird->width()/2 - 10, uiArrow->position().y,-1.f));
		uiBall->set_position(abfw::Vector3(uiMelee->position().x, uiArrow->position().y - uiArrow->height()/2 - uiBall->height()/2,-1.f));

	}


	//draw STATUS
	//---
	HUD::getInstance()->draw(renderer); 
	//ADD THE DELETE!!
	//delete fps, box, pos, points, guiPlayer;
}

void SpriteApp::beginGame(){
	scene->removeObject("instruction");
	AbertaySpriteSceneObject* background = new AbertaySpriteSceneObject("background","level_v3.png",level->getWidth(),level->getLength(),abfw::Vector3(level->getWidth()/2.f,level->getLength()/2.f,2));
	background->getSprite()->set_position(abfw::Vector3(0,0,0));
	scene->addObject(background);
	cam = new TargetCamera(0,platform_.width(),level->getLength() - platform_.height(),level->getLength(),0.1f,100,scene->getObject("player"));
	cam->setBoundaries(level->getLength(),0,level->getWidth());
	cam->setWindowSize(platform_.width(),platform_.height());
	scene->setCamera(cam);

	//HUD
	//not really needed: this values will be overwritten by the update function
	//ARROW
	float uiWidth = 1444/15;
	float uiHeight = 1196/15;
	float offset = 10;
	uiArrow = new abfw::Sprite;
	uiArrow->set_position(abfw::Vector3(cam->getRight() - uiWidth - offset , cam->getTop(),-1.f));
	uiArrow->set_width(uiWidth);
	uiArrow->set_height(uiHeight);
	uiArrow->set_texture(GUIUtils::createTextureFormFile("UI_arrows.png"));
	uiArrow->set_colour(abfw::Colour(1,1,1).GetABGR());
	
	//MELEE
	uiMelee = new abfw::Sprite;
	uiMelee->set_width(uiWidth/2);
	uiMelee->set_height(uiHeight/2);
	uiMelee->set_position(abfw::Vector3(uiArrow->position().x, uiArrow->position().y + uiArrow->height()/2 + uiMelee->height()/2,-1.f));
	uiMelee->set_texture(GUIUtils::createTextureFormFile("UI_melee.png"));
	uiMelee->set_colour(abfw::Colour(1,1,1,.3f).GetABGR());
	
	//BIRD
	uiBird = new abfw::Sprite;
	uiBird->set_width(uiWidth/2);
	uiBird->set_height(uiHeight/2);
	uiBird->set_position(abfw::Vector3(cam->getRight() - uiBird->width()/2 - offset, cam->getTop() + uiHeight/2.f,-1.f));
	uiBird->set_texture(GUIUtils::createTextureFormFile("UI_bird.png"));
	uiBird->set_colour(abfw::Colour(1,1,1,.3f).GetABGR());

	//RANGED
	uiRanged = new abfw::Sprite;
	uiRanged->set_position(abfw::Vector3(uiArrow->position().x - uiWidth/2 - uiWidth/3, cam->getTop() + uiHeight/2.f,-1.f));
	uiRanged->set_width(uiWidth/2);
	uiRanged->set_height(uiHeight/3);
	uiRanged->set_texture(GUIUtils::createTextureFormFile("UI_ranged.png"));
	uiRanged->set_colour(abfw::Colour(1,1,1,.3f).GetABGR());

	//BALL
	
	uiBall = new abfw::Sprite;
	uiBall->set_position(abfw::Vector3(uiMelee->position().x, cam->getTop() + offset,-1.f));
	uiBall->set_width(uiWidth/3);
	uiBall->set_height(uiHeight/3);
	uiBall->set_texture(GUIUtils::createTextureFormFile("UI_ball.png"));
	uiBall->set_colour(abfw::Colour(1,0.8f,0.4f).GetABGR());
	
	
	HUD::getInstance()->addSprite("arrow",uiArrow);
	HUD::getInstance()->addSprite("melee",uiMelee);
	HUD::getInstance()->addSprite("bird",uiBird);
	HUD::getInstance()->addSprite("ranged",uiRanged);
	HUD::getInstance()->addSprite("ball",uiBall);


	AudioManager::getInstance()->play("cave",true);
	AudioManager::getInstance()->setVolume("cave",0.2f);

}

void SpriteApp::displayScreen(int x, int y, int z, int w, int h, std::string texturePath, std::string screenName){
	AbertaySpriteSceneObject* screen = new AbertaySpriteSceneObject(screenName,texturePath,w,h,abfw::Vector3(x,y,z));
	scene->addObject(screen);
}

void SpriteApp::setGameFinished(){
	gameFinished = true;
}

void SpriteApp::displayDeathScreen(){
	displayScreen(cam->getRight() - ( (cam->getRight() - cam->getLeft())/2 ),cam->getTop() + ( ( cam->getBottom() - cam->getTop()) /2),0,platform_.width(),platform_.height(),"death.png","death");
	inputManager->setDeathScreen();
}

void SpriteApp::displayEndLevelScreen(){
	displayScreen(cam->getRight() - ( (cam->getRight() - cam->getLeft())/2 ),cam->getTop() + ( ( cam->getBottom() - cam->getTop()) /2),0,platform_.width(),platform_.height(),"end.png","end");
	inputManager->setEndGamescreen();
}

void SpriteApp::displayInstructionScreen(){
	scene->removeObject("scr");
	float screenWidth = platform_.width();
	float screenHeight = platform_.height();
	
	displayScreen(screenWidth/2,screenHeight/2,0,screenWidth,screenHeight,"instructions.png","instruction");
}
