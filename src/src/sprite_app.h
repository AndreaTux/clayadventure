#ifndef _sprite_app_H
#define _sprite_app_H

#include <system/application.h>
#include <graphics/sprite.h>
#include <maths/vector2.h>
#include <maths/vector3.h>


#include <input/sony_controller_input_manager.h>
#include "BL\objects\player\Player.h"
#include "GUI\Scene.h"
#include "GUI\Renderer.h"
#include "GUI\Camera.h"
#include "BL\InputManager.h"
#include "Audio\AudioManager.h"

// FRAMEWORK FORWARD DECLARATIONS
namespace abfw
{
	class Platform;
	class SpriteRenderer;
	class Font;
}

class SpriteApp : public abfw::Application
{
public:
	SpriteApp(abfw::Platform& platform);
	void Init();
	void CleanUp();
	bool Update(float frame_time);
	void Render();

	void beginGame();
	void displayDeathScreen();
	void displayEndLevelScreen();
	void displayInstructionScreen();
	void displayScreen(int x, int y, int z, int w, int h, std::string texturePath, std::string name);
	void setGameFinished();
private:
	void InitFont();
	void CleanUpFont();
	void DrawHUD();

	abfw::SpriteRenderer* sprite_renderer_;
	Renderer* renderer;

	abfw::Font* font_;

	abfw::Sprite sprite_;
	abfw::Sprite sprite2_;

	InputManager* inputManager;
	Player* player;
	Level* level;
	Scene* scene;
	Camera* cam;
	AudioManager* audioManager;
	abfw::Sprite* uiArrow;
	abfw::Sprite* uiMelee;
	abfw::Sprite* uiBall;
	abfw::Sprite* uiRanged;
	abfw::Sprite* uiBird;
			

	bool updateTitleScreen;
	float titleScreenTime;
	float titleScreenTimeCounter;
	bool gameFinished;
	
	float fps_;
};

#endif // _sprite_app_H