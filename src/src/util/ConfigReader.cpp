#include "ConfigReader.h"
#include <iostream>
#include <fstream>
#include <sstream>

ConfigReader::ConfigReader(std::string fileName){
	std::ifstream file(fileName);
	if (!file.good()){
		//TODO: raise an exception
		std::cout << "No such file.";
		root = 0;
	}else{
		xml_document<> doc;
		//init the buffer
		std::stringstream buffer;
		//read the file
		buffer << file.rdbuf();
		file.close();
		//put the content into a string
		content = (buffer.str());
		//parse the result
		doc.parse<0>(&content[0]);
		root = doc.first_node();
		/*
		xml_node<>* r = root->first_node("monsters");
		xml_node<>* r1 = r->first_node("object");
		r1->first_attribute("x");
		*/
	}
}

ConfigReader::ConfigReader(xml_node<>* node)
	:root(node){}

ConfigReader::~ConfigReader(){
}

ConfigReader* ConfigReader::getConfigReader(std::string nodeName){
	//TODO: watch out for memory leak
	xml_node<>* node = root->first_node(nodeName.c_str());
	if(node != 0){
		ConfigReader* result = new ConfigReader(root->first_node(nodeName.c_str()));
		return result;
	}
	return 0;
}

std::vector<ConfigReader*> ConfigReader::getConfigReaderList(std::string name){
	std::vector<ConfigReader*> result;

	for (xml_node<> *pNode = root->first_node(name.c_str()); pNode; pNode=pNode->next_sibling())
	{
		result.push_back(new ConfigReader(pNode));
	}

	return result;
}

bool ConfigReader::getBoolean(std::string attrName){
	std::string result = root->first_attribute(attrName.c_str())->value();
	return result == "true" ? true : false;
}

int ConfigReader::getInteger(std::string attrName){
	xml_attribute<> *attr = root->first_attribute(attrName.c_str());
	if(attr)
		return atoi(root->first_attribute(attrName.c_str())->value());
	//fix it throwing an exception
	return -1;
}

float ConfigReader::getFloat(std::string attrName){
	return atof(root->first_attribute(attrName.c_str())->value());
}

std::string ConfigReader::getString(std::string attrName){
	xml_attribute<> *attr = root->first_attribute(attrName.c_str());
	if (attr){
		return attr->value();
	}
	return "";
}

bool ConfigReader::isValid(){
	return root != 0;
}