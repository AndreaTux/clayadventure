#pragma once
#include <rapidxml-1.13\rapidxml.hpp>
#include <string>
#include <vector>

using namespace rapidxml;

class ConfigReader{
private:
	xml_node<>* root;
	std::string content;

	ConfigReader(xml_node<>*);

public:
	ConfigReader(std::string file);
	~ConfigReader();

	ConfigReader* getConfigReader(std::string);

	std::vector<ConfigReader*> getConfigReaderList(std::string);

	bool getBoolean(std::string);

	int getInteger(std::string);

	float getFloat(std::string);

	std::string getString(std::string);

	bool isValid();

};