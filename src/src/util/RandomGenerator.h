#pragma once
#include <stdlib.h>
class RandomGenerator{
private:
	static int seed;
public:
	static void inline initSeed(int s){
		seed = s;
		srand(seed);
	}
	static int inline getRandomInteger(int max){
		return rand() % max;
	}
};